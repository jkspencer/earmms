# Contributors to EARMMS

If you have contributed to EARMMS, and wish to be recognised for your
contributions, please send a PR updating this document with your name!

* [Alice Jenkinson (alxce)](https://gitlab.com/alxce)
* [James Koro Spencer (jkspencer)](https://gitlab.com/jkspencer)
