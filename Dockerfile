FROM alpine:edge

VOLUME /config

RUN apk add --no-cache \
    ruby \
    ruby-dev \
    ruby-bundler \
    ruby-etc \
    nodejs \
    nodejs-dev \
    npm \
    gcc \
    make \
    musl-dev \
    libffi-dev \
    bash \
    ca-certificates \
    libsodium-dev \
    postgresql-dev \
    sqlite-dev \
    libcurl \
    curl-dev \
    supervisor \
    dcron

# Create application directory
RUN mkdir /app
WORKDIR /app

# Cache gems so that repeated builds are shorter.
COPY Gemfile* /app/
RUN bundle install --deployment --with production

# Do the same for Node modules.
COPY package.json package-lock.json /app/
RUN npm install

# Actually copy the application in
COPY . /app

# Add crontab
RUN crontab /app/docker/crontab

# supervisord config & execution
RUN mkdir -p /var/log/supervisor /etc/supervisor
COPY docker/supervisord.conf /etc/supervisor/supervisord.conf
CMD /app/docker/entrypoint.sh
