# Installing earmms

## Key derivation microservice

EARMMS v1.x requires earmms\_keyderiv v0.1.2. This is because earmms\_keyderiv
v2.x introduces encrypting the derived keys on the wire, which is not, and will
not be, supported in EARMMS v1.x.

Version 2.x of EARMMS requires at least version 2.0.0 of earmms\_keyderiv.

[Instructions for setting up earmms\_keyderiv can be found in the
earmms\_keyderiv repository.](https://gitlab.com/againstprisons/earmms_keyderiv)

## Installing dependencies

```
$ bundle install
$ npm install
```

## Environment variables

* `RACK_ENV`: environment to run this application in (should always be
  `production`)
* `CONFIG_FILE`: If set, the Ruby file passed in this environment variable will
  be loaded into the application at runtime.
* `KEYDERIV_URL`: The URL to a running `earmms_keyderiv` instance.
* `DATABASE_URL`: The URL to the database.
* `SESSION_SECRET`: The secret key used for sessions. This should always be set
  to something, if it's left blank a new key will be used every time the app is
  restarted, which will make all sessions invalid.

## `config.rb` variables

* `:site_name`: The name of this site. Displayed in various places throughout
  the application, and in emails.
* `:org_name`: The name of the organization that this site is serving.
  Displayed in various places throughout the application, and in emails.
* `:base_url`: The URL to the site. Used in emails.
* `:email_from`: The email address to send emails from.
* `:theme_dir`: The path to the theme directory to use.

## Email configuration

### Development mode

If EARMMS is running in development mode (`RACK_ENV=development`) then it will
use SMTP on `localhost:9333` unless configured otherwise. The intention is that
in development you will use something like [MailCatcher][] instead of sending
emails on a live system.

To run MailCatcher on the port that EARMMS expects, you can launch MailCatcher
with the following command:

```
$ mailcatcher --smtp-port=9333
```

[MailCatcher]: https://github.com/sj26/mailcatcher

### Production mode

You need to configure the [mail gem][] by adding a `Mail.defaults{}` block to
your `config.rb`.

An example of SMTP delivery:

```ruby
Mail.defaults do
  delivery_method :smtp, {
    :address => 'mail.example.com',
    :port => 587,
    :enable_starttls_auto => true,

    :authentication => 'plain',
    :user_name => 'earmms@example.com',
    :domain => 'example.com',
    :password => 'password',
  }
end
```

[mail gem]: https://github.com/mikel/mail

## Running database migrations

You must run migrations before starting EARMMS, and after upgrading.

```shell
$ bundle exec rake db:migrate
```

## In-app config variables

These are set through the web UI at `/system/config`.

* `maintenance` (bool): whether the site maintenance mode is enabled
* `signups` (bool): whether signups are allowed
* `kaupapa-prompt` (text): if set, the contents of this config entry are
  displayed when a user tries to change their membership status from supporter
  to member, accompanied by a checkbox which the user must check in order to
  continue with the status change.
* `2fa-required-for-roles` (bool): whether 2FA is required to assign roles to a
  user. Does not apply for roles assigned through rake tasks.
* `meeting-reminder-secs` (int): Number of seconds between auto-queued runs of
  the SupporterMeetingReminder job. To disable auto-queueing this job, do not
  create (or remove, if it's there already) this config entry.
* `email-to-self` (bool): If true, all emails sent by EARMMS will be sent to
  the configured `email_from` address in addition to the intended recipients
* `email-subject-prefix` (text): Configures the prefix for automated email
  subject lines. The possible values are:
  * `none`: no prefix
  * `org-name`: `Organization Name: email subject`
  * `org-name-brackets`: `[Organization Name] email subject`
  * `site-name`: `Site Name: email subject`
  * `site-name-brackets`: `[Site Name] email subject` (this is the default)

## Example Docker configuration

### `docker-compose.yml`

```yaml
version: '2'

services:
  keyderiv:
    image: peopleagainstprisons/earmms_keyderiv:latest
    container_name: keyderiv
    environment:
      # Encryption keys (change me! see the earmms_keyderiv docs)
      - "INDEX_KEY=<SECRET KEY GOES HERE>"
      - "ENCRYPT_KEY=<SECRET KEY GOES HERE>"

  earmms:
    image: peopleagainstprisons/earmms:latest
    container_name: earmms
    ports:
      - "9292:80"
    volumes:
      - './config/earmms:/config'
    environment:
      - "RACK_ENV=production"

      # URL to keyderiv
      - "KEYDERIV_URL=http://keyderiv/"

      # Database
      - "DATABASE_URL=sqlite:///config/earmms.db"

      # Session secret (change me!)
      - "SESSION_SECRET=<SESSION SECRET GOES HERE>"

      # Path to the configuration file
      - "CONFIG_FILE=/config/config.rb"
```

### `./config/earmms/config.rb`

```ruby
EARMMS.configure do |c|
  c[:base_url] = "https://earmms.example.com"
  c[:org_name] = "Example Organization"
  c[:site_name] = "EARMMS site"
  c[:email_from] = "earmms@example.com"

  ## Theme configuration - this assumes that the theme is in the directory
  ## `theme` beside the config file. Comment out the below line if you don't
  ## have a theme.
  c[:theme_dir] = File.expand_path("../theme", __FILE__)
end

Mail.defaults do
  ## Set up your mail configuration as detailed above.
end
```
