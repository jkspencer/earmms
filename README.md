# earmms

An encrypted-at-rest membership management system designed for
[People Against Prisons Aotearoa](https://papa.org.nz).

## READ ME: v1.x not recommended for new installations

The `main` branch of this repository contains the latest v1.x release of
EARMMS. v1.x is **not recommended for new installations**.

EARMMS v2.0.0 is currently in development, and contains a host of improvements
which make the initial setup and continued administration of an EARMMS
instance a lot easier for newcomers.

We recommend that you wait until the release of v2.0.0 if you are considering
setting up a new instance of EARMMS. We expect v2.0.0 to be released in early
January 2020.

[You can see the development progress of v2.0.0 in it's merge request][v2mr].

[v2mr]: https://gitlab.com/againstprisons/earmms/merge_requests/2

## Installation

To install dependencies:

```
$ bundle install
$ npm install
```

See [INSTALL.md](./INSTALL.md) for more information on configuration.

## Contributors

We wish to thank the many contributors to EARMMS, including those who
contribute in ways other than writing code. You can see a list of
contributors to this project in the [CONTRIBUTORS.md](./CONTRIBUTORS.md)
file in this repository.

## Code of conduct

Please note that this project is released with a Contributor Code of Conduct.
By participating in this project you agree to abide by its terms, which can
be found in the [CODE-OF-CONDUCT.md](./CODE-OF-CONDUCT.md) file in this
repository.

## License

EARMMS is released under the permissive MIT license. For license details,
please see the [LICENSE](./LICENSE) file in this repository.
