require File.expand_path("../config/application.rb", __FILE__)
EARMMS.initialize :no_load_models => true, :no_load_configs => true, :no_check_keyderiv => true

def do_setup
  EARMMS.load_models
  EARMMS.load_configs
end

desc "Run scheduled tasks"
task :work_queue do |t|
  do_setup
  EARMMS::Workers.perform_queue
end

namespace :site do
  desc "Toggle maintenance mode"
  task :maintenance do
    do_setup

    c = EARMMS::ConfigModel.where(key: 'maintenance').first
    unless c
      c = EARMMS::ConfigModel.new(key: 'maintenance', type: 'boolean', value: 'no')
    end

    c.value = (c.value == 'yes' ? 'no' : 'yes')
    c.save

    puts "maintenance on: #{c.value}"
  end

  desc "Toggle whether signups are allowed"
  task :signups do
    do_setup

    c = EARMMS::ConfigModel.where(key: 'signups').first
    unless c
      c = EARMMS::ConfigModel.new(key: 'signups', type: 'boolean', value: 'no')
    end

    c.value = (c.value == 'yes' ? 'no' : 'yes')
    c.save

    puts "signups enabled: #{c.value}"
  end

  desc "Set up default site configuration"
  task :default_config do
    do_setup

    defaults = {
      'signups' => {
        :type => 'boolean',
        :value => 'no',
      },
      'maintenance' => {
        :type => 'boolean',
        :value => 'no',
      },
      '2fa-required-for-roles' => {
        :type => 'boolean',
        :value => 'yes',
      },
      'email-to-self' => {
        :type => 'boolean',
        :value => 'no',
      },
      'email-subject-prefix' => {
        :type => 'text',
        :value => 'site-name-brackets',
      }
    }

    defaults.each do |k, v|
      if EARMMS::ConfigModel.where(:key => k).count > 0
        puts "#{k} exists, skipping"
        next
      end

      puts "setting #{k} to #{v}"
      c = EARMMS::ConfigModel.new(:key => k, :type => v[:type], :value => v[:value])
      c.save
    end

    puts "default_config ok"

    # XXX: update type `bool` to type `boolean` - this was an error in the
    # initial implementation of the default_config task
    ds = EARMMS::ConfigModel.where(type: 'bool')
    unless ds.count.zero?
      puts "correcting type on #{ds.count} entries"
      ds.update(type: "boolean")
      puts "correcting type ok"
    end
  end
end

namespace :user do
  desc "Generate an invite code with system privileges"
  task :invite_admin do |t|
    do_setup

    roles = ["system:*"]
    invite = EARMMS::Token.generate_invite(:roles => roles)
    invite.save

    puts "#{EARMMS.base_url}/auth/signup?invite=#{invite.token}"
  end

  desc "Grant a role to the given user"
  task :grant, [:uid, :role] do |t, args|
    do_setup

    abort "uid not provided" unless args.uid
    abort "role not provided" unless args.role

    u = EARMMS::User[args.uid.to_i]
    abort "invalid user" unless u

    r = EARMMS::UserHasRole.new(user: u.id, role: args.role.downcase)
    r.save

    puts "ok"
  end

  desc "Remove a role from the given user"
  task :revoke, [:uid, :role] do |t, args|
    do_setup

    abort "uid not provided" unless args.uid
    abort "role not provided" unless args.role

    u = EARMMS::User[args.uid.to_i]
    abort "invalid user" unless u

    r = EARMMS::UserHasRole.where(user: u.id, role: args.role.downcase)
    abort "user doesn't have role #{args.role.downcase.inspect}" unless r

    r.delete
    puts "ok"
  end

  desc "Describe a user"
  task :describe, [:uid] do |t, args|
    do_setup

    abort "uid not provided" unless args.uid

    u = EARMMS::User[args.uid.to_i]
    abort "invalid user" unless u

    roles = EARMMS::UserHasRole.where(user: u.id).map(&:role).map(&:downcase)
    if roles.length > 0
      puts "User id #{u.id} has roles:"
      roles.each do |r|
        puts "-> #{r.inspect}"
      end
    else
      puts "User id #{u.id} has no roles"
    end
  end
end

namespace :export do
  desc "Export a branch meeting's attendance"
  task :meeting, [:id] do |t, args|
    do_setup

    abort "id not provided" unless args.id
    abort "invalid meeting" unless EARMMS::BranchMeeting[args.id.to_i]

    EARMMS::Workers::AttendanceExport.perform("requesting_user" => 'stdout', "type" => 'meeting', "meeting" => args.id.to_i)
  end

  desc "Export attendance for all meetings of a branch"
  task :branch, [:id] do |t, args|
    do_setup

    abort "id not provided" unless args.id
    abort "invalid meeting" unless EARMMS::Branch[args.id.to_i]

    EARMMS::Workers::AttendanceExport.perform("requesting_user" => 'stdout', "type" => 'branch', "branch" => args.id.to_i)
  end

  desc "Export a user's attendance"
  task :user, [:id] do |t, args|
    do_setup

    abort "id not provided" unless args.id
    abort "invalid meeting" unless EARMMS::User[args.id.to_i]

    EARMMS::Workers::AttendanceExport.perform("requesting_user" => 'stdout', "type" => 'user', "user" => args.id.to_i)
  end
end

namespace :db do
  desc "Run database migrations"
  task :migrate, [:version] do |t, args|
    Sequel.extension(:migration)

    migration_dir = File.expand_path("../migrations", __FILE__)
    version = nil
    version = args[:version].to_i if args[:version]

    Sequel::Migrator.run(EARMMS.database, migration_dir, :target => version)
  end
end

namespace :release do
  desc "Change version to given version"
  task :change_version, [:version] do |t, args|
    abort "version not provided" unless args[:version]
    puts "Updating version to #{args[:version]}"

    # Change app/version.rb
    version_rb_path = File.expand_path("../app/version.rb", __FILE__)
    version_rb = File.read(version_rb_path)
    old_version_rb = /VERSION = "(.*)"/.match(version_rb)[1]
    puts "version.rb was at #{old_version_rb}"
    version_rb.gsub!("VERSION = \"#{old_version_rb}\"", "VERSION = \"#{args[:version]}\"")
    File.open(version_rb_path, 'w') do |f|
      f.write(version_rb)
    end

    # Change package.json
    package_json_path = File.expand_path("../package.json", __FILE__)
    package_json = JSON.parse(File.read(package_json_path))
    puts "package.json was at #{package_json["version"]}"
    package_json["version"] = args[:version]
    File.open(package_json_path, 'w') do |f|
      f.write(JSON.pretty_generate(package_json))
    end

    puts "You should now run `bundle install` and `npm install` to update lockfiles, "
    puts "and then tag the release as \"v#{args[:version]}\" and `git push`."
  end
end
