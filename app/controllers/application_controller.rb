require 'sinatra/base'
require 'haml'

class EARMMS::ApplicationController < Sinatra::Base
  helpers EARMMS::ApplicationHelpers

  set :default_encoding, "UTF-8"
  set :views, File.expand_path('../../views', __FILE__)
  set :haml, :format => :html5, :default_encoding => "UTF-8"
  enable :sessions

  before do
    # Check if maintenance mode
    if is_maintenance?
      if current_prefix?('/auth') && !current?('/auth/signup')
        next
      elsif current_prefix?('/static/')
        next
      else
        if logged_in? && has_role?('system:use_during_maintenance')
          next
        else
          halt haml :'maintenance/index', :layout => :layout_minimal
        end
      end
    end

    # Check if user has been disabled
    if logged_in?
      u = current_user
      if u.disabled_reason != nil
        if current_prefix?('/auth') || current_prefix?("/static/")
          next
        else
          halt redirect("/auth/disabled")
        end
      end
    end

    # Set CSRF
    session[:csrf] ||= EARMMS::Crypto.generate_token.encode(Encoding::UTF_8)
    response.set_cookie('authenticity_token', {
      value: session[:csrf],
      expired: Time.now + (60 * 60 * 24 * 30), # 30 days
      path: '/',
      httponly: true,
    })

    # Check CSRF
    if !request.safe?
      unless request.params.key?('_csrf')
        next halt 403, "CSRF failed"
      end

      unless session[:csrf] == request.params['_csrf']
        next halt 403, "CSRF failed"
      end

      unless request.cookies.key?('authenticity_token')
        next halt 403, "CSRF failed"
      end

      unless session[:csrf] == request.cookies['authenticity_token']
        next halt 403, "CSRF failed"
      end
    end

    # Check if user is still in the extended signup flow
    if logged_in? && settings.extended_signup
      if current_prefix?('/auth') || current_prefix?('/static')
        next
      else
        u = current_user
        s = u.signup_extended_status
        if s == nil
          u.signup_extended_status = s = "profile"
          u.save
        end

        if s != "complete"
          next redirect("/auth/signup/ex/#{s}")
        end
      end
    end
  end

  not_found do
    haml :not_found
  end
end
