class EARMMS::BranchAdminController < EARMMS::ApplicationController
  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'branch:access'

    @branches = EARMMS::Branch.all.map do |b|
      if has_role? "branch:#{b.id}"
        member_count = EARMMS::ProfileFilter.perform_filter(:branch, "#{b.id.to_s}:member").count

        next {
          :id => b.id,
          :name => b.decrypt(:name).force_encoding("UTF-8"),
          :member_count => member_count,
        }
      else
        nil
      end
    end.compact

    @title = "Branch administration"
    haml :"branch_admin/index"
  end

  get '/:branchid' do |branchid|
    branchid = branchid.to_i
    next halt 404 unless logged_in?
    next halt 404 unless has_role? "branch:#{branchid}:access"

    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    @branchname = @branch.decrypt(:name).force_encoding("UTF-8")
    @member_count = EARMMS::ProfileFilter.perform_filter(:branch, "#{@branch.id.to_s}:member").count

    now = DateTime.now()
    @yesterday = now - 1
    @yesterday = DateTime.civil(@yesterday.year, @yesterday.month, @yesterday.day, 23, 59, 59)
    @twoweeks = @yesterday + 14
    @upcoming = EARMMS::BranchMeeting.where(branch: @branch.id).order(Sequel.desc(:id)).limit(5).map do |m|
      dt = DateTime.parse(m.decrypt(:datetime))
      next nil unless dt.between?(@yesterday, @twoweeks)

      {
        :id => m.id,
        :datetime => dt,
      }
    end.compact

    @title = "Branch #{@branchname}".force_encoding("UTF-8")
    haml :"branch_admin/branch"
  end
end
