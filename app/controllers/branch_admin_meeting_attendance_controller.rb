class EARMMS::BranchAdminMeetingAttendanceController < EARMMS::ApplicationController
  helpers EARMMS::BranchAdminMeetingAttendanceHelpers

  get '/:branchid/:meetingid' do |branchid, meetingid|
    branchid = branchid.to_i
    meetingid = meetingid.to_i
    next halt 404 unless logged_in?
    next halt 404 unless has_role? "branch:#{branchid}:attendance"

    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    @branchname = @branch.decrypt(:name).force_encoding("UTF-8")

    @meeting = EARMMS::BranchMeeting[meetingid]
    next halt 404 unless @meeting
    next halt 403 unless @meeting.branch == branchid

    @datetime = DateTime.parse(@meeting.decrypt(:datetime))
    @guests = @meeting.decrypt(:guests).force_encoding("UTF-8")
    @users = get_users_for_branch_meeting(@meeting)
    @title = "Attendance for meeting on #{@datetime.strftime("%d/%m/%Y")}".force_encoding("UTF-8")

    haml :'branch_admin/meetings/attendance'
  end

  post '/:branchid/:meetingid' do |branchid, meetingid|
    branchid = branchid.to_i
    meetingid = meetingid.to_i
    next halt 404 unless logged_in?
    next halt 404 unless has_role? "branch:#{branchid}:attendance"

    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    @branchname = @branch.decrypt(:name).force_encoding("UTF-8")

    @meeting = EARMMS::BranchMeeting[meetingid]
    next halt 404 unless @meeting
    next halt 403 unless @meeting.branch == branchid

    @datetime = DateTime.parse(@meeting.decrypt(:datetime))
    @guests = @meeting.decrypt(:guests).force_encoding("UTF-8")
    if request.params.key?("guests")
      @guests = request.params["guests"].force_encoding("UTF-8")
      @meeting.encrypt(:guests, @guests)
      @meeting.save
    end

    @users = get_users_for_branch_meeting(@meeting).map do |u|
      status = u[:status]
      if request.params.key?("attendance-#{u[:uid]}")
        status = request.params["attendance-#{u[:uid]}"].strip.downcase
        case status
        when "present"
          status = :present
        when "apologies"
          status = :apologies
        else
          status = :unknown
        end
      end

      at = nil
      if u[:at] == nil
        at = EARMMS::BranchMeetingAttendanceRecord.new
        at.meeting = @meeting.id
        at.save

        u[:at] = at.id
      else
        at = EARMMS::BranchMeetingAttendanceRecord[u[:at]]
      end

      at.encrypt(:attendance_status, status.to_s)
      at.encrypt(:profile, u[:pid])
      at.save

      EARMMS::BranchMeetingAttendanceRecordFilter.clear_filters_for(at)
      EARMMS::BranchMeetingAttendanceRecordFilter.create_filters_for(at)

      u[:status] = status.to_sym
      next u
    end

    flash :success, "Meeting attendance saved for #{@users.length} users."

    @title = "Attendance for meeting on #{@datetime.strftime("%d/%m/%Y")}".force_encoding("UTF-8")
    haml :'branch_admin/meetings/attendance'
  end
end
