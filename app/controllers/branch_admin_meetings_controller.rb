require 'sanitize'

class EARMMS::BranchAdminMeetingsController < EARMMS::ApplicationController
  get '/:branchid' do |branchid|
    branchid = branchid.to_i
    next halt 404 unless logged_in?
    next halt 404 unless has_role? "branch:#{branchid}:meeting"

    page = request.params.key?("page") ? request.params["page"].to_i : 1
    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    @branchname = @branch.decrypt(:name).force_encoding("UTF-8")

    ds = EARMMS::BranchMeeting.where(branch: @branch.id).order(Sequel.desc(:id)).paginate(page, 25)
    @current_page = ds.current_page
    @total_pages = ds.page_count
    @meetings = ds.map do |x|
      {
        :id => x.id,
        :datetime => DateTime.parse(x.decrypt(:datetime)),
        :location => x.decrypt(:location).force_encoding("UTF-8"),
      }
    end

    @title = "Meetings for branch #{@branchname}".force_encoding("UTF-8")
    haml :'branch_admin/meetings/index'
  end

  get '/:branchid/new' do |branchid|
    branchid = branchid.to_i
    next halt 404 unless logged_in?
    next halt 404 unless has_role? "branch:#{branchid}:meeting"

    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    @branchname = @branch.decrypt(:name).force_encoding("UTF-8")
    @title = "New meeting for branch #{@branchname}".force_encoding("UTF-8")

    # fields
    @date = nil
    @time = nil
    @location = nil
    @notes = nil

    haml :'branch_admin/meetings/new'
  end

  post '/:branchid/new' do |branchid|
    branchid = branchid.to_i
    next halt 404 unless logged_in?
    next halt 404 unless has_role? "branch:#{branchid}:meeting"

    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    @branchname = @branch.decrypt(:name).force_encoding("UTF-8")
    @title = "New meeting for branch #{@branchname}".force_encoding("UTF-8")

    # fields
    @date = request.params["date"].force_encoding("UTF-8")
    @time = request.params["time"].force_encoding("UTF-8")
    @location = request.params["location"].force_encoding("UTF-8")
    @notes = (request.params["notes"] || "").force_encoding("UTF-8")
    @notes = Sanitize.fragment(@notes, Sanitize::Config::RELAXED) # Sanitize HTML

    unless @date
      flash :error, "Please provide a date."
      next haml :'branch_admin/meetings/new'
    end

    unless @time
      flash :error, "Please provide a time."
      next haml :'branch_admin/meetings/new'
    end

    unless @location
      flash :error, "Please provide a location."
      next haml :'branch_admin/meetings/new'
    end

    datetime = DateTime.parse("#{@date} #{@time}")
    unless datetime
      flash :error, "Please provide a valid date and time."
      next haml :'branch_admin/meetings/new'
    end

    meeting = EARMMS::BranchMeeting.new
    meeting.branch = @branch.id
    meeting.save

    meeting.encrypt(:datetime, datetime)
    meeting.encrypt(:location, @location)
    meeting.encrypt(:notes, @notes)
    meeting.save

    if request.params.key?("send-email") && request.params["send-email"].strip.downcase == "on"
      meeting.send_reminder!
    end

    flash :success, "The meeting was created."
    next redirect "/branch/meetings/#{@branch.id}/#{meeting.id}"
  end

  get '/:branchid/:meetingid' do |branchid, meetingid|
    branchid = branchid.to_i
    meetingid = meetingid.to_i

    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    next halt 404 unless logged_in?
    next halt 404 unless has_role? "branch:#{branchid}:meeting"

    @branchname = @branch.decrypt(:name).force_encoding("UTF-8")
    @meeting = EARMMS::BranchMeeting[meetingid]
    next halt 404 unless @meeting
    next halt 404 unless @meeting.branch == branchid

    @datetime = DateTime.parse(@meeting.decrypt(:datetime))
    @title = "Meeting on #{@datetime.strftime("%d/%m/%Y")} for #{@branchname}".force_encoding("UTF-8")

    @date = @datetime.strftime("%Y-%m-%d")
    @time = @datetime.strftime("%H:%M")
    @location = @meeting.decrypt(:location).force_encoding("UTF-8")
    @notes = @meeting.decrypt(:notes).force_encoding("UTF-8")

    haml :'branch_admin/meetings/meeting'
  end

  post '/:branchid/:meetingid' do |branchid, meetingid|
    branchid = branchid.to_i
    meetingid = meetingid.to_i

    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    next halt 404 unless logged_in?
    next halt 404 unless has_role? "branch:#{branchid}:meeting"

    @branchname = @branch.decrypt(:name).force_encoding("UTF-8")
    @meeting = EARMMS::BranchMeeting[meetingid]
    next halt 404 unless @meeting
    next halt 404 unless @meeting.branch == branchid

    @datetime = DateTime.parse(@meeting.decrypt(:datetime))
    @title = "Meeting on #{@datetime.strftime("%d/%m/%Y")} for #{@branchname}".force_encoding("UTF-8")

    @date = @datetime.strftime("%Y-%m-%d")
    @time = @datetime.strftime("%H:%M")
    @location = @meeting.decrypt(:location).force_encoding("UTF-8")
    @notes = @meeting.decrypt(:notes).force_encoding("UTF-8")

    halt 404, "invalid action" unless request.params.key?("action")
    case request.params["action"].downcase
    when "edit"
      # fields
      @date = request.params["date"].force_encoding("UTF-8")
      @time = request.params["time"].force_encoding("UTF-8")
      @location = request.params["location"].force_encoding("UTF-8")
      @notes = (request.params["notes"] || "").force_encoding("UTF-8")

      unless @date
        flash :error, "Please provide a date."
        next haml :'branch_admin/meetings/new'
      end

      unless @time
        flash :error, "Please provide a time."
        next haml :'branch_admin/meetings/new'
      end

      unless @location
        flash :error, "Please provide a location."
        next haml :'branch_admin/meetings/new'
      end

      @datetime = DateTime.parse("#{@date} #{@time}")
      unless @datetime
        flash :error, "Please provide a valid date and time."
        next haml :'branch_admin/meetings/new'
      end

      @meeting.encrypt(:datetime, @datetime)
      @meeting.encrypt(:location, @location)
      @meeting.encrypt(:notes, @notes)
      @meeting.save

      flash :success, "The meeting was saved."

    when "delete"
      if !request.params.key?("confirm") || request.params["confirm"] != "on"
        flash :error, "Please check the confirm box."
        next haml :'branch_admin/meetings/meeting'
      end

      EARMMS::BranchMeetingAttendanceRecord.where(meeting: @meeting.id).each do |ar|
        EARMMS::BranchMeetingAttendanceRecordFilter.where(attendance_record: ar.id).delete
        ar.delete
      end

      @meeting.delete
      flash :success, "The meeting was deleted."
      next redirect "/branch/meetings/#{@branch.id}"
    end

    haml :'branch_admin/meetings/meeting'
  end 
end
