class EARMMS::BranchAdminMembersController < EARMMS::ApplicationController
  get '/:branchid' do |branchid|
    branchid = branchid.to_i
    next halt 404 unless logged_in?
    next halt 404 unless has_role? "branch:#{branchid}:list"

    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    @branchname = @branch.decrypt(:name).force_encoding("UTF-8")

    @members = EARMMS::ProfileFilter.perform_filter(:branch, "#{@branch.id.to_s}:member").map do |f|
      p = EARMMS::Profile[f.profile]
      u = EARMMS::User[p.user]

      {
        :user => u,
        :profile => p,
        :user_name => p.decrypt(:full_name).force_encoding("UTF-8"),
      }
    end

    @title = "Member list for branch #{@branchname}"
    haml :"branch_admin/members"
  end

  get '/:branchid/text' do |branchid|
    branchid = branchid.to_i
    next halt 404 unless logged_in?
    next halt 404 unless has_role? "branch:#{branchid}:list"

    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    @branchname = @branch.decrypt(:name).force_encoding("UTF-8")

    @members = EARMMS::ProfileFilter.perform_filter(:branch, "#{@branch.id.to_s}:member").map do |f|
      p = EARMMS::Profile[f.profile]
      u = EARMMS::User[p.user]

      {
        :user => u,
        :profile => p,
        :user_name => p.decrypt(:full_name).force_encoding("UTF-8"),
      }
    end

    content_type "text/plain"
    @members.map do |m|
      "#{m[:user_name]} <#{m[:user].email}>".force_encoding("UTF-8")
    end.join(", ").force_encoding("UTF-8")
  end
end
