class EARMMS::BranchAdminUserAttendanceController < EARMMS::ApplicationController
  get '/:branchid/:user' do |branchid, userid|
    branchid = branchid.to_i
    userid = userid.to_i
    next halt 404 unless logged_in?
    next halt 404 unless has_role? "branch:#{branchid}:attendance"

    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    @branchname = @branch.decrypt(:name).force_encoding("UTF-8")

    @user = EARMMS::User[userid]
    next halt 404 unless @user

    @profile = EARMMS::Profile.for_user(@user)
    next halt 403 unless @profile.decrypt(:branch) == branchid.to_s
    @user_name = @profile.decrypt(:full_name).force_encoding("UTF-8")

    page = request.params.key?("page") ? request.params["page"].to_i : 1
    @ds = EARMMS::BranchMeetingAttendanceRecordFilter.perform_filter(:profile, @profile.id.to_s).order(Sequel.desc(:id)).paginate(page, 25)
    @current_page = @ds.current_page
    @total_pages = @ds.page_count

    @attendance = @ds.map do |a|
      at = EARMMS::BranchMeetingAttendanceRecord[a.attendance_record]
      next nil unless at

      meeting = EARMMS::BranchMeeting[at.meeting]
      next nil unless meeting
      next nil unless meeting.branch == @branch.id

      status = at.decrypt(:attendance_status)
      case status
      when "present"
        status = :present
      when "apologies"
        status = :apologies
      else
        status = :unknown
      end

      {
        :meetingid => meeting.id,
        :datetime => DateTime.parse(meeting.decrypt(:datetime)),
        :status => status
      }
    end.compact

    @attendance.sort!{|a, b| b[:datetime] <=> a[:datetime]}

    @title = "Attendance for user #{@user_name}".force_encoding("UTF-8")
    haml :'branch_admin/user_attendance'
  end
end
