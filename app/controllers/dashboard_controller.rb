class EARMMS::DashboardController < EARMMS::ApplicationController
  helpers EARMMS::DashboardHelpers

  get '/' do
    redirect '/' unless logged_in?

    @title = "Dashboard"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @mybranch = @profile.decrypt(:branch).strip.to_i

    name = @profile.decrypt(:full_name)
    @has_set_name = !([nil, ''].include?(name))

    @branches = branch_meetings
    @mymeetings = @branches[@mybranch][:upcoming] if @branches[@mybranch]
    @othermeetings = @branches.keys.map do |id|
      next nil if id == @mybranch
      @branches[id][:upcoming]
    end.flatten.compact

    haml :'dashboard/index'
  end
end
