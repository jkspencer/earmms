class EARMMS::DashboardMeetingsController < EARMMS::ApplicationController
  get '/:branchid/:meetingid' do |branchid, meetingid|
    branchid = branchid.to_i
    meetingid = meetingid.to_i

    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    next halt 404 unless logged_in?

    @branchname = @branch.decrypt(:name).force_encoding("UTF-8")
    @meeting = EARMMS::BranchMeeting[meetingid]
    next halt 404 unless @meeting
    next halt 404 unless @meeting.branch == branchid

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @mybranch = @profile.decrypt(:branch).strip.to_i

    @datetime = DateTime.parse(@meeting.decrypt(:datetime))
    @title = "Meeting on #{@datetime.strftime("%d/%m/%Y")} for #{@branchname}".force_encoding("UTF-8")

    @location = @meeting.decrypt(:location).force_encoding("UTF-8")
    @notes = @meeting.decrypt(:notes).force_encoding("UTF-8")

    @at = EARMMS::BranchMeetingAttendanceRecordFilter.perform_filter(:profile, @profile.id.to_s).order(Sequel.desc(:id)).map do |a|
      at = EARMMS::BranchMeetingAttendanceRecord[a.attendance_record]
      next nil unless at
      next nil unless at.meeting == @meeting.id

      at
    end.compact.first

    haml :'dashboard/meeting'
  end

  post '/:branchid/:meetingid' do |branchid, meetingid|
    branchid = branchid.to_i
    meetingid = meetingid.to_i

    @branch = EARMMS::Branch[branchid]
    next halt 404 unless @branch
    next halt 404 unless logged_in?

    @branchname = @branch.decrypt(:name).force_encoding("UTF-8")
    @meeting = EARMMS::BranchMeeting[meetingid]
    next halt 404 unless @meeting
    next halt 404 unless @meeting.branch == branchid

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @mybranch = @profile.decrypt(:branch).strip.to_i

    @datetime = DateTime.parse(@meeting.decrypt(:datetime))
    @title = "Meeting on #{@datetime.strftime("%d/%m/%Y")} for #{@branchname}".force_encoding("UTF-8")

    @location = @meeting.decrypt(:location).force_encoding("UTF-8")
    @notes = @meeting.decrypt(:notes).force_encoding("UTF-8")

    next 400 unless request.params.key?("action")
    case request.params["action"].strip.downcase
    when "apologies"
      next 400, "not home branch" unless @branch.id == @mybranch

      @at = EARMMS::BranchMeetingAttendanceRecordFilter.perform_filter(:profile, @profile.id.to_s).order(Sequel.desc(:id)).map do |a|
        at = EARMMS::BranchMeetingAttendanceRecord[a.attendance_record]
        next nil unless at
        next nil unless at.meeting == @meeting.id

        at
      end.compact.first

      if @at
        case @at.decrypt(:attendance_status)
        when "apologies"
          flash :error, "You have already sent apologies for this meeting."
          next haml :'dashboard/meeting'

        when "present"
          flash :error, "You can't send apologies for a meeting you've already attended."
          next haml :'dashboard/meeting'

        end
      else
        @at = EARMMS::BranchMeetingAttendanceRecord.new
        @at.meeting = @meeting.id
        @at.save
      end

      @at.encrypt(:attendance_status, "apologies")
      @at.encrypt(:profile, @profile.id)
      @at.save

      EARMMS::BranchMeetingAttendanceRecordFilter.clear_filters_for(@at)
      EARMMS::BranchMeetingAttendanceRecordFilter.create_filters_for(@at)

      flash :success, "You have sent apologies for this meeting."
    else
      next 400, "invalid action"
    end

    haml :'dashboard/meeting'
  end
end
