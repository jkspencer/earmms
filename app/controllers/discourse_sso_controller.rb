class EARMMS::DiscourseSSOController < EARMMS::ApplicationController
  helpers EARMMS::DiscourseSSOHelpers

  get '/' do
    next halt(404) unless discourse_sso_enabled?

    # store payload/signature in session
    session[:discourse_sso] = {
      :payload => request.params["sso"],
      :signature => request.params["sig"],
    }

    unless logged_in?
      session[:after_login] = '/auth/sso/discourse/authenticate'

      flash(:warning, "Please log in to continue.")
      next redirect('/auth')
    end

    redirect('/auth/sso/discourse/authenticate')
  end

  get '/authenticate' do
    next halt(404) unless discourse_sso_enabled?
    next halt(403) unless logged_in?

    # display an error if the user isn't permitted to log in to discourse
    next halt(403, haml(:'discourse_sso/forbidden')) unless discourse_user_can_log_in?

    # get info from session
    raw_sso_data = session.delete(:discourse_sso)
    next halt(400, haml(:'discourse_sso/error')) unless raw_sso_data

    # get nonce value
    sso_data = discourse_sso_parse_data(raw_sso_data)
    next halt(400, haml(:'discourse_sso/error')) unless sso_data
    next halt(400, haml(:'discourse_sso/error')) unless [
      sso_data.key?(:nonce),
      sso_data[:nonce] != nil,
      sso_data.key?(:return_url),
      sso_data[:return_url] != nil,
    ].all?

    # check if valid domain
    host = Addressable::URI.parse(sso_data[:return_url]).normalized_host
    next halt(400, haml(:'discourse_sso/error')) unless discourse_host_whitelist.include?(host)

    # generate response
    response_data = discourse_sso_generate_response(sso_data[:nonce])

    # generate callback url
    callback = Addressable::URI.parse(sso_data[:return_url])
    callback.query_values = {
      "sso" => response_data[:payload],
      "sig" => response_data[:signature]
    }

    next redirect(callback.to_s)
  end
end
