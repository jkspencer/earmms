class EARMMS::GroupAddUserController < EARMMS::ApplicationController
  helpers EARMMS::GroupHelpers

  get '/:cid' do |cid|
    next halt 404 unless logged_in?

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    next halt 404 unless cid
    @group = EARMMS::Group[cid]
    next halt 404 unless @group
    @group_hash = get_groups_hash([@group.id], :user => @user)
    @group_hash = @group_hash[@group.id]

    next halt 404 unless @group_hash[:user_membership]
    next halt 404 unless @group_hash[:user_membership][:user_id] == @user.id
    next halt 404 unless @group_hash[:user_membership][:admin]

    @title = "Group: #{@group_hash[:name]}".force_encoding("UTF-8")

    haml :'group_admin/add/index'
  end

  post '/:cid' do |cid|
    next halt 404 unless logged_in?

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    next halt 404 unless cid
    @group = EARMMS::Group[cid]
    next halt 404 unless @group
    @group_hash = get_groups_hash([@group.id], :user => @user)
    @group_hash = @group_hash[@group.id]

    next halt 404 unless @group_hash[:user_membership]
    next halt 404 unless @group_hash[:user_membership][:user_id] == @user.id
    next halt 404 unless @group_hash[:user_membership][:admin]

    @title = "Group: #{@group_hash[:name]}".force_encoding("UTF-8")

    unless request.params.key?("user") && request.params["user"].strip != ""
      flash :error, "Invalid user."
      next haml :'group_admin/add/index'
    end

    uid = request.params["user"].strip.to_i
    user = EARMMS::User[uid]
    unless user
      flash :error, "Invalid user."
      next haml :'group_admin/add/index'
    end

    profile = EARMMS::Profile.for_user(user)
    unless profile
      flash :error, "Invalid user."
      next haml :'group_admin/add/index'
    end

    # check if group is restricted to org members, and if so, check if user is
    # not an org member
    if @group_hash[:restrict_to_member]
      if profile.decrypt(:membership_status)&.strip&.downcase != "member"
        flash :error, "#{profile.decrypt(:full_name)} (user ID #{user.id}) is not an organisation member, and this group is restricted to organisation members only.".force_encoding("UTF-8")
        next haml :'group_admin/add/index'
      end
    end

    # check if this user is in the group already
    @members = get_group_members(@group)
    if @members.keys.include?(user.id)
      flash :error, "#{profile.decrypt(:full_name)} (user ID #{user.id}) is already a member of this group!".force_encoding("UTF-8")
      next haml :'group_admin/add/index'
    end

    # gather information
    @add_user = {
      :uid => user.id,
      :pid => profile.id,
      :name => profile.decrypt(:full_name).force_encoding("UTF-8"),
      :email => user.email,
      :branch => 'unknown',
      :branch_id => profile.decrypt(:branch).to_i,
    }

    branch = EARMMS::Branch[@add_user[:branch_id]]
    if branch
      @add_user[:branch] = branch.decrypt(:name)
    end

    do_add = (request.params.key?("do-add") && request.params["do-add"].strip.downcase == "on")
    if do_add
      cm = EARMMS::GroupMember.new(group: @group.id)
      cm.save
      cm.encrypt(:user, user.id)
      cm.encrypt(:roles, "")
      cm.save

      EARMMS::GroupMemberFilter.clear_filters_for(cm)
      EARMMS::GroupMemberFilter.create_filters_for(cm)

      flash :success, "#{@add_user[:name]} (user ID #{@add_user[:uid]}) was added to the group.".force_encoding("UTF-8")
      next haml :'group_admin/add/index'

    else
      next haml :'group_admin/add/confirm'
    end
  end
end
