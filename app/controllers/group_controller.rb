class EARMMS::GroupController < EARMMS::ApplicationController
  helpers EARMMS::GroupHelpers

  get '/' do
    next halt 404 unless logged_in?

    @title = "Your groups"

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    @group_ids = get_group_ids_user_is_member(@user, :only_admin => true)
    next halt 404 if @group_ids.empty?

    @groups_by_type = get_groups_hash_by_type(@group_ids, {
      :user => @user,
      :only_user_joined => true,
      :link_to_edit => :groupadmin
    })

    next halt 404 if @groups_by_type.empty?

    haml :'group_admin/index'
  end
end
