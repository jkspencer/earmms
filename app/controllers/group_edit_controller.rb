class EARMMS::GroupEditController < EARMMS::ApplicationController
  helpers EARMMS::GroupHelpers

  get '/:id' do |cid|
    next halt 404 unless logged_in?

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    next halt 404 unless cid
    @group = EARMMS::Group[cid]
    next halt 404 unless @group
    @group_hash = get_groups_hash([@group.id], :user => @user)
    @group_hash = @group_hash[@group.id]

    next halt 404 unless @group_hash[:user_membership]
    next halt 404 unless @group_hash[:user_membership][:user_id] == @user.id
    next halt 404 unless @group_hash[:user_membership][:admin]

    @title = "Group: #{@group_hash[:name]}".force_encoding("UTF-8")
    @members = get_group_members(@group)

    haml :'group_admin/edit'
  end

  post '/:id' do |cid|
    next halt 404 unless logged_in?

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    next halt 404 unless cid
    @group = EARMMS::Group[cid]
    next halt 404 unless @group
    @group_hash = get_groups_hash([@group.id], :user => @user)
    @group_hash = @group_hash[@group.id]

    next halt 404 unless @group_hash[:user_membership]
    next halt 404 unless @group_hash[:user_membership][:user_id] == @user.id
    next halt 404 unless @group_hash[:user_membership][:admin]

    @title = "Group: #{@group_hash[:name]}".force_encoding("UTF-8")
    @members = get_group_members(@group)

    unless request.params.key?("action") && request.params["action"].strip != ""
      next halt 400, "no action provided"
    end

    action = request.params["action"].strip.downcase
    if action == "user_remove"
      unless request.params.key?("user") && request.params["user"].strip != ""
        next halt 400, "invalid user"
      end

      uid = request.params["user"].strip.to_i
      user = EARMMS::User[uid]
      next halt 400, "invalid user" unless user

      profile = EARMMS::Profile.for_user(user)
      next halt 400, "invalid user" unless profile

      if user.id == @user.id
        flash :error, "You can't remove yourself from the group through the admin interface. Please use the \"leave group\" button on the group summary page."
        next redirect(request.path)
      end

      cm = EARMMS::GroupMemberFilter.perform_filter(:user, user.id.to_s).map do |cmf|
        cm = EARMMS::GroupMember[cmf.group_member]
        next nil unless cm.group == @group.id

        cm
      end.compact.first

      roles = cm.decrypt(:roles).split(",").map do |r|
        r.strip.downcase
      end

      if roles.include?("admin")
        flash :error, "You can't delete an admin of the group. Please demote the user first."
        next redirect(request.path)
      end

      EARMMS::GroupMemberFilter.clear_filters_for(cm)
      cm.delete

      flash :success, "Successfully removed #{profile.decrypt(:full_name)} (user ID #{user.id})".force_encoding("UTF-8")
      next redirect(request.path)

    elsif action == "user_promote"
      unless request.params.key?("user") && request.params["user"].strip != ""
        next halt 400, "invalid user"
      end

      uid = request.params["user"].strip.to_i
      user = EARMMS::User[uid]
      next halt 400, "invalid user" unless user

      profile = EARMMS::Profile.for_user(user)
      next halt 400, "invalid user" unless profile

      if user.id == @user.id
        admins = @members.keys.map do |k|
          next nil unless @members[k][:admin]
          next nil if @members[k][:user].id == @user.id

          k
        end.compact

        if admins.empty?
          flash :error, "You can't demote yourself if there are no other group admins. Please promote someone else first."
          next redirect(request.path)
        end
      end

      cm = EARMMS::GroupMemberFilter.perform_filter(:user, user.id.to_s).map do |cmf|
        cm = EARMMS::GroupMember[cmf.group_member]
        next nil unless cm.group == @group.id

        cm
      end.compact.first

      roles = cm.decrypt(:roles).split(",").map do |r|
        r.strip.downcase
      end

      if roles.include?("admin")
        roles.delete("admin")
        flash :success, "Successfully demoted #{profile.decrypt(:full_name)} (user ID #{user.id})".force_encoding("UTF-8")
      else
        roles << "admin"
        flash :success, "Successfully promoted #{profile.decrypt(:full_name)} (user ID #{user.id})".force_encoding("UTF-8")
      end

      cm.encrypt(:roles, roles.join(","))
      cm.save

      EARMMS::GroupMemberFilter.clear_filters_for(cm)
      EARMMS::GroupMemberFilter.create_filters_for(cm)
      next redirect(request.path)

    else
      next halt 400, "invalid action"
    end

    redirect(request.path)
  end
end
