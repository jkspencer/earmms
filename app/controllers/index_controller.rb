class EARMMS::IndexController < EARMMS::ApplicationController
  get '/' do
    redirect '/dashboard' if logged_in?

    @title = "Home"
    haml :'index/index'
  end
end
