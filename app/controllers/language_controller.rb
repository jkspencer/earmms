class EARMMS::LanguageController < EARMMS::ApplicationController
  post '/' do
    if request.params.key?('action')
      if request.params['action'] == 'change-language'
        session[:lang] = request.params['language']

        if logged_in?
          u = current_user
          p = EARMMS::Profile.for_user(u)
          p.encrypt(:preferred_language, session[:lang])
          p.save
        end

        next redirect(request.referer)
      end
    end
  end
end
