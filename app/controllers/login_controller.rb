require 'rotp'
require 'base64'
require 'u2f'

class EARMMS::LoginController < EARMMS::ApplicationController
  helpers EARMMS::LoginHelpers

  before do
    @u2f ||= U2F::U2F.new(request.base_url)
  end

  get '/' do
    @title = "Log in"

    haml :'auth/login/index'
  end

  post '/' do
    next redirect('/') if logged_in?

    errormsg = "Invalid email or password"
    next error_page(errormsg) unless request.params.key?("email")
    next error_page(errormsg) unless request.params.key?("password")

    @user = EARMMS::User.where(email: request.params["email"].downcase).first
    next error_page(errormsg) unless @user
    next error_page(errormsg) unless @user.verify_password(request.params["password"])

    p = EARMMS::Profile.for_user(@user)
    l = p.decrypt(:preferred_language)
    session[:lang] = l if l != ''

    if @user.totp_enabled
      session[:uid] = @user.id
      next redirect '/auth/twofactor'
    elsif EARMMS::U2FRegistration.where(user: @user.id).count > 0
      session[:uid] = @user.id
      next redirect '/auth/twofactor'

    else
      token = EARMMS::Token.generate_session(@user)
      session[:token] = token.token
      token.save

      flash(:success, "Successfully logged in!")

      redir_to = session.delete(:after_login)
      redir_to = '/' unless redir_to
      next redirect(redir_to)
    end
  end

  get '/twofactor' do
    next redirect('/') if logged_in?

    next halt 400 unless session[:uid]
    u = EARMMS::User[session[:uid].to_i]
    next halt 400 unless u

    @has_u2f = EARMMS::U2FRegistration.where(user: u.id).count > 0
    @has_totp = u.totp_enabled

    key_handles = EARMMS::U2FRegistration.where(user: u.id).map(&:key_handle)
    @app_id = @u2f.app_id
    @challenge = @u2f.challenge
    session[:u2f_challenge] = @challenge
    @sign_requests = @u2f.authentication_requests(key_handles).map do |m|
      m = JSON.parse(JSON.generate(m))
      m["appId"] = @app_id
      m["challenge"] = @challenge
      m
    end

    haml :'auth/login/twofactor'
  end

  post '/twofactor' do
    next redirect('/') if logged_in?

    unless session.key?(:uid)
      flash(:error, "An error occurred while trying to authenticate you, please try again")
      next redirect('/auth/login')
    end

    u = EARMMS::User[session[:uid].to_i]
    unless u
      flash(:error, "An error occurred while trying to authenticate you, please try again")
      next redirect('/auth/login')
    end

    unless request.params.key?("2fa_type")
      flash(:error, "An error occurred while trying to authenticate you, please try again")
      next redirect('/auth/login')
    end

    @has_u2f = EARMMS::U2FRegistration.where(user: u.id).count > 0
    @has_totp = u.totp_enabled

    key_handles = EARMMS::U2FRegistration.where(user: u.id).map(&:key_handle)
    @app_id = @u2f.app_id
    @challenge = @u2f.challenge
    if session[:u2f_challenge]
      @challenge = session[:u2f_challenge]
    else
      @challenge = @u2f.challenge
      session[:u2f_challenge] = @challenge
    end

    @sign_requests = @u2f.authentication_requests(key_handles).map do |m|
      m = JSON.parse(JSON.generate(m))
      m["appId"] = @app_id
      m["challenge"] = @challenge
      m
    end

    case request.params["2fa_type"].strip.downcase
    when "totp"
      next twofactor_error_page("Please provide a verification code.") unless request.params.key?("code")

      t = ROTP::TOTP.new(u.decrypt(:totp_secret))
      if t.verify_with_drift(request.params["code"].strip, 15)
        token = EARMMS::Token.generate_session(u)
        session[:uid] = nil
        session[:token] = token.token
        token.save

        flash :success, "Successfully logged in!"

        redir_to = session.delete(:after_login)
        redir_to = '/' unless redir_to
        next redirect(redir_to)

      else
        next twofactor_error_page("Invalid verification code.")
      end
    when "u2f"
      next twofactor_error_page("An error occurred, please try again.") unless request.params.key?("response")

      resp = U2F::SignResponse.load_from_json(request.params["response"])
      reg = EARMMS::U2FRegistration.where(user: u.id, key_handle: resp.key_handle).first
      next twofactor_error_page("An error occurred, please try again") unless reg

      begin
        @u2f.authenticate!(session[:u2f_challenge], resp, Base64.decode64(reg.public_key), reg.counter)
      rescue U2F::Error => e
        next twofactor_error_page("Unable to authenticate: #{e.class.name}")
      ensure
        session.delete(:u2f_challenge)
      end

      reg.counter = resp.counter
      reg.save

      token = EARMMS::Token.generate_session(u)
      session[:uid] = nil
      session[:token] = token.token
      token.save

      flash :success, "Successfully logged in!"

      redir_to = session.delete(:after_login)
      redir_to = '/' unless redir_to
      next redirect(redir_to)

    else
      flash(:error, "An error occurred while trying to authenticate you, please try again")
      next redirect('/auth/login')
    end
  end

  get '/logout' do
    redirect('/auth') unless logged_in?
    t = EARMMS::Token.where(token: session[:token]).first
    redirect('/auth') unless t

    t.valid = false
    t.save

    flash(:success, "Successfully logged out.")
    redirect('/auth')
  end
end
