class EARMMS::LoginPasswordResetController < EARMMS::ApplicationController
  get '/' do
    @title = "Password reset"

    next redirect('/') if logged_in?
    haml :'auth/password_reset/index'
  end

  post '/' do
    @title = "Password reset"

    next redirect('/') if logged_in?

    unless request.params.key?("email")
      flash :error, "Please supply an email address."
      next haml :'auth/password_reset/index'
    end

    email = request.params["email"].strip.downcase
    user = EARMMS::User.where(email: email).first

    # Don't let the user know that the email address is incorrect as this can
    # be a way to quickly discover whether a given email address exists as a
    # user in the system
    unless user
      next haml :'auth/password_reset/sent'
    end

    user.send_password_reset!

    haml :'auth/password_reset/sent'
  end

  get '/:token' do |token|
    @title = "Password reset"
    next redirect('/') if logged_in?

    @token = EARMMS::Token.where(token: token.strip.downcase).first
    next halt 404 unless @token
    next halt 404 unless @token.valid
    next halt 404 unless @token.use == 'password_reset'

    @user = EARMMS::User[@token.user]
    next halt 404 unless @user

    haml :'auth/password_reset/reset'
  end

  post '/:token' do |token|
    @title = "Password reset"
    next redirect('/') if logged_in?

    @token = EARMMS::Token.where(token: token.strip.downcase).first
    next halt 404 unless @token
    next halt 404 unless @token.valid
    next halt 404 unless @token.use == 'password_reset'

    @user = EARMMS::User[@token.user]
    next halt 404 unless @user

    unless request.params.key?("password") && request.params.key?("password-repeat")
      flash :error, "Please provide your password in both fields."
      next haml :'auth/password_reset/reset'
    end

    if request.params["password"].strip == "" || request.params["password-repeat"].strip == ""
      flash :error, "Please provide your password in both fields."
      next haml :'auth/password_reset/reset'
    end

    unless request.params["password"] == request.params["password-repeat"]
      flash :error, "The passwords you entered did not match."
      next haml :'auth/password_reset/reset'
    end

    @user.password = request.params["password"]
    @user.save

    flash :success, "Your password has been successfully changed, you may now log in."
    next redirect('/auth')
  end
end
