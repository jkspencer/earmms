require 'sanitize'

class EARMMS::MassEmailController < EARMMS::ApplicationController
  helpers EARMMS::MassEmailHelpers

  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role?('email:access') || is_group_admin?(:include_email_privs => true)

    @title = "Mass emailing"
    @targets = get_email_targets
    if @targets.empty?
      next haml :'mass_email/no_targets'
    end

    @previous_emails = EARMMS::MassEmail.where(user: current_user.id).map do |m|
      target = m.decrypt(:target)
      target_h = @targets.select{|x| x[:target] == target}.first
      target_name = "unknown"
      target_name = target_h[:name] if target_h && target_h.key?(:name)

      {
        :id => m.id,
        :created => m.creation,
        :target => target,
        :target_name => target_name.force_encoding("UTF-8"),
      }
    end

    @selected = @targets[0][:target]
    @text = ""
    @subject = ""

    haml :'mass_email/index'
  end

  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role?('email:access') || is_group_admin?(:include_email_privs => true)

    @title = "Mass emailing"
    @targets = get_email_targets
    if @targets.empty?
      next haml :'mass_email/no_targets'
    end

    @previous_emails = EARMMS::MassEmail.where(user: current_user.id).map do |m|
      target = m.decrypt(:target)
      target_h = @targets.select{|x| x[:target] == target}.first
      target_name = "unknown"
      target_name = target_h[:name] if target_h && target_h.key?(:name)

      {
        :id => m.id,
        :created => m.creation,
        :target => target,
        :target_name => target_name.force_encoding("UTF-8"),
      }
    end

    unless request.params.key?("target")
      flash :error, "Target was not provided"
      next haml :'mass_email/index'
    end

    @selected = request.params["target"].strip.downcase
    if @targets.select{|x| x[:target] == @selected}.empty?
      flash :error, "Invalid target was selected"
      next haml :'mass_email/index'
    end

    @subject = request.params["subject"]
    if @subject == nil || @subject.strip.empty?
      flash :error, "You must provide a subject for the email."
      next haml :'mass_email/index'
    end
    @subject = @subject.force_encoding("UTF-8")

    @text = request.params["text"]
    if @text == nil || @text.strip.empty?
      flash :error, "You must provide content for the email."
      next haml :'mass_email/index'
    end
    @text = @text.force_encoding("UTF-8")

    if request.params.key?("reedit") and request.params["reedit"] == "on"
      next haml :'mass_email/index'
    end

    haml :'mass_email/confirm'
  end

  post '/send' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role?('email:access') || is_group_admin?(:include_email_privs => true)

    @title = "Mass emailing"
    @targets = get_email_targets
    @footer = "Please do not reply to this email.\n"

    unless request.params.key?("target")
      flash :error, "Target was not provided"
      next haml :'mass_email/index'
    end

    @selected = request.params["target"].strip.downcase
    if @targets.select{|x| x[:target] == @selected}.empty?
      flash :error, "Invalid target was selected"
      next haml :'mass_email/index'
    end

    @subject = request.params["subject"]
    if @subject == nil || @subject.strip.empty?
      flash :error, "You must provide a subject for the email."
      next haml :'mass_email/index'
    end
    @subject = @subject.force_encoding("UTF-8")

    @text = request.params["text"]
    if @text == nil || @text.strip.empty?
      flash :error, "You must provide content for the email."
      next haml :'mass_email/index'
    end

    # Sanitize HTML
    @text = Sanitize.fragment(@text, Sanitize::Config::RELAXED)
    @text = @text.force_encoding("UTF-8")

    # Gather list of emails to send to
    users = []
    t = @selected.split ":"
    case t[0]
    when "branch"
      id = t[1]
      status = t[2]
      branch = EARMMS::Branch[id.to_i]
      unless branch
        flash :error, "Invalid branch specified"
        next haml :'mass_email/index'
      end

      @footer += "You have been sent this email because you are a #{status} of the #{branch.decrypt(:name)} branch of #{org_name}."

    when "group"
      id = t[1]
      group = EARMMS::Group[id.to_i]
      unless group
        flash :error, "Invalid group specified"
        next haml :'mass_email/index'
      end

      if has_role?("email:all_groups")
        has_perms = true
      else
        has_perms = EARMMS::GroupMemberFilter.perform_filter(:user, current_user.id.to_s).map do |cmf|
          cm = EARMMS::GroupMember[cmf.group_member]
          next nil unless cm

          group = EARMMS::Group[cm.group]
          next nil unless group

          roles = cm.decrypt(:roles).split(",")
          next nil unless roles.include?("email") || roles.include?("admin")

          next true
        end.compact.all?
      end

      unless has_perms
        flash :error, "You do not have permission to send to that group."
        next haml :'mass_email/index'
      end

      @footer += "You have been sent this email because you are a member of the #{org_name} group \"#{group.decrypt(:name)}\"."

    when "status"
      status = t[1].downcase

      @footer += "You have been sent this email because you are a #{status} of #{org_name}."

    when "all"
      @footer += "You have been sent this email because you have signed up to #{org_name}."

    else
      flash :error, "Invalid target was selected"
      next haml :'mass_email/index'
    end

    # add global email header
    header_entry = EARMMS::ConfigModel.where(key: 'mass-email-header').first
    if header_entry && header_entry.respond_to?(:value)
      val = Sanitize.fragment(header_entry.value.force_encoding("UTF-8"), Sanitize::Config::RESTRICTED).force_encoding("UTF-8")
      if val.gsub("&nbsp;", "").strip != ""
        @text = [
          header_entry.value.force_encoding("UTF-8"),
          @text,
        ].join("\n")
      end
    end

    # add global email footer
    footer_entry = EARMMS::ConfigModel.where(key: 'mass-email-footer').first
    if footer_entry && footer_entry.respond_to?(:value)
      val = Sanitize.fragment(footer_entry.value.force_encoding("UTF-8"), Sanitize::Config::RESTRICTED).force_encoding("UTF-8")
      if val.gsub("&nbsp;", "").strip != ""
        @text = [
          @text,
          "<hr>",
          footer_entry.value.force_encoding("UTF-8"),
        ].join("\n")
      end
    end

    @text = [
      @text,
      "<hr>",
    ]

    @footer.split("\n").each do |line|
      @text << "<p>#{line}</p>"
    end

    @text = @text.map{|x| x.force_encoding("UTF-8")}.join("\n").force_encoding("UTF-8")

    d = EARMMS::MassEmail.new(creation: Time.now, user: current_user.id)
    d.save
    d.view_token_generate!
    d.encrypt(:target, @selected)
    d.encrypt(:subject, @subject)
    d.encrypt(:content, @text)
    d.save

    plain_text = [
      "Sorry, but this email can only be viewed in HTML mode. ",
      "To view this email, either allow HTML email in your email client, or ",
      "visit the following link in a browser: ",
      d.view_token_link,
    ].join('')

    viewlink_footer = [
      '<div style="display:block;width:100%;text-align:center;font-size:0.8em;">',
      "<a style=\"color:grey !important;\" href=\"#{d.view_token_link}\">",
      'To view this email in a browser, click here.',
      '</a></div>',
    ].join('')

    qm = EARMMS::EmailQueue.new(creation: Time.now)
    qm.save
    qm.queue_status = 'queued'
    qm.encrypt(:recipients, JSON.generate({"type" => "mass_email_target", "target" => @selected}))
    qm.encrypt(:subject, @subject)
    qm.encrypt(:content, plain_text)
    qm.encrypt(:content_html, [@text, viewlink_footer].join("\n").force_encoding("UTF-8"))
    qm.save

    d.email_queue_id = qm.id
    d.save

    haml :'mass_email/success'
  end

  get '/view/:id' do |id|
    next halt 404 unless logged_in?
    next halt 404 unless has_role?('email:access') || is_group_admin?(:include_email_privs => true)

    @title = "Mass emailing"
    @user = current_user
    @email = EARMMS::MassEmail[id.to_i]
    unless @email
      next halt 404
    end

    @targets = get_email_targets
    if @targets.empty?
      next haml :'mass_email/no_targets'
    end

    target = @email.decrypt(:target)
    target_h = @targets.select{|x| x[:target] == target}.first
    target_name = "unknown"
    target_name = target_h[:name] if target_h && target_h.key?(:name)

    queue_status = queue_status_friendly = 'unknown'
    queued = EARMMS::EmailQueue[@email.email_queue_id]
    queue_status = queued.queue_status if queued

    case queue_status
    when "queued"
      queue_status_friendly = "Email is in send queue"
    when "sending"
      queue_status_friendly = "Email is currently being sent"
    when "complete"
      queue_status_friendly = "Email sent successfully"
    when "error"
      queue_status_friendly = "An error occurred while sending the email"
    end

    @data = {
      :id => id.to_i,
      :created => @email.creation,
      :target => target,
      :target_name => target_name.force_encoding("UTF-8"),
      :subject => @email.decrypt(:subject).force_encoding("UTF-8"),
      :content => @email.decrypt(:content).force_encoding("UTF-8"),
      :queue_status => queue_status_friendly,
    }

    haml :'mass_email/view'
  end
end
