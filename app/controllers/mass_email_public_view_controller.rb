class EARMMS::MassEmailPublicViewController < EARMMS::ApplicationController
  get '/:token' do |token|
    token = token.strip.downcase if token.is_a?(String)
    if token.nil? || token.empty?
      next halt 404
    end

    @email = EARMMS::MassEmail.where(view_token: token).first
    if @email.nil?
      next halt 404
    end

    @title = "Email sent #{@email.creation.strftime("%Y-%m-%d %H:%M")}"

    @data = {
      :created => @email.creation,
      :subject => @email.decrypt(:subject).force_encoding("UTF-8"),
      :content => @email.decrypt(:content).force_encoding("UTF-8"),
    }

    haml :'mass_email/public_view'
  end
end
