class EARMMS::MembershipAdminAlertsController < EARMMS::MembershipAdminController
  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:access'

    @alerts = {
      :dismiss_action => '/admin/alerts',
      :alerts => EARMMS::Alert.non_actioned_grouped_for_section(:membership)
    }

    @title = "Membership alerts"
    haml :'membership_admin/alerts/index'
  end

  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:access'
    next halt 400, "no action specified" unless request.params.key?("action")

    case request.params["action"].strip.downcase
    when "dismiss"
      next halt 400, "no alert specified" unless request.params.key?("alertid")
      alertid = request.params["alertid"].strip.to_i
      alert = EARMMS::Alert[alertid]
      next halt 400, "invalid alert id" unless alert
      next halt 400, "alert section mismatch" unless alert.section.downcase == "membership"
      alert.dismiss!

      flash(:success, "Dismissed alert #{alert.id}")

      # Update sidebar alert count (since this is retrieved before the dismissal)
      @membership_alerts = EARMMS::Alert.where(section: "membership", actioned: false).count
    else
       next halt 400, "invalid action"
    end

    @alerts = {
      :dismiss_action => '/admin/alerts',
      :alerts => EARMMS::Alert.non_actioned_grouped_for_section(:membership)
    }

    @title = "Membership alerts"
    haml :'membership_admin/alerts/index'
  end

  get '/page/:page' do |page|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:access'

    page = page.strip.to_i
    @alerts = {
      :dismissed => true,
      :alerts => EARMMS::Alert.actioned_page_for_section(:membership, page)
    }

    @title = "Membership alerts"
    haml :'membership_admin/alerts/page'
  end
end
