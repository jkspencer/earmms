class EARMMS::MembershipAdminBranchController < EARMMS::MembershipAdminController
  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch'

    @branches = EARMMS::Branch.all.map do |b|
      {
        :id => b.id,
        :name => b.decrypt(:name).force_encoding("UTF-8"),
      }
    end

    @title = "Branches"
    haml :'membership_admin/branch/index'
  end
end
