class EARMMS::MembershipAdminBranchEditController < EARMMS::MembershipAdminController
  helpers EARMMS::MembershipAdminBranchEditHelpers

  get '/:branchid' do |branchid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch'

    id = branchid.strip.to_i
    next halt 400, "no branch id" unless id > 0

    @branch = EARMMS::Branch[id]
    next halt 404, "invalid branch" unless @branch

    @title = "Branch #{@branch.decrypt(:name)} (id #{@branch.id})".force_encoding("UTF-8")
    haml :'membership_admin/branch/edit'
  end

  post '/:branchid' do |branchid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch'

    id = branchid.strip.to_i
    next halt 400, "no branch id" unless id > 0

    @branch = EARMMS::Branch[id]
    next halt 404, "invalid branch" unless @branch

    next halt 400, "no action" unless request.params.key?("action")
    action = request.params["action"].strip.downcase

    case action
    when "edit"
      next error_page("Branch name was not provided") unless request.params.key?("name")
      name = request.params["name"].strip
      next error_page("Branch name was not provided") if name == ""

      oldname = @branch.decrypt(:name).force_encoding("UTF-8")
      @branch.encrypt(:name, name)
      @branch.save

      flash :success, "Branch #{oldname} was successfully renamed to #{name}".force_encoding("UTF-8")

    when "export"
      EARMMS::Workers::AttendanceExport.queue(:type => 'branch', :branch => @branch.id, :requesting_user => current_user.id)
      flash :success, "An export of attendance records for this branch will be emailed to you."

    else
      next halt 400, "invalid action"
    end

    @title = "Branch #{@branch.decrypt(:name)} (id #{@branch.id})".force_encoding("UTF-8")
    haml :'membership_admin/branch/edit'
  end
end
