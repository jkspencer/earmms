class EARMMS::MembershipAdminBranchMeetingAttendanceController < EARMMS::MembershipAdminController
  get '/:branchid/:meetingid' do |branchid, meetingid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch'

    @branch = EARMMS::Branch[branchid.to_i]
    next halt 404 unless @branch
    @branch_name = @branch.decrypt(:name).force_encoding("UTF-8")

    meeting = EARMMS::BranchMeeting[meetingid.to_i]
    next halt 404 unless meeting
    next halt 404 unless meeting.branch == @branch.id

    @meeting = {
      :meeting => meeting,
      :id => meeting.id,
      :datetime => Time.parse(meeting.decrypt(:datetime)),
      :location => meeting.decrypt(:location).force_encoding("UTF-8"),
      :notes => meeting.decrypt(:notes).force_encoding("UTF-8"),
    }

    @attendance = EARMMS::BranchMeetingAttendanceRecord.where(meeting: meeting.id).map do |at|
      profile = EARMMS::Profile[at.decrypt(:profile).to_i]
      next nil unless profile
      user = EARMMS::User[profile.user]
      next nil unless user

      status = nil
      case at.decrypt(:attendance_status)
      when "present"
        status = t(:'attendance/present')
      when "apologies"
        status = t(:'attendance/apologies')
      else
        status = t(:'attendance/unknown')
      end

      {
        :user => user,
        :profile => profile,
        :at => at,

        :name => profile.decrypt(:full_name).force_encoding("UTF-8"),
        :status => status,
      }
    end.compact

    @title = t(:'admin/meeting_attendance', :branch => @branch_name, :date => @meeting[:datetime].strftime("%Y-%m-%d"))
    haml :'membership_admin/branch/meetings/attendance'
  end

  post '/:branchid/:meetingid/export' do |branchid, meetingid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch'

    branch = EARMMS::Branch[branchid.to_i]
    next halt 404 unless branch

    meeting = EARMMS::BranchMeeting[meetingid.to_i]
    next halt 404 unless meeting
    next halt 404 unless meeting.branch == branch.id

    EARMMS::Workers::AttendanceExport.queue :type => "meeting", :meeting => meeting.id, :requesting_user => current_user.id
    flash :success, t(:'admin/meeting_attendance_export_ok')

    next redirect "/admin/branch/meetings/attendance/#{branchid}/#{meetingid}"
  end
end
