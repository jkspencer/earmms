class EARMMS::MembershipAdminBranchMeetingsController < EARMMS::MembershipAdminController
  get '/:branchid' do |branchid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch'

    @branch = EARMMS::Branch[branchid.to_i]
    next halt 404 unless @branch
    @branch_name = @branch.decrypt(:name).force_encoding("UTF-8")

    @meetings = EARMMS::BranchMeeting.where(branch: @branch.id).map do |m|
      {
        :id => m.id,
        :datetime => Time.parse(m.decrypt(:datetime)),
        :location => m.decrypt(:location).force_encoding("UTF-8"),
        :notes => m.decrypt(:notes).force_encoding("UTF-8"),
      }
    end

    @title = "Branch meetings for #{@branch_name}".force_encoding("UTF-8")
    haml :'membership_admin/branch/meetings/list'
  end

  get '/:branchid/:meetingid' do |branchid, meetingid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch'

    @branch = EARMMS::Branch[branchid.to_i]
    next halt 404 unless @branch
    @branch_name = @branch.decrypt(:name).force_encoding("UTF-8")

    meeting = EARMMS::BranchMeeting[meetingid.to_i]
    next halt 404 unless meeting
    next halt 404 unless meeting.branch == @branch.id

    @meeting = {
      :meeting => meeting,
      :id => meeting.id,
      :datetime => Time.parse(meeting.decrypt(:datetime)),
      :location => meeting.decrypt(:location).force_encoding("UTF-8"),
      :notes => meeting.decrypt(:notes).force_encoding("UTF-8"),
    }

    @title = "#{@branch_name} meeting on #{@meeting[:datetime].strftime("%Y-%m-%d")}".force_encoding("UTF-8")
    haml :'membership_admin/branch/meetings/meeting'
  end
end
