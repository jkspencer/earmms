class EARMMS::MembershipAdminBranchNewController < EARMMS::MembershipAdminController
  helpers EARMMS::MembershipAdminBranchNewHelpers

  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch'

    @title = "New branch"
    haml :'membership_admin/branch/new'
  end

  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:branch'

    next error_page("A branch name was not provided.") unless request.params.key?("name")
    name = request.params["name"].strip
    next error_page("A branch name was not provided.") if name == ""

    branch = EARMMS::Branch.new
    branch.save
    branch.encrypt(:name, name)
    branch.save

    flash :success, "A new branch with name #{name} was created as ID #{branch.id}".force_encoding("UTF-8")
    next redirect "/admin/branch/edit/#{branch.id}"
  end
end
