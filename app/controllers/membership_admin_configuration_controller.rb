class EARMMS::MembershipAdminConfigurationController < EARMMS::MembershipAdminController
  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'
    @title = "Configuration"

    @maintenance_enabled = is_maintenance?()
    @signups_enabled = true
    signups_entry = EARMMS::ConfigModel.where(key: 'signups').first
    if signups_entry
      @signups_enabled = (signups_entry.value == 'yes')
    end

    haml :'membership_admin/config/index'
  end

  post '/toggle-signups' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'

    @entry = EARMMS::ConfigModel.where(key: 'signups').first
    unless @entry
      @entry = EARMMS::ConfigModel.new(key: 'signups', type: 'bool', value: 'yes')
    end
    @entry.value = (@entry.value == 'yes' ? 'no' : 'yes')
    @entry.save

    message = "config entry #{@entry.key.inspect} was set to #{@entry.value.inspect}".force_encoding("UTF-8")
    gen_alert :system, :config_change, message

    flash :success, "#{(@entry.value == 'yes' ? 'Enabled' : 'Disabled')} signups."
    redirect '/admin/config'
  end

  post '/toggle-maintenance' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'

    @entry = EARMMS::ConfigModel.where(key: 'maintenance').first
    unless @entry
      @entry = EARMMS::ConfigModel.new(key: 'maintenance', type: 'bool', value: 'no')
    end
    @entry.value = (@entry.value == 'yes' ? 'no' : 'yes')
    @entry.save

    message = "config entry #{@entry.key.inspect} was set to #{@entry.value.inspect}".force_encoding("UTF-8")
    gen_alert :system, :config_change, message

    flash :success, "#{(@entry.value == 'yes' ? 'Enabled' : 'Disabled')} maintenance."
    redirect '/admin/config'
  end
end
