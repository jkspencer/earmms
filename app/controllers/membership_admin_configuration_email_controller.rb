class EARMMS::MembershipAdminConfigurationEmailController < EARMMS::MembershipAdminController
  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'
    @title = "Configuration: Email header and footer"

    @header_entry = EARMMS::ConfigModel.where(key: 'mass-email-header').first
    @footer_entry = EARMMS::ConfigModel.where(key: 'mass-email-footer').first

    haml :'membership_admin/config/email'
  end

  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'
    @title = "Configuration: Email header and footer"

    @header_entry = EARMMS::ConfigModel.where(key: 'mass-email-header').first
    unless @header_entry
      @header_entry = EARMMS::ConfigModel.new(key: 'mass-email-header', type: 'text', value: '')
    end

    @footer_entry = EARMMS::ConfigModel.where(key: 'mass-email-footer').first
    unless @footer_entry
      @footer_entry = EARMMS::ConfigModel.new(key: 'mass-email-footer', type: 'text', value: '')
    end

    if request.params.key?("header")
      header = request.params["header"].strip
      if @header_entry.value.strip != header
        @header_entry.value = request.params["header"].strip
        @header_entry.save

        message = "config entry #{@header_entry.key.inspect} was set to #{@header_entry.value.inspect}".force_encoding("UTF-8")
        gen_alert :system, :config_change, message
      end
    end

    if request.params.key?("footer")
      footer = request.params["footer"].strip
      if @footer_entry.value.strip != footer
        @footer_entry.value = footer
        @footer_entry.save

        message = "config entry #{@footer_entry.key.inspect} was set to #{@footer_entry.value.inspect}".force_encoding("UTF-8")
        gen_alert :system, :config_change, message
      end
    end

    flash :success, "Email header and footer updated."

    haml :'membership_admin/config/email'
  end
end
