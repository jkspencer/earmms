class EARMMS::MembershipAdminConfigurationKaupapaController < EARMMS::MembershipAdminController
  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'
    @title = "Configuration: Kaupapa prompt"

    @kaupapa_entry = EARMMS::ConfigModel.where(key: 'kaupapa-prompt').first

    haml :'membership_admin/config/kaupapa'
  end

  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:config'
    @title = "Configuration: Kaupapa prompt"

    @kaupapa_entry = EARMMS::ConfigModel.where(key: 'kaupapa-prompt').first

    next halt 400, "invalid action" unless request.params.key?("action")
    action = request.params["action"].strip.downcase
    if action == "edit"
      unless @kaupapa_entry
        @kaupapa_entry = EARMMS::ConfigModel.new(key: 'kaupapa-prompt', type: 'bool')
      end

      unless request.params.key?("text")
        flash :error, "No text was provided."
        next haml :'membership_admin/config/kaupapa'
      end

      text = request.params["text"]
      if text.strip == ""
        flash :error, "No text was provided."
        next haml :'membership_admin/config/kaupapa'
      end

      @kaupapa_entry.value = text
      @kaupapa_entry.save

      message = "config entry #{@kaupapa_entry.key.inspect} was set to #{@kaupapa_entry.value.inspect}".force_encoding("UTF-8")
      gen_alert :system, :config_change, message
      flash :success, "The kaupapa prompt text was updated."

    elsif action == "disable"
      unless request.params.key?("disable-confirm") && request.params["disable-confirm"].strip.downcase == "on"
        flash :error, "Not disabling kaupapa prompt as the confirmation box was not checked"
        next haml :'membership_admin/config/kaupapa'
      end

      @kaupapa_entry.delete
      @kaupapa_entry = nil

      message = "config entry #{"kaupapa-prompt".inspect} was set to #{nil.inspect}".force_encoding("UTF-8")
      gen_alert :system, :config_change, message
      flash :success, "The kaupapa prompt was disabled."

    else
      next halt 400, "invalid action"
    end

    haml :'membership_admin/config/kaupapa'
  end
end
