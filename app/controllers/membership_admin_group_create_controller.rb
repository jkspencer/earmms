class EARMMS::MembershipAdminGroupCreateController < EARMMS::MembershipAdminController
  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:group'

    unless request.params.key?("name")
      flash :error, "You must provide a name for the new group."
      next redirect '/admin/group'
    end

    name = request.params["name"].strip
    if name == ""
      flash :error, "You must provide a name for the new group."
      next redirect '/admin/group'
    end
    name = name.force_encoding("UTF-8")

    type = request.params["type"].strip
    if !(["caucus", "working_group"].include?(type))
      flash :error, "You must select a valid type for the new group."
      next redirect '/admin/group'
    end

    group = EARMMS::Group.new(type: type)
    group.save
    group.encrypt(:name, name)
    group.save

    flash :success, "A new group was created with the name #{name}.".force_encoding("UTF-8")
    redirect "/admin/group/edit/#{group.id}"
  end
end
