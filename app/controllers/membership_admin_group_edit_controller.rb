class EARMMS::MembershipAdminGroupEditController < EARMMS::MembershipAdminController
  helpers EARMMS::GroupHelpers

  get '/:id' do |id|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:group'

    @group = EARMMS::Group[id.to_i]
    next halt 404 unless @group

    @name = @group.decrypt(:name).force_encoding("UTF-8")
    @description = @group.decrypt(:description).force_encoding("UTF-8")
    @is_joinable = @group.is_joinable
    @is_restricted_to_member = @group.restrict_to_member
    @agreement = @group.decrypt(:user_agreement).force_encoding("UTF-8")
    @members = get_group_members(@group)
    @title = "Group: #{@name}".force_encoding("UTF-8")

    haml :'membership_admin/group/edit'
  end

  post '/:id' do |id|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:group'
    next halt 400, "no action specified" unless request.params.key?("action")

    @group = EARMMS::Group[id.to_i]
    next halt 404 unless @group

    @name = @group.decrypt(:name).force_encoding("UTF-8")
    @description = @group.decrypt(:description).force_encoding("UTF-8")
    @is_joinable = @group.is_joinable
    @is_restricted_to_member = @group.restrict_to_member
    @agreement = @group.decrypt(:user_agreement).force_encoding("UTF-8")
    @members = get_group_members(@group)
    @title = "Group: #{@name}".force_encoding("UTF-8")

    action = request.params["action"].strip.downcase
    if action == "edit"
      if request.params.key?("name")
        @name = request.params["name"].strip.force_encoding("UTF-8")
        @title = "Group: #{@name}".force_encoding("UTF-8")
        @group.encrypt(:name, request.params["name"].strip)
      end

      if request.params.key?("description")
        @description = request.params["description"].strip.force_encoding("UTF-8")
        @group.encrypt(:description, request.params["description"].strip)
      end

      if request.params.key?("type")
        type = request.params["type"].strip
        unless (["caucus", "working_group"].include?(type))
          flash :error, "You must select a valid type for the group."
          next haml :'membership_admin/group/edit'
        end

        @group.type = type
      end

      @is_joinable = @group.is_joinable = (request.params["is_joinable"]&.strip&.downcase == "on")
      @is_restricted_to_member = @group.restrict_to_member = (request.params["is_restricted"]&.strip&.downcase == "on")

      if request.params.key?("agreement")
        @agreement = request.params["agreement"].strip.force_encoding("UTF-8")
        @group.encrypt(:user_agreement, @agreement)
      end

      @group.save
      flash :success, "The group was updated."

    elsif action == "user_add"
      error_msg = "Invalid user ID."
      unless request.params.key?("user") && request.params["user"].strip != ""
        flash :error, error_msg
        next haml :'membership_admin/group/edit'
      end

      uid = request.params["user"].strip.to_i
      user = EARMMS::User[uid]
      unless user
        flash :error, error_msg
        next haml :'membership_admin/group/edit'
      end

      profile = EARMMS::Profile.for_user(user)
      unless profile
        flash :error, error_msg
        next haml :'membership_admin/group/edit'
      end

      if @is_restricted_to_member
        if profile.decrypt(:membership_status)&.strip&.downcase != "member"
          flash :error, "#{profile.decrypt(:full_name)} (user ID #{user.id}) is not an organisation member, and this group is restricted to organisation members only.".force_encoding("UTF-8")
          next haml :'membership_admin/group/edit'
        end
      end

      # check if this user is in the group already
      if @members.keys.include?(user.id)
        flash :error, "#{profile.decrypt(:full_name)} (user ID #{user.id}) is already a member of this group!".force_encoding("UTF-8")
        next haml :'membership_admin/group/edit'
      end

      cm = EARMMS::GroupMember.new(group: @group.id)
      cm.save
      cm.encrypt(:user, user.id)
      cm.encrypt(:roles, "")
      cm.save

      EARMMS::GroupMemberFilter.clear_filters_for(cm)
      EARMMS::GroupMemberFilter.create_filters_for(cm)

      flash :success, "#{profile.decrypt(:full_name)} (user ID #{user.id}) was added to the group.".force_encoding("UTF-8")

    elsif action == "user_promote"
      unless request.params.key?("user") && request.params["user"].strip != ""
        next halt 400, "invalid user"
      end

      uid = request.params["user"].strip.to_i
      user = EARMMS::User[uid]
      next halt 400, "invalid user" unless user

      profile = EARMMS::Profile.for_user(user)
      next halt 400, "invalid user" unless profile

      cm = EARMMS::GroupMemberFilter.perform_filter(:user, user.id.to_s).map do |cmf|
        cm = EARMMS::GroupMember[cmf.group_member]
        next nil unless cm.group == @group.id

        cm
      end.compact.first

      roles = cm.decrypt(:roles).split(",").map do |r|
        r.strip.downcase
      end

      if roles.include?("admin")
        roles.delete("admin")
        flash :success, "Successfully demoted #{profile.decrypt(:full_name)} (user ID #{user.id})".force_encoding("UTF-8")
      else
        roles << "admin"
        flash :success, "Successfully promoted #{profile.decrypt(:full_name)} (user ID #{user.id})".force_encoding("UTF-8")
      end

      cm.encrypt(:roles, roles.join(","))
      cm.save

      EARMMS::GroupMemberFilter.clear_filters_for(cm)
      EARMMS::GroupMemberFilter.create_filters_for(cm)

    elsif action == "user_remove"
      unless request.params.key?("user") && request.params["user"].strip != ""
        next halt 400, "invalid user"
      end

      uid = request.params["user"].strip.to_i
      user = EARMMS::User[uid]
      next halt 400, "invalid user" unless user

      profile = EARMMS::Profile.for_user(user)
      next halt 400, "invalid user" unless profile

      cm = EARMMS::GroupMemberFilter.perform_filter(:user, user.id.to_s).map do |cmf|
        cm = EARMMS::GroupMember[cmf.group_member]
        next nil unless cm.group == @group.id

        cm
      end.compact.first

      roles = cm.decrypt(:roles).split(",").map do |r|
        r.strip.downcase
      end

      if roles.include?("admin")
        flash :error, "You can't delete an admin of the group. Please demote the user first."
        next haml :'membership_admin/group/edit'
      end

      EARMMS::GroupMemberFilter.clear_filters_for(cm)
      cm.delete

      flash :success, "Successfully removed #{profile.decrypt(:full_name)} (user ID #{user.id})".force_encoding("UTF-8")

    elsif action == "remove_group"
      confirm = (request.params.key?("remove-confirm") && request.params["remove-confirm"].strip.downcase == "on")
      if confirm
        EARMMS::GroupMember.where(group: @group.id).all.each do |cm|
          EARMMS::GroupMemberFilter.clear_filters_for(cm)
          cm.destroy
        end

        @group.destroy

        flash :success, "The group has been removed."
        next redirect "/admin/group"

      else
        flash :error, "You must check the box to confirm removing the group."
        next haml :'membership_admin/group/edit'
      end

    else
      next halt 400, "invalid action"
    end

    @members = get_group_members(@group)

    haml :'membership_admin/group/edit'
  end
end
