class EARMMS::MembershipAdminImportController < EARMMS::MembershipAdminController
  helpers EARMMS::MembershipAdminImportHelpers

  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:import'

    @first_branch = EARMMS::Branch.order(Sequel.asc(:id)).first
    unless @first_branch
      flash :error, "There are no branches, so this import will fail. Please create branches before attempting an import."
    end

    @title = "Import users"
    haml :'membership_admin/import/index'
  end

  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:import'

    next halt 400, "no csv data" unless request.params.key?("csv")
    @csv = request.params["csv"].force_encoding("UTF-8")
    @output_data = import_parse_csv(@csv)

    @title = "Import users"
    if request.params.key?("action") && request.params["action"].strip.downcase == "do-import"
      if !request.params.key?("confirm") || request.params["confirm"].downcase != "on"
        flash :error, "Please check the confirmation box."
        next haml :'membership_admin/import/confirm'
      end

      send_reset = (request.params.key?("send-reset") && request.params["send-reset"].downcase == "on")

      # custom fields
      custom_fields = custom_profile_fields.map do |f|
        [f[:name], f[:default] || nil]
      end.to_h
      custom_field_json = JSON.generate(custom_fields)

      @imported_users = []
      @output_data.each do |row|
        existing = false
        email = row[:email_address]
        if email
          email = email.strip.downcase
          u = EARMMS::User.where(email: email).first
          if u
            existing = true
          end
        end

        unless existing
          u = EARMMS::User.new
          u.email = email
          u.password = EARMMS::Crypto.generate_token
          u.creation = Time.now
          u.last_login = Time.now
          u.signup_extended_status = "complete"
          u.save

          u.send_password_reset! if send_reset
        end

        p = EARMMS::Profile.where(user: u.id).first
        unless p
          p = EARMMS::Profile.new
          p.user = u.id
          p.save
        end

        p.encrypt(:full_name, row[:full_name])
        p.encrypt(:address, row[:address])
        p.encrypt(:phone_home, row[:home_phone])
        p.encrypt(:phone_mobile, row[:mobile_phone])
        p.encrypt(:membership_status, row[:membership_status])
        p.encrypt(:branch, row[:branch][0])
        p.encrypt(:custom_fields, custom_field_json)

        p.save

        EARMMS::ProfileFilter.clear_filters_for(p)
        EARMMS::ProfileFilter.create_filters_for(p)

        changelog_data = {
          :type => 'membership_status',
          :from => nil,
          :to => row[:membership_status],
        }

        changelog = EARMMS::ProfileChangelog.new(profile: p.id, created: row[:member_since])
        changelog.save
        changelog.encrypt(:data, JSON.dump(changelog_data))
        changelog.save

        @imported_users << {
          :user => u,
          :existing => existing,
          :profile => p,
        }
      end

      @existing_users = @imported_users.select do |x|
        x[:existing] == true
      end

      next haml :'membership_admin/import/result'
    else
      next haml :'membership_admin/import/confirm'
    end
  end
end
