class EARMMS::MembershipAdminIndexController < EARMMS::MembershipAdminController
  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:access'

    @membership_alerts = EARMMS::Alert.where(section: "membership", actioned: false).count
    @counts = {
      :members => EARMMS::ProfileFilter.perform_filter(:membership_status, "member").count,
      :supporters => EARMMS::ProfileFilter.perform_filter(:membership_status, "supporter").count,
      :branches => EARMMS::Branch.all.map do |branch|
        [
          branch.decrypt(:name).force_encoding("UTF-8"),
          EARMMS::ProfileFilter.perform_filter(:branch, "#{branch.id.to_s}:member").count,
        ]
      end.to_h,
    }

    @title = "Membership admin"
    haml :'membership_admin/index'
  end
end
