class EARMMS::MembershipAdminUserAttendanceController < EARMMS::MembershipAdminController
  get '/:id' do |uid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change'

    page = request.params.key?("page") ? request.params["page"].to_i : 1

    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user

    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    @user_name = @profile.decrypt(:full_name).force_encoding("UTF-8")

    @ds = EARMMS::BranchMeetingAttendanceRecordFilter
      .perform_filter(:profile, @profile.id.to_s)
      .order(Sequel.desc(:id))
      .paginate(page, 25)

    @current_page = @ds.current_page
    @total_pages = @ds.page_count

    @attendance = @ds.map do |entry|
      at = EARMMS::BranchMeetingAttendanceRecord[entry.attendance_record]
      next nil unless at

      meeting = EARMMS::BranchMeeting[at.meeting]
      next nil unless meeting

      branch = EARMMS::Branch[meeting.branch]
      next nil unless branch

      status = at.decrypt(:attendance_status)
      case status
      when "present"
        status = "Present"
      when "apologies"
        status = "Apologies"
      else
        status = "Unknown"
      end

      {
        :meetingid => meeting.id,
        :branch => branch,
        :branch_name => branch.decrypt(:name).force_encoding("UTF-8"),
        :datetime => Time.parse(meeting.decrypt(:datetime)),
        :status => status
      }
    end

    @attendance = @attendance.compact.sort{|a, b| b[:created] <=> a[:created]}

    haml :'membership_admin/user/attendance'
  end

  post '/:id/export' do |uid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change'

    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user

    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    EARMMS::Workers::AttendanceExport.queue(:type => "user", :user => @user.id, :requesting_user => current_user.id)
    flash :success, "An export for this user's attendance data will be emailed to you."

    redirect "/admin/user/attendance/#{uid}"
  end
end
