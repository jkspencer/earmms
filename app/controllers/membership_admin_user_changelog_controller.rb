class EARMMS::MembershipAdminUserChangelogController < EARMMS::MembershipAdminController
  get '/:id' do |uid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change'

    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user

    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    @user_name = @profile.decrypt(:full_name).force_encoding("UTF-8")

    @changelog_entries = EARMMS::ProfileChangelog.where(profile: @profile.id).map do |entry|
      data = entry.decrypt(:data)
      next nil unless data
      data = JSON.parse(data)

      out = {
        :type => data["type"],
        :created => entry.created,
      }

      case data["type"]
      when "membership_status"
        out[:type_friendly] = "Membership status change"
        out[:message] = "Status changed from \"#{data["from"]}\" to \"#{data["to"]}\"".force_encoding("UTF-8")
      when "branch_change"
        out[:type_friendly] = "Branch change"

        from_name = "unknown"
        from = EARMMS::Branch[data["from"].to_i] unless data["from"] == 'unknown'
        from_name = from.decrypt(:name) if from

        to_name = "unknown"
        to = EARMMS::Branch[data["to"].to_i] unless data["to"] == 'unknown'
        to_name = to.decrypt(:name) if to

        out[:message] = "Branch changed from #{from_name} (ID #{from ? from.id : 'unknown'}) to #{to_name} (ID #{to ? to.id : 'unknown'})".force_encoding("UTF-8")
      end

      out
    end

    @changelog_entries = @changelog_entries.compact.sort{|a, b| b[:created] <=> a[:created]}

    haml :'membership_admin/user/changelog'
  end
end
