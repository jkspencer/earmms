class EARMMS::MembershipAdminUserController < EARMMS::MembershipAdminController
  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change'

    @branches = EARMMS::Branch.all.map do |b|
      {
        :id => b.id,
        :name => b.decrypt(:name).force_encoding("UTF-8"),
      }
    end

    @custom_fields = settings.custom_user_fields.map do |f|
      field_desc = {
        :name => f[:name].to_s.force_encoding("UTF-8"),
        :friendly => f[:friendly].force_encoding("UTF-8"),
        :type => (f[:type].to_s || "text").force_encoding("UTF-8"),
        :required => f[:required] || false,
        :is_custom => true,
      }

      if f[:type] == 'select'
        field_desc[:select_options] = f[:select_options]
      end

      field_desc
    end

    @title = "Users"
    haml :'membership_admin/user/index'
  end
end
