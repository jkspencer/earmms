class EARMMS::MembershipAdminUserCreateController < EARMMS::MembershipAdminController
  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change'

    haml :'membership_admin/user/create'
  end

  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change'

    if !request.params.key?("user-name") || request.params["user-name"].strip == ""
      flash :error, "A name must be provided."
      next redirect '/admin/user'
    end

    name = request.params["user-name"].strip.force_encoding("UTF-8")

    user = EARMMS::User.new
    user.creation = Time.now
    user.save

    profile = EARMMS::Profile.new(user: user.id)
    profile.save
    profile.encrypt(:full_name, name)
    profile.encrypt(:membership_status, 'supporter')
    profile.save

    EARMMS::ProfileFilter.clear_filters_for(profile)
    EARMMS::ProfileFilter.create_filters_for(profile)

    flash :success, "The new user #{name.inspect} was created (uid #{user.id} pid #{profile.id})".force_encoding("UTF-8")
    redirect "/admin/user/edit/#{user.id}?back=/admin/user"
  end
end
