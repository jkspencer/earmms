class EARMMS::MembershipAdminUserEditController < EARMMS::MembershipAdminController
  helpers EARMMS::MembershipAdminUserEditHelpers

  before do
    @fields = [
      {name: "full_name", friendly: t(:full_name), type: "text", required: true},
      {name: "birthday", friendly: t(:birthday), type: "date", required: false},
      {name: "phone_home", friendly: t(:phone_home), type: "text", required: false},
      {name: "phone_mobile", friendly: t(:phone_mobile), type: "text", required: false},
      {name: "address", friendly: t(:address), type: "textarea", required: false},
      {name: "prisoner_number", friendly: t(:prisoner_number), type: "text", required: false},
    ]

    settings.custom_user_fields.each do |f|
      field_desc = {
        :name => f[:name].to_s.force_encoding("UTF-8"),
        :friendly => f[:friendly].force_encoding("UTF-8"),
        :type => (f[:type].to_s || "text").force_encoding("UTF-8"),
        :required => f[:required] || false,
        :default => f[:default] || nil,
        :select_options => f[:type] == 'select' ? f[:select_options] : nil,
        :display_only_if => f[:display_only_if] || nil,
        :display_only_if_membership_status => f[:display_only_if_membership_status] || nil,
        :is_custom => true,
      }

      @fields << field_desc
    end
  end

  get '/:uid' do |uid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change'

    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user

    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    @user_name = @profile.decrypt(:full_name).force_encoding("UTF-8")

    profile_custom_json = @profile.decrypt(:custom_fields)
    if profile_custom_json && profile_custom_json != ""
      profile_custom_json = JSON.parse(profile_custom_json)
    else
      profile_custom_json = {}
    end

    @values = @fields.map do |m|
      k = m[:name].force_encoding("UTF-8")
      v = nil

      if m[:is_custom]
        v = profile_custom_json[k]
      else
        v = @profile.decrypt(k.to_sym)
      end

      if v.nil? || v.empty?
        if m[:default]
          v = m[:default]
        end
      end

      [k, v&.force_encoding("UTF-8")]
    end.compact.to_h

    @membership_status = @profile.decrypt(:membership_status).strip
    @membership_status = nil unless EARMMS.membership_status_options.keys.include? @membership_status.to_sym
    @status_change_req = @profile.decrypt(:status_change_alert).strip.to_i
    @status_change_req = EARMMS::Alert[@status_change_req] unless @status_change_req == nil
    @branch_change_req = @profile.decrypt(:branch_change_alert).strip.to_i
    @branch_change_req = EARMMS::Alert[@branch_change_req] unless @branch_change_req == nil

    @user_is_nil = false
    if @user.email == nil
      @user_is_nil = true
    end

    @branches = EARMMS::Branch.all.map do |b|
      {
        :id => b.id,
        :name => b.decrypt(:name).force_encoding("UTF-8"),
      }
    end

    @branch = @profile.decrypt(:branch).strip.to_i
    @branch = nil unless @branches.map{|x| x[:id]}.include?(@branch)

    @title = "Editing user #{@profile.decrypt(:full_name)}".force_encoding("UTF-8")
    haml :'membership_admin/user/edit'
  end

  post '/:uid' do |uid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change'
    next halt 400, "no action" unless request.params.key?("action")

    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user

    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    @user_name = @profile.decrypt(:full_name).force_encoding("UTF-8")

    profile_custom_json = @profile.decrypt(:custom_fields)
    if profile_custom_json && profile_custom_json != ""
      profile_custom_json = JSON.parse(profile_custom_json)
    else
      profile_custom_json = {}
    end

    @values = @fields.map do |m|
      k = m[:name].force_encoding("UTF-8")
      v = nil

      if m[:is_custom]
        v = profile_custom_json[k]
      else
        v = @profile.decrypt(k.to_sym)
      end

      if v.nil? || v.empty?
        if m[:default]
          v = m[:default]
        end
      end

      [k, v&.force_encoding("UTF-8")]
    end.compact.to_h

    @membership_status = @profile.decrypt(:membership_status).strip
    @membership_status = nil unless EARMMS.membership_status_options.keys.include? @membership_status.to_sym
    @status_change_req = @profile.decrypt(:status_change_alert).strip.to_i
    @status_change_req = EARMMS::Alert[@status_change_req] unless @status_change_req == nil
    @branch_change_req = @profile.decrypt(:branch_change_alert).strip.to_i
    @branch_change_req = EARMMS::Alert[@branch_change_req] unless @branch_change_req == nil

    @user_is_nil = false
    if @user.email == nil
      @user_is_nil = true
    end

    @branches = EARMMS::Branch.all.map do |b|
      {
        :id => b.id,
        :name => b.decrypt(:name).force_encoding("UTF-8"),
      }
    end

    case request.params["action"].strip.downcase
    when "profile_edit"
      required = @fields.reject{|f| !f[:required]}
      has_all_required = required.map do |f|
        next false unless request.params.key?(f[:name])
        next false unless request.params[f[:name]]
        true
      end.all?

      unless has_all_required
        flash(:error, "A required field was not provided.")
        next haml :'user_settings/index'
      end

      @fields.each do |field|
        next unless request.params.key?(field[:name])
        if field[:is_custom]
          profile_custom_json[field[:name]] = request.params[field[:name]].force_encoding("UTF-8")
        else
          @profile.encrypt(field[:name], request.params[field[:name]].force_encoding("UTF-8"))
        end
      end

      @profile.encrypt(:custom_fields, JSON.dump(profile_custom_json))
      @profile.save

      flash :success, "The profile was successfully updated."

    when "membership_edit"
      next halt 400, "status not given" unless request.params.key?("status")

      oldstatus = @profile.decrypt(:membership_status)
      status = request.params["status"].strip.downcase.to_sym

      changelog_data = {
        :type => 'membership_status',
        :from => oldstatus,
        :to => status,
      }

      changelog = EARMMS::ProfileChangelog.new(profile: @profile.id, created: Time.now)
      changelog.save
      changelog.encrypt(:data, JSON.dump(changelog_data))
      changelog.save

      if status == :unknown
        @profile.membership_status = nil
        @profile.save

        flash :success, "Membership status set to 'unknown'"
      else
        next halt 400, "invalid status" unless EARMMS.membership_status_options.keys.include?(status)

        @profile.encrypt(:membership_status, status.to_s)
        @profile.save

        flash :success, "Membership status set to #{EARMMS.membership_status_options[status]}"
      end

      if @status_change_req
        @profile.status_change_alert = nil
        @profile.save
        @status_change_req.dismiss!
        @status_change_req = nil

        flash :success, "The pending membership status alert for this user has been dismissed."
      end

    when "branch_edit"
      next halt 400, "branch not given" unless request.params.key?("branch")

      changelog_data = {
        :type => 'branch_change',
        :from => @profile.decrypt(:branch),
        :to => request.params["branch"],
      }

      changelog = EARMMS::ProfileChangelog.new(profile: @profile.id, created: Time.now)
      changelog.save
      changelog.encrypt(:data, JSON.dump(changelog_data))
      changelog.save

      if request.params["branch"].strip.downcase == "unknown"
        @profile.branch = nil
        @profile.save
        flash :success, "Profile branch was set to 'unknown'"
      else
        branchid = request.params["branch"].strip.to_i
        b = @branches.map{|x| x[:id]}.include?(branchid)
        unless b
          next halt 400, "invalid branch"
        end

        branch_name = @branches.reject{|b| b[:id] != branchid}.first[:name].force_encoding("UTF-8")

        @profile.encrypt(:branch, branchid.to_s)
        @profile.save

        # Alert branch admins about the user joining/leaving their branch
        alert_branch_admin_user_change(@profile, changelog_data[:from].to_i, branchid.to_i)

        flash :success, "Profile branch was set to #{branch_name}".force_encoding("UTF-8")
      end

      if @branch_change_req
        @profile.branch_change_alert = nil
        @profile.save
        @branch_change_req.dismiss!
        @branch_change_req = nil

        flash :success, "The pending branch change alert for this user has been dismissed."
      end

    when "disable_2fa"
      if request.params.key?("confirm") && request.params["confirm"] == "on"
        @user.totp_enabled = false
        @user.totp_secret = nil
        @user.save

        EARMMS::U2FRegistration.where(user: @user.id).delete

        message = "Two-factor authentication has been disabled for #{@user_name} (uid #{@user.id})".force_encoding("UTF-8")
        gen_alert :membership, :admin_force_edit_user, message
        flash :success, message
      else
        flash :error, "Not disabling 2FA because the confirm box was not checked"
      end

    when "change_email"
      if request.params.key?("email")
        email = request.params["email"].strip.downcase
        if EARMMS::User.where(email: email).count > 0
          flash :error, "A user with this email address already exists"
        else
          oldemail = @user.email
          @user.email = email
          @user.save

          message = "Email address for uid #{@user.id} changed from #{oldemail} to #{email}.".force_encoding("UTF-8")
          gen_alert :membership, :admin_force_edit_user, message
          flash :success, message
        end
      else
        flash :error, "An email address was not provided"
      end

    when "remove_email"
      if request.params.key?("confirm") && request.params["confirm"] == "on"
        oldemail = @user.email
        @user.email = nil
        @user.password_hash = nil
        @user.totp_enabled = false
        @user.totp_secret = nil
        @user.save

        EARMMS::U2FRegistration.where(user: @user.id).delete
        EARMMS::Token.where(user: @user.id, use: 'session').update(valid: false)

        message = "Email address for user #{@user.id} removed (was #{oldemail}). The user's password has been invalidated and two-factor authentication disabled.".force_encoding("UTF-8")
        gen_alert :membership, :admin_force_edit_user, message
        flash :success, message
      else
        flash :error, "Not removing email address because the confirm box was not checked"
      end

    when "pw_reset"
      if @user.email == nil
        flash :error, "Can't send a password reset to a user with no email address!"
      elsif request.params.key?("confirm") && request.params["confirm"] == "on"
        @user.password_hash = nil
        @user.send_password_reset!
        @user.save

        # invalidate all tokens
        current_id = current_user.id
        EARMMS::Token.where(user: @user.id, use: 'session').update(valid: false)
        if @user.id == current_id
          current_token = EARMMS::Token.where(token: session[:token]).first
          current_token.valid = true
          current_token.save
        end

        message = "Password for user #{@user.id} invalidated and password reset email sent"
        gen_alert :membership, :admin_force_edit_user, message
        flash :success, message
      else
        flash :error, "Not sending password reset because the confirm checkbox was not checked"
      end

    when "disable_account"
      reason = request.params["reason"]
      reason = reason.strip if reason.is_a?(String)

      if reason.nil? || reason == ""
        @user.disabled_reason = nil
        flash :success, "Account has been enabled."
      else
        @user.encrypt(:disabled_reason, reason)
        flash :success, "Account has been disabled."
      end

      @user.save

    when "delete_account"
      if request.params.key?("confirm") && request.params["confirm"] == "on"
        alert_message = "User #{@user_name} (uid #{@user.id}) has deleted their account. A summary will be emailed.".force_encoding("UTF-8")
        flash_message = "The account for user #{@user_name} (uid #{@user.id}) has been deleted. A summary will be emailed.".force_encoding("UTF-8")
        exception_message = "An exception occurred while deleting the user account. Please contact the system administrator and give them these details: uid #{@user.id} pid #{@profile.id}".force_encoding("UTF-8")

        begin
          @user.delete_account!
          gen_alert :membership, :user_deletion, alert_message
          flash :success, flash_message
          redirect '/admin/user'

        rescue => e
          puts "Error deleting uid #{@user.id} pid #{@profile.id}: #{e}"
          puts e.backtrace
          STDOUT.flush

          flash :error, exception_message
        end
      else
        flash :error, "Not deleting account because the confirm checkbox was not checked"
      end

    else
      next halt 400, "invalid action"
    end

    EARMMS::ProfileFilter.clear_filters_for(@profile)
    EARMMS::ProfileFilter.create_filters_for(@profile)

    @membership_status = @profile.decrypt(:membership_status).strip
    @membership_status = nil unless EARMMS.membership_status_options.keys.include? @membership_status.to_sym

    @branch = @profile.decrypt(:branch).strip.to_i
    @branch = nil unless @branches.map{|x| x[:id]}.include?(@branch)

    @user_is_nil = false
    if @user.email == nil
      @user_is_nil = true
    end

    @values = @fields.map do |m|
      k = m[:name].force_encoding("UTF-8")
      v = nil

      if m[:is_custom]
        v = profile_custom_json[k] || ''
      else
        v = @profile.decrypt(k.to_sym) || ''
      end

      [k, v&.force_encoding("UTF-8")]
    end.compact.to_h

    # Refresh alert count in case we dismissed any while editing this user
    @membership_alerts = EARMMS::Alert.where(section: "membership", actioned: false).count

    @title = "Editing user #{@user_name}".force_encoding("UTF-8")
    haml :'membership_admin/user/edit'
  end
end
