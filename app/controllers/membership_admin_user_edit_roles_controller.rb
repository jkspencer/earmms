class EARMMS::MembershipAdminUserEditRolesController < EARMMS::MembershipAdminController
  helpers EARMMS::MembershipAdminUserEditRolesHelpers

  get '/:uid' do |uid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change_roles'

    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user

    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    @user_name = @profile.decrypt(:full_name).force_encoding("UTF-8")
    @title = t :'admin/editing_user_roles', :user => @user_name

    @roles = get_available_roles(@user)

    haml :'membership_admin/user/edit_roles'
  end

  post '/:uid' do |uid|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change_roles'

    @user = EARMMS::User[uid.to_i]
    next halt 404 unless @user

    @profile = EARMMS::Profile.for_user(@user)
    next halt 404 unless @profile

    @user_name = @profile.decrypt(:full_name).force_encoding("UTF-8")
    @title = t :'admin/editing_user_roles', :user => @user_name

    @roles = get_available_roles(@user)

    # Check for two-factor
    unless @user.totp_enabled
      t = EARMMS::ConfigModel.where(key: '2fa-required-for-roles').first
      if t && t.value == "yes"
        flash :error, t(:'roles/can_not_grant_without_two_factor')
        next haml :'membership_admin/user/edit_roles'
      end
    end

    # Create a list of the roles to add and the roles to remove
    to_add = []
    to_remove = []
    @roles.each do |cat|
      cat[:roles].each do |role|
        rkey = "role__#{role[:role]}"
        if role[:has]
          if !request.params.key?(rkey) || request.params[rkey].strip.downcase != "on"
            to_remove << role[:role]
          end
        else
          if request.params.key?(rkey) && request.params[rkey].strip.downcase == "on"
            to_add << role[:role]
          end
        end
      end
    end

    # Action the changes
    to_add.each do |r|
      uhr = EARMMS::UserHasRole.new(user: @user.id, role: r)
      uhr.save
    end

    to_remove.each do |r|
      EARMMS::UserHasRole.where(user: @user.id, role: r).delete
    end

    flash :success, "Added #{to_add.length} roles, and removed #{to_remove.length} roles."

    # Refresh checkboxes
    @roles = get_available_roles(@user)

    haml :'membership_admin/user/edit_roles'
  end
end
