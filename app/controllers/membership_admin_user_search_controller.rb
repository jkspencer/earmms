class EARMMS::MembershipAdminUserSearchController < EARMMS::MembershipAdminController
  helpers EARMMS::MembershipAdminUserHelpers

  get '/expanded' do
    searchtype = request.params["searchtype"].strip.downcase
    query = request.params[searchtype]

    new_uri = Addressable::URI.parse('/admin/user/search')
    new_uri.query_values = {
      "searchtype" => searchtype,
      "query" => query,
    }

    redirect(new_uri.to_s)
  end

  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change'
    next halt 400, "no searchtype provided" unless request.params.key?("searchtype")
    next halt 400, "no query provided" unless request.params.key?("query")

    @searchtype = request.params["searchtype"].strip.downcase
    @query = request.params["query"].strip

    @current_uri = current_uri_with_query(@searchtype, @query)
    @custom_fields = custom_profile_fields()

    @sort = "userid"
    @sort = request.params["sort"].strip.downcase if request.params.key?("sort")
    @sort_reverse = false
    @sort_reverse = (request.params["sortrev"].strip.downcase == "on") if request.params.key?("sortrev")

    @results = do_search(@searchtype, @query)
    @results = do_search_get_data(@results)
    @results = do_search_sort(@results, @sort) if @results
    @results.reverse! if @sort_reverse && @results

    @title = t :'admin/search_users'
    haml :'membership_admin/user/list'
  end

  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'admin:user_change'
    next halt 400, "no searchtype provided" unless request.params.key?("searchtype")
    next halt 400, "no query provided" unless request.params.key?("query")
    next halt 400, "no action provided" unless request.params.key?("action")

    @searchtype = request.params["searchtype"].strip.downcase
    @query = request.params["query"].strip

    @current_uri = current_uri_with_query(@searchtype, @query)
    @custom_fields = custom_profile_fields()

    @sort = "userid"
    @sort = request.params["sort"].strip.downcase if request.params.key?("sort")
    @sort_reverse = false
    @sort_reverse = (request.params["sortrev"].strip.downcase == "on") if request.params.key?("sortrev")

    @results = do_search(@searchtype, @query)
    @results = do_search_get_data(@results)
    @results = do_search_sort(@results, @sort) if @results
    @results.reverse! if @sort_reverse && @results

    action = request.params["action"].strip.downcase
    if action == "as_text_email"
      content_type 'text/plain'
      next @results.map do |m|
        "#{m[:user_name]} <#{m[:user].email}>".force_encoding("UTF-8")
      end.compact.join(", ").force_encoding("UTF-8")

    elsif action == "export_search"
      type = request.params["export_type"]
      type = "text" if type.nil? || type == ""
      type = "text" unless %w[text csv].include? type
      type = type.to_sym

      EARMMS::Workers::SearchExport.queue({
        :searchtype => @searchtype,
        :query => @query,
        :exporttype => type,
        :requesting_user => current_user.id,
      })

      message = :'admin/search_export_ok'
      if type == :text
        message = :'admin/search_export_ok/text'
      elsif type == :csv
        message = :'admin/search_export_ok/csv'
      end

      flash :success, t(message)

    elsif action == "perform_mass_action"
      mass_action = request.params["mass_action"]&.strip&.downcase
      if mass_action.nil? || mass_action.empty?
        next halt 400, "invalid mass action"
      end

      EARMMS::Workers::MassAction.queue({
        :searchtype => @searchtype,
        :query => @query,
        :action => mass_action,
      })

      flash :success, t(:'admin/mass_action/success')

    else
      next halt 400, "invalid action"
    end

    @title = t :'admin/search_users'
    haml :'membership_admin/user/list'
  end
end
