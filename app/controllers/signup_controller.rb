require 'rotp'

class EARMMS::SignupController < EARMMS::ApplicationController
  helpers EARMMS::SignupHelpers

  get '/' do
    @title = "Sign up"

    @invite = get_invite(request.params["invite"])
    unless @invite
      next haml :'auth/signup/disabled' unless signups_allowed?
    end

    haml :'auth/signup/index'
  end

  post '/' do
    redirect('/') if logged_in?

    @invite = get_invite(request.params["invite"])
    unless @invite
      next haml :'auth/signup/disabled' unless signups_allowed?
    end

    next error_page("You must enter an email address.") unless request.params.key?("email")
    next error_page("You must enter a password.") unless request.params.key?("password")
    next error_page("You must enter your password again.") unless request.params.key?("password-repeat")
    next error_page("Your passwords don't match.") unless request.params["password"] == request.params["password-repeat"]

    check_user = EARMMS::User.where(email: request.params["email"].downcase).first
    next error_page("A user already exists with this email address.") if check_user

    user = EARMMS::User.new
    user.email = request.params["email"].downcase
    user.password = request.params["password"]
    user.creation = Time.now
    user.last_login = Time.now
    user.signup_extended_status = "complete"
    user.save

    if @invite
      @invite.user = user.id
      @invite.valid = false
      @invite.save

      # roles
      data = JSON.parse(@invite.extra_data || '{}')
      if data.key?("roles") && data["roles"] != nil
        data["roles"].each do |r|
          uhr = EARMMS::UserHasRole.new(user: user.id, role: r)
          uhr.save
        end
      end
    end

    token = EARMMS::Token.generate_session(user)
    session[:token] = token.token
    token.save

    profile = EARMMS::Profile.new
    profile.save
    profile.user = user.id
    profile.encrypt(:membership_status, "supporter")
    profile.encrypt(:custom_fields, custom_field_json)
    profile.save

    if settings.extended_signup
      user.signup_extended_status = "profile"
      user.save

      next redirect("/auth/signup/ex/profile")

    else
      EARMMS::ProfileFilter.clear_filters_for(profile)
      EARMMS::ProfileFilter.create_filters_for(profile)

      flash(:success, "Welcome to #{site_name}!")
      next redirect('/')
    end
  end
end
