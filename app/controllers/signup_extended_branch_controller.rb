require 'rotp'

class EARMMS::SignupExtendedBranchController < EARMMS::ApplicationController
  helpers EARMMS::SignupHelpers

  get '/' do
    next redirect("/") unless settings.extended_signup
    next redirect("/auth/signup") unless logged_in?

    @title = "Sign up"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    signup_status = @user.signup_extended_status.strip.downcase
    next redirect("/") if signup_status == "complete"
    next redirect("/auth/signup/ex/#{signup_status}") unless signup_status == "branch"

    @branches = EARMMS::Branch.all.map do |b|
      {
        :id => b.id,
        :name => b.decrypt(:name).force_encoding("UTF-8"),
      }
    end

    haml :'auth/signup/extended/branch'
  end

  post '/' do
    next redirect("/") unless settings.extended_signup
    next redirect("/auth/signup") unless logged_in?

    @title = "Sign up"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    signup_status = @user.signup_extended_status.strip.downcase
    next redirect("/") if signup_status == "complete"
    next redirect("/auth/signup/ex/#{signup_status}") unless signup_status == "branch"

    @branches = EARMMS::Branch.all.map do |b|
      {
        :id => b.id,
        :name => b.decrypt(:name).force_encoding("UTF-8"),
      }
    end

    new_branch = EARMMS::Branch[request.params["branch"].to_i]
    unless new_branch
      flash :error, "An invalid branch was selected. Please try again."
      next haml :'auth/signup/extended/branch'
    end

    alert_data = {
      "from_branch" => "unknown",
      "from_branch_id" => 0,
      "to_branch" => new_branch.decrypt(:name).force_encoding("UTF-8"),
      "to_branch_id" => new_branch.id,
    }

    @pending = gen_alert :membership, :user_branch_change_req, JSON.dump(alert_data)
    @profile.encrypt(:branch_change_alert, @pending.id)
    @profile.save

    @user.signup_extended_status = "complete"
    @user.save

    redirect '/auth/signup/ex/complete'
  end
end
