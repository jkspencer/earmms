class EARMMS::SignupExtendedCompleteController < EARMMS::ApplicationController
  helpers EARMMS::SignupHelpers

  get '/' do
    next redirect("/") unless settings.extended_signup
    next redirect("/auth/signup") unless logged_in?

    @title = "Sign up"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    signup_status = @user.signup_extended_status.strip.downcase
    next redirect("/auth/signup/ex/#{signup_status}") unless signup_status == "complete"

    @status = @profile.decrypt(:membership_status)

    @pending_status = @profile.decrypt(:status_change_alert)
    @pending_status = nil if @pending_status == ""
    if @pending_status
      @pending_status = EARMMS::Alert[@pending_status.to_i]
    end

    @pending_branch = @profile.decrypt(:branch_change_alert)
    @pending_branch = nil if @pending_branch == ""
    if @pending_branch
      @pending_branch = EARMMS::Alert[@pending_branch.to_i]
    end

    haml :'auth/signup/extended/complete'
  end

  post '/' do
    next redirect("/") unless settings.extended_signup
    next redirect("/auth/signup") unless logged_in?

    @title = "Sign up"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    signup_status = @user.signup_extended_status.strip.downcase
    next redirect("/auth/signup/ex/#{signup_status}") unless signup_status == "complete"

    EARMMS::ProfileFilter.clear_filters_for(@profile)
    EARMMS::ProfileFilter.create_filters_for(@profile)

    redirect '/'
  end
end
