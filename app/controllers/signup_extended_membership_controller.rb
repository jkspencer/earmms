require 'rotp'

class EARMMS::SignupExtendedMembershipController < EARMMS::ApplicationController
  helpers EARMMS::SignupHelpers

  get '/' do
    next redirect("/") unless settings.extended_signup
    next redirect("/auth/signup") unless logged_in?

    @title = "Sign up"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    signup_status = @user.signup_extended_status.strip.downcase
    next redirect("/") if signup_status == "complete"
    next redirect("/auth/signup/ex/#{signup_status}") unless signup_status == "membership"

    twocolumn = EARMMS::ConfigModel.where(key: 'signup-twocolumn').first
    @twocolumn = twocolumn.respond_to?(:value) ? twocolumn.value == 'yes' : false
    kaupapa = EARMMS::ConfigModel.where(key: 'kaupapa-prompt').first
    @kaupapa_prompt = kaupapa.respond_to?(:value) ? kaupapa.value : nil
    membership_requirements = EARMMS::ConfigModel.where(key: 'signup-member-requirements').first
    @membership_requirements = membership_requirements.respond_to?(:value) ? membership_requirements.value : nil
    supporter_requirements = EARMMS::ConfigModel.where(key: 'signup-supporter-requirements').first
    @supporter_requirements = supporter_requirements.respond_to?(:value) ? supporter_requirements.value : nil

    if @twocolumn
      haml :'auth/signup/extended/membership_twocolumn'
    else
      haml :'auth/signup/extended/membership'
    end
  end

  post '/' do
    next redirect("/") unless settings.extended_signup
    next redirect("/auth/signup") unless logged_in?

    @title = "Sign up"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    signup_status = @user.signup_extended_status.strip.downcase
    next redirect("/") if signup_status == "complete"
    next redirect("/auth/signup/ex/#{signup_status}") unless signup_status == "membership"

    action = request.params.key?("action") ? request.params["action"].strip.downcase : nil
    if action == "as_member"
      from_status = @profile.decrypt(:membership_status)
      to_status = (from_status == "member" ? "supporter" : "member")

      alert_data = {"from_status" => from_status, "to_status" => to_status}
      @pending_status = gen_alert :membership, :user_status_change_req, JSON.dump(alert_data)
      @profile.encrypt(:status_change_alert, @pending_status.id)
      @profile.save

    elsif action == "as_supporter"
      @profile.encrypt(:membership_status, "supporter")
      @profile.save

    else
      next halt 400, "invalid action"
    end

    @user.signup_extended_status = "branch"
    @user.save

    redirect '/auth/signup/ex/branch'
  end
end
