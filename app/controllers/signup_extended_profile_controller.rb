require 'rotp'

class EARMMS::SignupExtendedProfileController < EARMMS::ApplicationController
  helpers EARMMS::SignupHelpers

  before do
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @status = @profile.decrypt(:membership_status)

    @fields = [
      {name: "full_name", friendly: t(:full_name), type: "text", required: true},
      {name: "birthday", friendly: t(:birthday), type: "date", required: false},
      {name: "phone_home", friendly: t(:phone_home), type: "text", required: false},
      {name: "phone_mobile", friendly: t(:phone_mobile), type: "text", required: false},
      {name: "address", friendly: t(:address), type: "textarea", required: true},
    ]

    settings.custom_user_fields.each do |f|
      next unless f[:display_in_profile_edit]
      if f[:display_only_if_membership_status]
        next unless f[:display_only_if_membership_status].include?(@status.strip.downcase)
      end

      field_desc = {
        :name => f[:name].to_s.force_encoding("UTF-8"),
        :friendly => f[:friendly].force_encoding("UTF-8"),
        :type => (f[:type].to_s || "text").force_encoding("UTF-8"),
        :required => f[:required] || false,
        :default => f[:default] || nil,
        :select_options => f[:type] == 'select' ? f[:select_options] : nil,
        :display_only_if => f[:display_only_if] || nil,
        :display_only_if_membership_status => f[:display_only_if_membership_status] || nil,
        :is_custom => true,
      }

      @fields << field_desc
    end
  end

  get '/' do
    next redirect("/") unless settings.extended_signup
    next redirect("/auth/signup") unless logged_in?

    @title = "Sign up"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    signup_status = @user.signup_extended_status.strip.downcase
    next redirect("/") if signup_status == "complete"
    next redirect("/auth/signup/ex/#{signup_status}") unless signup_status == "profile"

    profile_custom_json = @profile.decrypt(:custom_fields)
    if profile_custom_json && profile_custom_json != ""
      profile_custom_json = JSON.parse(profile_custom_json)
    else
      profile_custom_json = {}
    end

    @values = @fields.map do |m|
      k = m[:name].force_encoding("UTF-8")
      v = nil

      if m[:is_custom]
        v = profile_custom_json[k] || ''
      else
        v = @profile.decrypt(k.to_sym) || ''
      end

      [k, v&.force_encoding("UTF-8")]
    end.compact.to_h

    haml :'auth/signup/extended/profile'
  end

  post '/' do
    next redirect("/") unless settings.extended_signup
    next redirect("/auth/signup") unless logged_in?

    @title = "Sign up"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    signup_status = @user.signup_extended_status.strip.downcase
    next redirect("/") if signup_status == "complete"
    next redirect("/auth/signup/ex/#{signup_status}") unless signup_status == "profile"

    profile_custom_json = @profile.decrypt(:custom_fields)
    if profile_custom_json && profile_custom_json != ""
      profile_custom_json = JSON.parse(profile_custom_json)
    else
      profile_custom_json = {}
    end

    @values = @fields.map do |m|
      k = m[:name].force_encoding("UTF-8")
      v = nil

      if m[:is_custom]
        v = profile_custom_json[k] || ''
      else
        v = @profile.decrypt(k.to_sym) || ''
      end

      [k, v&.force_encoding("UTF-8")]
    end.compact.to_h

    required = @fields.reject{|f| !f[:required]}
    has_all_required = required.map do |f|
      next false unless request.params.key?(f[:name])
      next false unless request.params[f[:name]]
      true
    end.all?

    unless has_all_required
      flash(:error, "A required field was not provided.")
      next haml :'auth/signup/extended/profile'
    end

    @fields.each do |field|
      next unless request.params.key?(field[:name])
      if field[:is_custom]
        profile_custom_json[field[:name]] = request.params[field[:name]]
      else
        @profile.encrypt(field[:name], request.params[field[:name]])
      end

      @values[field[:name]] = request.params[field[:name]]
    end

    @profile.encrypt(:custom_fields, JSON.dump(profile_custom_json))
    @profile.save

    @user.signup_extended_status = "membership"
    @user.save

    redirect '/auth/signup/ex/membership'
  end
end
