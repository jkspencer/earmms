class EARMMS::StaticController < EARMMS::ApplicationController
  VENDOR_WHITELIST = [
    "/purecss/build",
    "/font-awesome/css",
    "/font-awesome/fonts",
    "/zxcvbn/dist",
  ]

  configure do
    scsspath = File.join(EARMMS.root, "assets", "scss")
    viewspath = File.join(EARMMS.root, "app", "views")

    set(:views, {
      :scss => scsspath,
      :default => viewspath
    })
  end

  get '/styles.css' do
    scss :styles, :style => :compressed
  end

  get '/theme.css' do
    next 404 unless settings.respond_to?(:theme_dir)
    next 404 unless settings.theme_dir
    scss :theme, :style => :compressed
  end

  get '/vendor/*' do
    fn = File.expand_path(params['splat'].first, "/")
    next 404 unless VENDOR_WHITELIST.map{|x| fn.start_with?(x)}.any?
    path = File.join(EARMMS.root, "node_modules", fn)
    next 404 unless File.file? path
    send_file path
  end

  get '/*' do
    fn = File.expand_path(params['splat'].first, "/")
    path = File.join(EARMMS.root, "public", fn)
    if settings.respond_to?(:theme_dir) && settings.theme_dir
      themepath = File.join(settings.theme_dir, "public", fn)
      path = themepath if File.file? themepath
    end

    next 404 unless File.file? path
    send_file path
  end
end
