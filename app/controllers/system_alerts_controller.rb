class EARMMS::SystemAlertsController < EARMMS::SystemController
  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:alerts'

    @alerts = {
      :dismiss_action => '/system/alerts',
      :alerts => EARMMS::Alert.non_actioned_grouped_for_section(:system)
    }

    @title = "Alerts"
    haml :'system/alerts/index'
  end

  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:alerts'
    next halt 400, "no action specified" unless request.params.key?("action")

    case request.params["action"].strip.downcase
    when "dismiss"
      next halt 400, "no alert specified" unless request.params.key?("alertid")
      alertid = request.params["alertid"].strip.to_i
      alert = EARMMS::Alert[alertid]
      next halt 400, "invalid alert id" unless alert
      next halt 400, "alert section mismatch" unless alert.section.downcase == "system"
      alert.dismiss!

      flash(:success, "Dismissed alert #{alert.id}")

      # Update sidebar alert count
      @system_alerts = EARMMS::Alert.where(section: "system", actioned: false).count
    else
       next halt 400, "invalid action"
    end

    @alerts = {
      :dismiss_action => '/system/alerts',
      :alerts => EARMMS::Alert.non_actioned_grouped_for_section(:system)
    }

    @title = "Alerts"
    haml :'system/alerts/index'
  end

  get '/page/:page' do |page|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:alerts'

    page = page.strip.to_i
    @alerts = {
      :dismissed => true,
      :alerts => EARMMS::Alert.actioned_page_for_section(:system, page)
    }

    @title = "Alerts"
    haml :'system/alerts/page'
  end
end
