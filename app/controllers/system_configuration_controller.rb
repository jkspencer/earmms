class EARMMS::SystemConfigurationController < EARMMS::SystemController
  helpers EARMMS::SystemConfigurationHelpers

  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:change_config'

    @title = "System configuration"
    @configentries = EARMMS::ConfigModel.all
    haml :'system/config/index'
  end

  post '/edit' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:change_config'

    @title = "System configuration"
    unless request.params.key?("key")
      flash :error, "key was not provided"
      next request.params.to_h.inspect
      next redirect '/system/config'
    end

    @key = request.params["key"].strip.force_encoding("UTF-8")
    @entry = EARMMS::ConfigModel.where(key: @key).first
    @type = (@entry && @entry.type ? @entry.type.to_sym : nil)
    @value = (@entry ? (@entry.value ? @entry.value : '') : '').force_encoding("UTF-8")

    unless @entry
      @entry = EARMMS::ConfigModel.new(key: @key)
      @entry.type = nil
      @entry.save
      flash :success, "Config entry #{@key} created".force_encoding("UTF-8")
    end

    if request.params.key?("delete") && request.params["delete"] = "on"
      @entry.delete
      flash :success, "Entry #{@key} was deleted.".force_encoding("UTF-8")
      next redirect '/system/config'
    end

    if request.params.key?("value") || request.params.key?("expanded")
      if request.params.key?("type")
        unless @type == nil
          flash :error, "Can't set type of an existing entry"
          next redirect '/system/config'
        end

        @type = request.params["type"].strip.downcase.to_sym
        @entry.type = @type.to_s
        @entry.save
      end

      if request.params.key?("expanded") && request.params["expanded"] == "on"
        if request.params.key?("value-#{@type.to_s}")
          @value = request.params["value-#{@type.to_s}"].strip.force_encoding("UTF-8")
        else
          next halt 400, "bad expanded key"
        end
      else
        @value = request.params["value"].strip
      end

      @entry.value = @value.force_encoding("UTF-8")
      @entry.save

      message = "config entry #{@key.inspect} was set to #{@value.inspect}".force_encoding("UTF-8")
      gen_alert :system, :config_change, message
      flash :success, message
    end

    haml :'system/config/edit'
  end

  post '/refresh' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:change_config'

    # set maintenance mode
    maintenance = EARMMS::ConfigModel.where(key: 'maintenance').first
    unless maintenance
      maintenance = EARMMS::ConfigModel.new(key: 'maintenance', type: 'bool')
    end
    maintenance.value = 'yes'
    maintenance.save

    # refresh configs
    EARMMS.refresh_db_configs

    # disable maintenance
    maintenance.value = 'no'
    maintenance.save

    flash :success, "Refreshed global config from database."
    redirect '/system/config'
  end
end
