class EARMMS::SystemController < EARMMS::ApplicationController
  before do
    @system_alerts = EARMMS::Alert.where(section: "system", actioned: false).count
  end
end
