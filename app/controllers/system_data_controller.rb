class EARMMS::SystemDataController < EARMMS::SystemController
  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:data_correction'

    @title = "Data correction"
    @branches = EARMMS::Branch.all.map do |b|
      [b.id, b.decrypt(:name).force_encoding("UTF-8")]
    end.to_h

    haml :'system/data'
  end

  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:data_correction'

    @title = "Data correction"
    @branches = EARMMS::Branch.all.map do |b|
      [b.id, b.decrypt(:name).force_encoding("UTF-8")]
    end.to_h

    next halt 400, "no action" unless request.params.key?("action")
    case request.params["action"].strip.downcase
    when "invalid-status-reset"
      if request.params.key?("confirm") && request.params["confirm"] == "on"
        count = 0

        EARMMS::Profile.where(membership_status: nil).each do |p|
          p.encrypt(:membership_status, "supporter")
          p.save

          EARMMS::ProfileFilter.clear_filters_for(p)
          EARMMS::ProfileFilter.create_filters_for(p)

          count += 1
        end

        message = "#{count} profiles had their membership status corrected to supporter."
        gen_alert :system, :data_correction, message
        flash :success, message
      else
        flash :error, "Not doing anything because the confirmation box was not checked."
      end

    when "branch-all-unset"
      if request.params.key?("confirm") && request.params["confirm"] == "on"
        count = 0

        next halt 400, "no branch specified" unless request.params.key?("branch")
        branchid = request.params["branch"].to_i
        next halt 400, "invalid branch" unless branchid
        next halt 400, "invalid branch" unless @branches[branchid]

        EARMMS::Profile.where(branch: nil).each do |p|
          next unless p.decrypt(:membership_status) == "member"

          p.encrypt(:branch, branchid.to_s)
          p.save
          count += 1

          EARMMS::ProfileFilter.clear_filters_for(p)
          EARMMS::ProfileFilter.create_filters_for(p)
        end

        message = "#{count} profiles with unset branches were set to branch #{@branches[branchid]} (branch id #{branchid})".force_encoding("UTF-8")
        gen_alert :system, :data_correction, message
        flash :success, message
      else
        flash :error, "Not doing anything because the confirmation box was not checked."
      end
    else
      next halt 400, "invalid action"
    end

    haml :'system/data'
  end
end
