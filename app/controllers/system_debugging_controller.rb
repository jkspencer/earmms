class EARMMS::SystemDebuggingController < EARMMS::SystemController
  before do
    @appconfig_hidden = [
      :session_secret,
    ]
  end

  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:debugging'

    @title = "Debugging info"
    haml :'system/debug/index'
  end

  post '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:debugging'

    if request.params.key?("flash")
      type = request.params["flash"].downcase.to_sym
      flash(type, "This is a test of the flash type #{type}. Unicode test: ā".force_encoding("UTF-8"))
    end

    if request.params.key?("alert")
      next halt 403 unless has_role? 'system:alerts'

      valid_sections = [
        :system,
        :membership
      ]

      section = request.params["alert"].strip.downcase.to_sym
      next halt 403 unless valid_sections.include? section

      if request.params.key?("alert-empty") && request.params["alert-empty"].strip.downcase == "on"
        alert = EARMMS::Alert.new(section: section.to_s, actioned: false, created: Time.now)
        alert.save
      else
        alert = gen_alert section, :test, "Test alert, please ignore"
      end

      flash(:success, "Added test alert in section #{section.inspect} with ID #{alert.id}")
    end

    # refresh alert count
    @system_alerts = EARMMS::Alert.where(section: "system", actioned: false).count

    @title = "Debugging info"
    haml :'system/debug/index'
  end

  get '/appconfig' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:debugging'

    @title = "Application configuration"
    @appconfig = EARMMS.app_config.map do |k, v|
      next nil if @appconfig_hidden.include?(k)
      [k.to_s.force_encoding("UTF-8"), v.inspect.to_s.force_encoding("UTF-8")]
    end.compact.to_h

    haml :'system/debug/appconfig'
  end
end
