class EARMMS::SystemGrantController < EARMMS::SystemController
  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:grant'

    @title = "Grant roles"
    haml :'system/grant/index'
  end

  get '/list' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:grant'

    @users = {}

    next halt 400, "no type key" unless request.params.key?("type")
    case request.params["type"].strip.downcase
    when "all"
      @criteria = "all users with any role"
      EARMMS::UserHasRole.each do |uhr|
        unless @users.key?(uhr.user)
          @users[uhr.user] = {
            user: EARMMS::User[uhr.user],
            roles: []
          }

          @users[uhr.user][:name] = EARMMS::Profile.for_user(@users[uhr.user][:user]).decrypt(:full_name).force_encoding("UTF-8")
        end

        @users[uhr.user][:roles] << uhr
      end

    when "role"
      next halt 400, "no role key" unless request.params.key?("role")
      role = request.params["role"].strip.downcase
      @criteria = "role #{role}"

      EARMMS::UserHasRole.where(role: role).each do |uhr|
        unless @users.key?(uhr.user)
          @users[uhr.user] = {
            user: EARMMS::User[uhr.user],
            roles: []
          }

          @users[uhr.user][:name] = EARMMS::Profile.for_user(@users[uhr.user][:user]).decrypt(:full_name).force_encoding("UTF-8")
        end

        @users[uhr.user][:roles] << uhr
      end

    when "uid"
      next halt 400, "no uid key" unless request.params.key?("uid")
      uid = request.params["uid"].strip.to_i
      @criteria = "uid #{uid}"

      EARMMS::UserHasRole.where(user: uid).each do |uhr|
        unless @users.key?(uhr.user)
          @users[uhr.user] = {
            user: EARMMS::User[uhr.user],
            roles: []
          }

          @users[uhr.user][:name] = EARMMS::Profile.for_user(@users[uhr.user][:user]).decrypt(:full_name).force_encoding("UTF-8")
        end

        @users[uhr.user][:roles] << uhr
      end

    else
      next halt 400, "bad type"
    end

    @title = "Role list"
    haml :'system/grant/list'
  end

  get '/edit' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:grant'
    next halt 400, "no uid key" unless request.params.key?("uid")

    @user = EARMMS::User[request.params["uid"].strip.to_i]
    next halt 400, "invalid user" unless @user
    @user_name = EARMMS::Profile.for_user(@user).decrypt(:full_name).force_encoding("UTF-8")
    @has_2fa = @user.totp_enabled || (EARMMS::U2FRegistration.where(user: @user.id).count > 0)

    @title = "Role editing for uid #{@user.id}"
    haml :'system/grant/edit'
  end

  post '/edit' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:grant'
    next halt 400, "no uid key" unless request.params.key?("uid")
    next halt 400, "no action key" unless request.params.key?("action")
    next halt 400, "no role key" unless request.params.key?("role")

    @user = EARMMS::User[request.params["uid"].strip.to_i]
    next halt 400, "invalid user" unless @user
    @user_name = EARMMS::Profile.for_user(@user).decrypt(:full_name).force_encoding("UTF-8")
    @has_2fa = @user.totp_enabled || (EARMMS::U2FRegistration.where(user: @user.id).count > 0)

    unless @has_2fa
      t = EARMMS::ConfigModel.where(key: '2fa-required-for-roles').first
      if t && t.value == "yes"
        flash :error, "Can not grant roles to user without 2FA enabled"
        next haml :'system/grant/edit'
      end
    end

    case request.params["action"].strip.downcase
    when "delete"
      r = request.params["role"].strip.downcase
      EARMMS::UserHasRole.where(user: @user.id, role: r).delete

      message = "role #{r.inspect} removed for uid #{@user.id}"
      gen_alert :system, :role_edit, message
      flash :success, message
    when "add"
      r = request.params["role"].strip.downcase
      uhr = EARMMS::UserHasRole.new(user: @user.id, role: r)
      uhr.save

      message = "role #{r.inspect} added for uid #{@user.id}"
      gen_alert :system, :role_edit, message
      flash :success, message
    else
      next halt 400, "invalid action"
    end

    # refresh alert count
    @system_alerts = EARMMS::Alert.where(section: "system", actioned: false).count

    @title = "Role editing for uid #{@user.id}"
    haml :'system/grant/edit'
  end
end
