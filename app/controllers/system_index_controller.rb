class EARMMS::SystemIndexController < EARMMS::SystemController
  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:access'

    @title = "System home"
    haml :'system/index'
  end
end
