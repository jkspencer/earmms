class EARMMS::SystemWorkQueueController < EARMMS::SystemController
  helpers EARMMS::SystemWorkQueueHelpers

  get '/' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:work_queue'

    @status = 'queued'
    if request.params.key?("status")
      @status = request.params["status"].downcase
    end

    page = request.params.key?("page") ? request.params["page"].to_i : 1

    @ds = EARMMS::WorkQueue
      .where(status: @status)
      .order(Sequel.desc(:id))
      .paginate(page, 25)

    @current_page = @ds.current_page
    @total_pages = @ds.page_count
    @tasks = @ds.map{|m| work_queue_data(m)}

    @new_uri = Addressable::URI.parse("/system/workqueue/queue")
    @new_uri.query_values = {
      :back => "/system/workqueue/?page=#{@current_page}&status=#{@status}"
    }

    @title = "Work queue"
    haml :'system/work_queue/index'
  end

  get '/queue' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:work_queue'
    @title = "Queue a job"
    @tasks = EARMMS::Workers.constants.map do |sym|
      m = EARMMS::Workers.const_get(sym)
      next nil unless m.respond_to?(:perform)
      [sym, m]
    end.compact.to_h

    @back = '/system/workqueue'
    if request.params.key?("back")
      @back = request.params["back"]
    end

    haml :'system/work_queue/queue'
  end

  post '/queue' do
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:work_queue'
    @title = "Queue a job"
    @tasks = EARMMS::Workers.constants.map do |sym|
      m = EARMMS::Workers.const_get(sym)
      next nil unless m.respond_to?(:perform)
      [sym, m]
    end.compact.to_h

    @back = '/system/workqueue'
    if request.params.key?("back")
      @back = request.params["back"]
    end

    if !request.params.key?("type") || request.params["type"].strip == ""
      flash :error, "No job type key provided"
      next haml :'system/work_queue/queue'
    end

    type = request.params["type"].strip.to_sym
    unless @tasks.keys.include?(type)
      flash :error, "Invalid job type"
      next haml :'system/work_queue/queue'
    end

    data = {}
    if request.params.key?("data") && request.params["data"].strip != ""
      begin
        data = JSON.parse(request.params["data"].strip)
      rescue => e
        flash :error, "Error decoding data JSON: #{e}"
        next haml :'system/work_queue/queue'
      end
    end

    date = Time.now.strftime("%Y-%m-%d")
    if request.params.key?("date") && request.params["date"].strip != ""
      date = request.params["date"].strip
    end

    time = Time.now.strftime("%H:%M")
    if request.params.key?("time") && request.params["time"].strip != ""
      time = request.params["time"].strip
    end

    begin
      run_at = Time.parse("#{date} #{time}")
    rescue => e
      flash :warning, "Failed to parse time '#{date} #{time}', queueing for now instead"
      run_at = Time.now
    end

    begin
      @tasks[type].queue_at(run_at, data)
    rescue => e
      flash :error, "Error queueing: #{e}"
    end

    flash :success, "Queued #{type} to be run at #{run_at.iso8601} (in #{(run_at - Time.now).round}s)"
    haml :'system/work_queue/queue'
  end

  get '/log/:id' do |id|
    next halt 404 unless logged_in?
    next halt 404 unless has_role? 'system:work_queue'

    entry = EARMMS::WorkQueue[id.to_i]
    next halt 404 unless entry
    @entry = work_queue_data(entry)
    @log = entry.decrypt(:log)

    @back = '/system/workqueue'
    if request.params.key?("back")
      @back = request.params["back"]
    end

    @title = "Log for job #{id}"
    haml :'system/work_queue/log'
  end
end
