class EARMMS::UserDisabledController < EARMMS::ApplicationController
  get '/' do
    next redirect('/') unless logged_in?

    @user = current_user
    next redirect('/') if @user.disabled_reason == nil
    @reason = @user.decrypt(:disabled_reason)

    @title = t :'account_disabled'
    haml :'auth/user_disabled', :layout => :layout_minimal
  end
end
