class EARMMS::UserSettingsAccountController < EARMMS::ApplicationController
  helpers EARMMS::UserSettingsAccountHelpers

  get '/' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Account settings"
    haml :'user_settings/account'
  end

  post '/' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Account settings"

    next halt 400, "no action specified" unless request.params.key?("action")
    action = request.params["action"].strip.downcase
    u = current_user

    case action
    when "change-email"
      next error_page("Please supply an email address.") unless request.params.key?("email")
      next error_page("Please supply your password.") unless request.params.key?("password")

      # verify password
      unless u.verify_password(request.params["password"])
        next error_page("Password was incorrect.")
      end

      # check there's no users in the db with that email
      if EARMMS::User.where(email: request.params["email"].strip.downcase).count > 0
        next error_page("This email address is already in use.")
      end

      u.email = request.params["email"].strip.downcase
      u.save

      flash :success, "Email address successfully changed."

    when "change-password"
      next error_page("Please supply your password.") unless request.params.key?("password")
      next error_page("Please supply your new password.") unless request.params.key?("newpass")
      next error_page("Please supply your new password, to confirm") unless request.params.key?("newpass-confirm")
      next error_page("Your passwords do not match.") unless request.params["newpass"] == request.params["newpass-confirm"]

      u.password = request.params["newpass"]
      u.invalidate_tokens(session[:token])
      u.save

      flash :success, "Password successfully changed."

    when "delete-account"
      next error_page("Please check the confirmation box.") unless request.params.key?("confirm")
      next error_page("Please check the confirmation box.") unless request.params["confirm"] == "on"
      
      profile = EARMMS::Profile.for_user(u)
      message = "User #{profile.decrypt(:full_name).force_encoding("UTF-8")} (uid #{u.id}) has deleted their account. A summary will be emailed.".force_encoding("UTF-8")
      gen_alert :membership, :user_deletion, message

      u.delete_account!

      flash :success, "Your account has been deleted. We're sorry to see you go!"
      next redirect '/'
    else
      next halt 400, "bad action"
    end

    haml :'user_settings/account'
  end
end
