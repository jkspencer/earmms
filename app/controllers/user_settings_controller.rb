class EARMMS::UserSettingsController < EARMMS::ApplicationController
  before do
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @status = @profile.decrypt(:membership_status)

    @fields = [
      {name: "full_name", friendly: t(:full_name), type: "text", required: true},
      {name: "birthday", friendly: t(:birthday), type: "date", required: false},
      {name: "phone_home", friendly: t(:phone_home), type: "text", required: false},
      {name: "phone_mobile", friendly: t(:phone_mobile), type: "text", required: false},
      {name: "address", friendly: t(:address), type: "textarea", required: false},
    ]

    settings.custom_user_fields.each do |f|
      next unless f[:display_in_profile_edit]
      if f[:display_only_if_membership_status]
        next unless f[:display_only_if_membership_status].include?(@status.strip.downcase)
      end

      field_desc = {
        :name => f[:name].to_s.force_encoding("UTF-8"),
        :friendly => f[:friendly].force_encoding("UTF-8"),
        :type => (f[:type].to_s || "text").force_encoding("UTF-8"),
        :required => f[:required] || false,
        :default => f[:default] || nil,
        :select_options => f[:type] == 'select' ? f[:select_options] : nil,
        :display_only_if => f[:display_only_if] || nil,
        :display_only_if_membership_status => f[:display_only_if_membership_status] || nil,
        :is_custom => true,
      }

      @fields << field_desc
    end
  end

  get '/' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "My profile"
    @profile = EARMMS::Profile.for_user(current_user)

    profile_custom_json = @profile.decrypt(:custom_fields)
    if profile_custom_json && profile_custom_json != ""
      profile_custom_json = JSON.parse(profile_custom_json)
    else
      profile_custom_json = {}
    end

    @values = @fields.map do |m|
      k = m[:name].force_encoding("UTF-8")
      v = nil

      if m[:is_custom]
        v = profile_custom_json[k]
      else
        v = @profile.decrypt(k.to_sym)
      end

      if v.nil? || v.empty?
        if m[:default]
          v = m[:default]
        end
      end

      [k, v&.force_encoding("UTF-8")]
    end.compact.to_h

    haml :'user_settings/index'
  end

  post '/' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "My profile"
    @profile = EARMMS::Profile.for_user(current_user)

    profile_custom_json = @profile.decrypt(:custom_fields)
    if profile_custom_json && profile_custom_json != ""
      profile_custom_json = JSON.parse(profile_custom_json)
    else
      profile_custom_json = {}
    end

    @values = @fields.map do |m|
      k = m[:name].force_encoding("UTF-8")
      v = nil

      if m[:is_custom]
        v = profile_custom_json[k]
      else
        v = @profile.decrypt(k.to_sym)
      end

      if v.nil? || v.empty?
        if m[:default]
          v = m[:default]
        end
      end

      [k, v&.force_encoding("UTF-8")]
    end.compact.to_h

    required = @fields.reject{|f| !f[:required]}
    has_all_required = required.map do |f|
      next false unless request.params.key?(f[:name])
      next false unless request.params[f[:name]]
      true
    end.all?

    unless has_all_required
      flash(:error, "A required field was not provided.")
      next haml :'user_settings/index'
    end

    @fields.each do |field|
      next unless request.params.key?(field[:name])
      if field[:is_custom]
        profile_custom_json[field[:name]] = request.params[field[:name]].force_encoding("UTF-8")
      else
        @profile.encrypt(field[:name], request.params[field[:name]].force_encoding("UTF-8"))
      end

      @values[field[:name]] = request.params[field[:name]].force_encoding("UTF-8")
    end

    @profile.encrypt(:custom_fields, JSON.dump(profile_custom_json))
    @profile.save

    EARMMS::ProfileFilter.clear_filters_for(@profile)
    EARMMS::ProfileFilter.create_filters_for(@profile)

    flash(:success, "Your profile has been updated.")
    haml :'user_settings/index'
  end
end
