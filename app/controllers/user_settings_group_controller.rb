class EARMMS::UserSettingsGroupController < EARMMS::ApplicationController
  helpers EARMMS::GroupHelpers

  get '/' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    @group_ids = get_group_ids_user_is_member(@user)
    if @group_ids.empty?
      @groups = {}
    else
      @groups = get_groups_hash_by_type(@group_ids, {
        :user => @user,
        :only_user_joined => true,
        :link_to_edit => :user
      })
    end

    @title = "Your groups"
    haml :'user_settings/group/index'
  end
end
