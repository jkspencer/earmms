class EARMMS::UserSettingsGroupEditController < EARMMS::ApplicationController
  helpers EARMMS::GroupHelpers

  get '/:id' do |cid|
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    next halt 404 unless cid
    @group = EARMMS::Group[cid]
    next halt 404 unless @group
    @group_hash = get_groups_hash([@group.id], :user => @user)
    @group_hash = @group_hash[@group.id]

    next halt 404 unless @group_hash[:user_membership]
    next halt 404 unless @group_hash[:user_membership][:user_id]
    next halt 404 unless @group_hash[:user_membership][:user_id] == @user.id

    @title = "Group: #{@group_hash[:name]}".force_encoding("UTF-8")
    if @group_hash[:user_membership][:admin]
      @members = get_group_members(@group)
    end

    haml :'user_settings/group/edit'
  end

  post '/:id' do |cid|
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)

    next halt 404 unless cid
    @group = EARMMS::Group[cid]
    next halt 404 unless @group
    @group_hash = get_groups_hash([@group.id], :user => @user)
    @group_hash = @group_hash[@group.id]

    next halt 404 unless @group_hash[:user_membership]
    next halt 404 unless @group_hash[:user_membership][:user_id]
    next halt 404 unless @group_hash[:user_membership][:user_id] == @user.id

    @title = "Group: #{@group_hash[:name]}".force_encoding("UTF-8")
    if @group_hash[:user_membership][:admin]
      @members = get_group_members(@group)
    end

    unless request.params.key?("action") && request.params["action"].strip != ""
      next halt 400, "no action provided"
    end

    action = request.params["action"].strip.downcase
    if action == "leave"
      cm = @group_hash[:user_membership][:group_member]

      roles = cm.decrypt(:roles).split(",").map do |r|
        r.strip.downcase
      end

      if roles.include?("admin")
        flash :error, "You can't leave this group because you are an administrator. Please demote yourself first."
        next haml :'user_settings/group/edit'
      end

      EARMMS::GroupMemberFilter.clear_filters_for(cm)
      cm.delete

      flash :success, "You have left the group."
      next redirect '/user/group'

    else
      next halt 400, "invalid action"
    end

    redirect(request.path)
  end
end
