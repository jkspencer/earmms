class EARMMS::UserSettingsGroupJoinController < EARMMS::ApplicationController
  helpers EARMMS::GroupHelpers

  get '/' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Joinable groups"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @membership_status = @profile.decrypt(:membership_status)&.strip&.downcase

    # get joinable groups
    @joinable_groups = get_groups_hash(:all, :user => @user).map do |k, v|
      next nil if v[:user_membership]
      next nil unless v[:is_joinable]
      if v[:restrict_to_member]
        next nil unless @membership_status == "member"
      end

      [k, v]
    end.compact.to_h

    # sort joinable groups by type
    @joinable_by_type = {}
    @joinable_groups.each do |k, v|
      @joinable_by_type[v[:type]] ||= {}
      @joinable_by_type[v[:type]][k] = v
    end

    haml :'user_settings/group/joinable'
  end

  post '/' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Joinable groups"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @membership_status = @profile.decrypt(:membership_status)&.strip&.downcase

    # get joinable groups
    @joinable_groups = get_groups_hash(:all, :user => @user).map do |k, v|
      next nil if v[:user_membership]
      next nil unless v[:is_joinable]
      if v[:restrict_to_member]
        next nil unless @membership_status == "member"
      end


      [k, v]
    end.compact.to_h

    # sort joinable groups by type
    @joinable_by_type = {}
    @joinable_groups.each do |k, v|
      @joinable_by_type[v[:type]] ||= {}
      @joinable_by_type[v[:type]][k] = v
    end

    unless request.params.key?("group") && request.params["group"].strip != ""
      flash :error, "No group was provided."
      next haml :'user_settings/group/joinable'
    end

    cid = request.params["group"].strip.to_i
    unless @joinable_groups.keys.include?(cid)
      flash :error, "The selected group does not exist or is not joinable."
      next haml :'user_settings/group/joinable'
    end

    @group = EARMMS::Group[cid]
    unless @group
      flash :error, "The selected group does not exist or is not joinable."
      next haml :'user_settings/group/joinable'
    end

    @group_hash = get_groups_hash([cid], :user => @user)
    if @group_hash.length != 1
      flash :error, "An error occurred."
      next haml :'user_settings/group/joinable'
    end
    @group_hash = @group_hash[cid]

    if @group_hash[:user_membership]
      flash :error, "You are already a member of the selected group."
      next haml :'user_settings/group/joinable'
    end

    unless request.params.key?("action")
      flash :error, "An error occurred."
      next haml :'user_settings/group/joinable'
    end

    action = request.params["action"].strip.downcase
    if action == "confirmation"
      next haml :'user_settings/group/join_confirmation'

    elsif action == "do-join"
      if @group_hash[:agreement] != ""
        confirmation = (request.params.key?("agreement-ok") && request.params["agreement-ok"].strip.downcase == "on")
        unless confirmation
          flash :error, "You must agree to the terms before you can join this group."
          next haml :'user_settings/group/join_confirmation'
        end
      end

      # get user branch info
      branch_name = "unknown"
      branch = @profile.decrypt(:branch).to_i
      branch = EARMMS::Branch[branch] if branch
      branch_name = branch.decrypt(:name).force_encoding("UTF-8") if branch

      # construct user info hash
      user = {
        :name => @profile.decrypt(:full_name).force_encoding("UTF-8"),
        :email => @user.email,
        :branch => branch_name,
      }

      # send email to group admins
      group_admins = get_group_members(@group).select{|k, v| v[:admin] == true}
      to_addresses = group_admins.map{|k,v| v[:email]}.compact

      if !to_addresses.empty?
        text = EARMMS.email_templates.group_new_user(@group_hash, user)

        qm = EARMMS::EmailQueue.new(creation: Time.now)
        qm.save
        qm.queue_status = 'queued'
        qm.encrypt(:recipients, JSON.generate({"type" => "list", "list" => to_addresses}))
        qm.encrypt(:subject, EARMMS.email_subject("A new user joined a group"))
        qm.encrypt(:content, text.content_text)
        qm.encrypt(:content_html, text.content_html)
        qm.save
      end

      # create GroupMember entry
      cm = EARMMS::GroupMember.new(group: @group.id)
      cm.save
      cm.encrypt(:user, @user.id)
      cm.encrypt(:roles, "")
      cm.save

      EARMMS::GroupMemberFilter.clear_filters_for(cm)
      EARMMS::GroupMemberFilter.create_filters_for(cm)

      flash :success, "You have successfully joined the group #{@group_hash[:name]}.".force_encoding("UTF-8")
      next redirect "/user/group/edit/#{cid}"

    else
      flash :error, "An error occurred."
      next haml :'user_settings/group/joinable'
    end
  end
end
