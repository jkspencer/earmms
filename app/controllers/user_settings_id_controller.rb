class EARMMS::UserSettingsIDController < EARMMS::ApplicationController
  get '/' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "User ID"
    haml :'user_settings/userid'
  end
end
