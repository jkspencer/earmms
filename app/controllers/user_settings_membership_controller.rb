class EARMMS::UserSettingsMembershipController < EARMMS::ApplicationController
  get '/' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Membership"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @status = @profile.decrypt(:membership_status)

    @branch = @profile.decrypt(:branch)
    @branch = nil if @branch == ""
    if @branch
      @branch = EARMMS::Branch[@branch.to_i]
    end

    t = EARMMS::ConfigModel.where(key: 'kaupapa-prompt').first
    @kaupapa_prompt = t.respond_to?(:value) ? t.value.force_encoding("UTF-8") : nil

    @pending_status = @profile.decrypt(:status_change_alert)
    @pending_status = nil if @pending_status == ""
    if @pending_status
      @pending_status = EARMMS::Alert[@pending_status.to_i]
    end

    @pending_branch = @profile.decrypt(:branch_change_alert)
    @pending_branch = nil if @pending_branch == ""
    if @pending_branch
      @pending_branch = EARMMS::Alert[@pending_branch.to_i]
    end

    haml :'user_settings/membership_status/index'
  end

  post '/' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Membership"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @status = @profile.decrypt(:membership_status)

    @branch = @profile.decrypt(:branch)
    @branch = nil if @branch == ""
    if @branch
      @branch = EARMMS::Branch[@branch.to_i]
    end

    t = EARMMS::ConfigModel.where(key: 'kaupapa-prompt').first
    @kaupapa_prompt = t.respond_to?(:value) ? t.value.force_encoding("UTF-8") : nil

    @pending_status = @profile.decrypt(:status_change_alert)
    @pending_status = nil if @pending_status == ""
    if @pending_status
      @pending_status = EARMMS::Alert[@pending_status.to_i]
    end

    @pending_branch = @profile.decrypt(:branch_change_alert)
    @pending_branch = nil if @pending_branch == ""
    if @pending_branch
      @pending_branch = EARMMS::Alert[@pending_branch.to_i]
    end

    unless request.params.key?("action")
      next halt 400, "no action param specified"
    end

    case request.params["action"]
    when "toggle-status"
      if @pending
        flash :error, "You can not make another change request while one is pending."
        next haml :'user_settings/membership_status/index'
      end

      from_status = @status
      to_status = (from_status == "member" ? "supporter" : "member")

      if to_status == "member" && @kaupapa_prompt != nil
        unless request.params.key?("kaupapa-agree") && request.params["kaupapa-agree"] == "on"
          flash :error, "You must agree to the kaupapa to continue becoming a member of #{org_name}."
          next haml :'user_settings/membership_status/index'
        end
      end

      alert_data = {"from_status" => from_status, "to_status" => to_status}
      @pending_status = gen_alert :membership, :user_status_change_req, JSON.dump(alert_data)
      @profile.encrypt(:status_change_alert, @pending_status.id)
      @profile.save

      flash :success, "A request to change your membership status to #{to_status} has been sent."
    else
      next halt 400, "invalid action"
    end

    haml :'user_settings/membership_status/index'
  end

  get '/change-branch' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Membership"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @status = @profile.decrypt(:membership_status)

    @branch = @profile.decrypt(:branch)
    @branch = nil if @branch == ""
    if @branch
      @branch = EARMMS::Branch[@branch.to_i]
    end

    @pending = @profile.decrypt(:branch_change_alert)
    @pending = nil if @pending == ""
    if @pending
      @pending = EARMMS::Alert[@pending.to_i]
    end

    @branches = EARMMS::Branch.all.map do |b|
      {
        :id => b.id,
        :name => b.decrypt(:name).force_encoding("UTF-8"),
      }
    end

    haml :'user_settings/membership_status/change_branch'
  end

  post '/change-branch' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    unless request.params.key?("branch")
      next halt 400, "no branch specified"
    end

    @title = "Membership"
    @user = current_user
    @profile = EARMMS::Profile.for_user(@user)
    @status = @profile.decrypt(:membership_status)

    @branch = @profile.decrypt(:branch)
    @branch = nil if @branch == ""
    if @branch
      @branch = EARMMS::Branch[@branch.to_i]
    end

    @pending = @profile.decrypt(:branch_change_alert)
    @pending = nil if @pending == ""
    if @pending
      @pending = EARMMS::Alert[@pending.to_i]
    end

    @branches = EARMMS::Branch.all.map do |b|
      {
        :id => b.id,
        :name => b.decrypt(:name).force_encoding("UTF-8"),
      }
    end

    if @pending
      flash :error, "You can not make another change request while one is pending."
      next haml :'user_settings/membership_status/change_branch'
    end

    from_branch = @branch
    to_branch = EARMMS::Branch[request.params["branch"].to_i]
    unless to_branch
      next halt 400, "invalid branch"
    end

    if from_branch && from_branch.id == to_branch.id
      flash :error, "You can't make a request to change branch to your current branch."
      next haml :'user_settings/membership_status/change_branch'
    end

    from_branch_name = "unknown"
    from_branch_name = from_branch.decrypt(:name).force_encoding("UTF-8") if from_branch
    to_branch_name = to_branch.decrypt(:name).force_encoding("UTF-8")

    alert_data = {
      "from_branch" => from_branch_name,
      "from_branch_id" => from_branch ? from_branch.id : 0,
      "to_branch" => to_branch_name,
      "to_branch_id" => to_branch.id,
    }

    @pending = gen_alert :membership, :user_branch_change_req, JSON.dump(alert_data)
    @profile.encrypt(:branch_change_alert, @pending.id)
    @profile.save

    flash :success, "A request to change your branch from #{from_branch_name} to #{to_branch_name} has been sent.".force_encoding("UTF-8")
    haml :'user_settings/membership_status/change_branch'
  end
end
