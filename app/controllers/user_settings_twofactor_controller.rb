require 'rotp'
require 'rqrcode'
require 'base64'
require 'u2f'

class EARMMS::UserSettingsTwoFactorController < EARMMS::ApplicationController
  before do
    @u2f ||= U2F::U2F.new(request.base_url)
  end

  get '/' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Two-factor authentication"
    @user = current_user

    @has_u2f = EARMMS::U2FRegistration.where(user: @user.id).count > 0

    haml :'user_settings/twofactor/index'
  end

  post '/totp' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Two-factor authentication"
    @user = current_user

    unless request.params.key?("statechange")
      flash :error, "Something went wrong, please try again!"
      haml :'user_settings/twofactor/index'
    end

    case request.params["statechange"]
    when "enable"
      @token = ROTP::Base32.random_base32
      @user.encrypt(:totp_secret, @token)
      @user.save

      t = ROTP::TOTP.new(@token, issuer: site_name)
      url = t.provisioning_uri(@user.email)
      qr = RQRCode::QRCode.new(url)
      svg = qr.as_svg(offset: 0, color: '000', shape_rendering: 'crispEdges', module_size: 11)
      svg_b64 = Base64.encode64(svg)
      @qrcode_data = "data:image/svg+xml;base64,#{svg_b64}"

      next haml :'user_settings/twofactor/verify'

    when "verify"
      @token = @user.decrypt(:totp_secret)

      t = ROTP::TOTP.new(@token, issuer: site_name)
      url = t.provisioning_uri(@user.email)
      qr = RQRCode::QRCode.new(url)
      svg = qr.as_svg(offset: 0, color: '000', shape_rendering: 'crispEdges', module_size: 11)
      svg_b64 = Base64.encode64(svg)
      @qrcode_data = "data:image/svg+xml;base64,#{svg_b64}"

      unless request.params.key?("code")
        flash :error, "Please enter your verification code."
        next haml :'user_settings/twofactor/verify'
      end

      t = ROTP::TOTP.new(@token)
      if t.verify_with_drift(request.params["code"].strip, 15)
        @user.totp_enabled = true
        @user.invalidate_tokens(session[:token])
        @user.save

        flash :success, "Two-factor authentication has been enabled on your account. All other devices logged into your account have now been logged out."
        next redirect '/user/twofactor'
      else
        flash :error, "The provided verification code was incorrect."
        next haml :'user_settings/twofactor/verify'
      end

    when "disable"
      unless request.params.key?("confirm") && request.params["confirm"] == "on"
        flash :error, "Please check the box to confirm you wish to disable two-factor authentication."
        next redirect '/user/twofactor'
      end

      @user.totp_enabled = false
      @user.totp_secret = nil
      @user.save

      flash :success, "Two-factor authentication has been disabled."
      next redirect '/user/twofactor'

    else
      flash :error, "Something went wrong, please try again!"
      next redirect '/user/twofactor'
    end
  end

  get '/u2f' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Two-factor authentication"
    @user = current_user

    unless @user.totp_enabled
      flash :error, "You must have registered an authentication app before registering a security key."
      next redirect '/user/twofactor'
    end

    registrations = EARMMS::U2FRegistration.where(user: @user.id)
    @u2fcount = registrations.count
    @u2fkeys = registrations.map do |r|
      pubkey = Base64.decode64(r.public_key)
      id = EARMMS::Crypto.bin_to_hex(r.public_key[0..3]) + "..." + EARMMS::Crypto.bin_to_hex(r.public_key[-4..-1])

      {
        :id => r.id,
        :name => r.name,
        :identifier => id.encode(Encoding::UTF_8)
      }
    end

    haml :'user_settings/twofactor/u2f/index'
  end

  post '/u2f/delete' do 
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Two-factor authentication"
    @user = current_user

    unless @user.totp_enabled
      flash :error, "You must have registered an authentication app before registering a security key."
      next redirect '/user/twofactor'
    end

    halt 400 unless request.params.key?("key")
    id = request.params["key"].to_i
    halt 400 unless id
    @key = EARMMS::U2FRegistration.where(user: @user.id, id: id).first
    halt 400 unless @key

    name = @key.name
    @key.delete

    flash :success, "The security key #{name} has been removed."
    redirect '/user/twofactor/u2f'
  end

  get '/u2f/register' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Two-factor authentication"
    @user = current_user

    unless @user.totp_enabled
      flash :error, "You must have registered an authentication app before registering a security key."
      next redirect '/user/twofactor'
    end

    @registration_requests = @u2f.registration_requests.map do |m|
      m = JSON.parse(JSON.generate(m))
      m["appId"] = request.base_url
      m
    end
    session[:challenges] = @registration_requests.map{|x| x["challenge"]}
    key_handles = EARMMS::U2FRegistration.where(user: @user.id).map(&:key_handle)
    @sign_requests = @u2f.authentication_requests(key_handles).map do |m|
      m = JSON.parse(JSON.generate(m))
      m["appId"] = @u2f.app_id
      m
    end

    haml :'user_settings/twofactor/u2f/register'
  end

  post '/u2f/register' do
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Two-factor authentication"
    @user = current_user

    unless @user.totp_enabled
      flash :error, "You must have registered an authentication app before registering a security key."
      next redirect '/user/twofactor'
    end

    @tid = nil
    begin
      response = U2F::RegisterResponse.load_from_json(request.params["response"])
      reg = @u2f.register!(session[:challenges], response)
      t = EARMMS::U2FRegistration.new(user: @user.id)
      t.certificate = reg.certificate
      t.key_handle = reg.key_handle
      t.public_key = reg.public_key
      t.counter = reg.counter
      t.save

      @tid = t.id
    rescue U2F::Error => e
      flash :error, "Unable to register U2F token: #{e.class.name}"
    ensure
      session.delete(:challenges)
    end

    redirect "/user/twofactor/u2f/name/#{@tid}"
  end

  get '/u2f/name/:id' do |id|
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Two-factor authentication"
    @user = current_user

    unless @user.totp_enabled
      flash :error, "You must have registered an authentication app before registering a security key."
      next redirect '/user/twofactor'
    end

    @u2freg = EARMMS::U2FRegistration[id.to_i]
    next halt 400 unless @u2freg
    next halt 400 unless @u2freg.user == @user.id

    haml :'user_settings/twofactor/u2f/name'
  end

  post '/u2f/name/:id' do |id|
    unless logged_in?
      flash(:error, "You must be logged in to access this page.")
      next redirect('/auth')
    end

    @title = "Two-factor authentication"
    @user = current_user

    unless @user.totp_enabled
      flash :error, "You must have registered an authentication app before registering a security key."
      next redirect '/user/twofactor'
    end

    @u2freg = EARMMS::U2FRegistration[id.to_i]
    next halt 400 unless @u2freg
    next halt 400 unless @u2freg.user == @user.id

    unless request.params.key?("name")
      flash :error, "You must specify a name for this security key."
      next haml :'user_settings/twofactor/u2f/name'
    end

    if request.params["name"].strip == ""
      flash :error, "You must specify a name for this security key."
      next haml :'user_settings/twofactor/u2f/name'
    end

    @u2freg.name = request.params["name"].strip
    @u2freg.save

    redirect '/user/twofactor/u2f'
  end
end
