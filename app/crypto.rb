require 'rack'
require 'rbnacl'
require 'typhoeus'

module EARMMS::Crypto
  @opslimit = 2**20
  @memlimit = 2**24
  @tokenlength = 32

  def self.keyderiv_url
    unless ENV.key?("KEYDERIV_URL")
      raise RuntimeError, "KEYDERIV_URL not specified in environment"
    end

    return ENV["KEYDERIV_URL"]
  end

  def self.keyderiv_use_2x
    ENV["KEYDERIV_USE_2X"]&.strip&.downcase == "true"
  end

  def self.keyderiv_secret
    self.hex_to_bin(ENV["KEYDERIV_SECRET"])
  end

  ###########################################################################
  # Encryption/decryption
  ###########################################################################

  def self.get_encryption_key_2x(table, column, row)
    raise "keyderiv 2.x enabled but no secret provided" unless self.keyderiv_secret
    box = RbNaCl::SecretBox.new(self.keyderiv_secret)
    nonce = RbNaCl::Random.random_bytes(box.nonce_bytes)

    body = {
      "mode" => "encrypt",
      "table" => table.to_s,
      "column" => column.to_s,
      "row" => row.to_s,
      "nonce" => self.bin_to_hex(nonce),
    }

    out = Typhoeus.get(self.keyderiv_url, :method => :post, :body => body)
    raise "Non-200 from keyderiv" unless out.response_code == 200

    box.decrypt(nonce, self.hex_to_bin(out.body.strip))
  end

  def self.get_encryption_key_1x(table, column, row)
    body = {
      "mode" => "encrypt",
      "table" => table.to_s,
      "column" => column.to_s,
      "row" => row.to_s
    }

    out = Typhoeus.get(self.keyderiv_url, :method => :post, :body => body)
    raise "Non-200 from keyderiv" unless out.response_code == 200
    return out.body.strip
  end

  def self.get_encryption_key(table, column, row)
    return self.get_encryption_key_2x(table, column, row) if self.keyderiv_use_2x
    self.get_encryption_key_1x(table, column, row)
  end

  def self.decrypt(table, column, row, data, opts = {})
    return nil if data.nil? || data.empty?
    encoding = opts[:encoding] || Encoding::UTF_8

    data = hex_to_bin(data)
    key = self.get_encryption_key(table, column, row)
    box = RbNaCl::SecretBox.new(hex_to_bin(key))

    nonce = data[0..(box.nonce_bytes - 1)]
    data = data[(box.nonce_bytes)..(data.length)]
    decrypted = box.decrypt(nonce, data)

    return decrypted.force_encoding(encoding)
  end

  def self.encrypt(table, column, row, data)
    key = self.get_encryption_key(table, column, row)
    box = RbNaCl::SecretBox.new(hex_to_bin(key))

    nonce = RbNaCl::Random.random_bytes(box.nonce_bytes)
    return bin_to_hex(nonce + box.encrypt(nonce, data))
  end

  ###########################################################################
  # Indexes
  ###########################################################################

  def self.get_index_key_2x(table, column)
    raise "keyderiv 2.x enabled but no secret provided" unless self.keyderiv_secret
    box = RbNaCl::SecretBox.new(self.keyderiv_secret)
    nonce = RbNaCl::Random.random_bytes(box.nonce_bytes)

    body = {
      "mode" => "index",
      "table" => table.to_s,
      "column" => column.to_s,
      "nonce" => self.bin_to_hex(nonce),
    }

    out = Typhoeus.get(self.keyderiv_url, :method => :post, :body => body)
    raise "Non-200 from keyderiv" unless out.response_code == 200

    box.decrypt(nonce, self.hex_to_bin(out.body.strip))
  end

  def self.get_index_key_1x(table, column)
    body = {
      "mode" => "index",
      "table" => table.to_s,
      "column" => column.to_s,
    }

    out = Typhoeus.get(self.keyderiv_url, :method => :post, :body => body)
    raise "Non-200 from keyderiv" unless out.response_code == 200
    return out.body.strip
  end

  def self.get_index_key(table, column)
    return self.get_index_key_2x(table, column) if self.keyderiv_use_2x
    self.get_index_key_1x(table, column)
  end

  def self.index(table, column, data)
    key = self.get_index_key(table, column)
    index = RbNaCl::PasswordHash.scrypt(data, hex_to_bin(key), @opslimit, @memlimit, 64)
    return bin_to_hex(index)
  end

  ###########################################################################
  # Password hashing
  ###########################################################################

  def self.password_hash(password, salt=nil)
    unless salt
      saltbytes = RbNaCl::PasswordHash::SCrypt::SALTBYTES
      salt = RbNaCl::Random.random_bytes(saltbytes)
    end

    hash = RbNaCl::PasswordHash.scrypt(password, salt, @opslimit, @memlimit, 64)

    return bin_to_hex(salt + hash)
  end

  def self.password_verify(hashed, password)
    return false if hashed.nil? || hashed.empty?

    saltbytes = RbNaCl::PasswordHash::SCrypt::SALTBYTES
    data = hex_to_bin(hashed)
    salt = data[0..(saltbytes - 1)]

    return Rack::Utils.secure_compare(hashed, self.password_hash(password, salt))
  end

  ###########################################################################
  # Token generation
  ###########################################################################

  def self.generate_token
    return bin_to_hex(RbNaCl::Random.random_bytes(@tokenlength))
  end

  ###########################################################################
  # Utility functions
  ###########################################################################

  def self.hex_to_bin(data)
    return data.scan(/../).map do |x|
      x.hex.chr
    end.join.encode(Encoding::BINARY)
  end

  def self.bin_to_hex(data)
    return data.each_byte.map do |b| 
      t = b.to_s(16)
      t = "0" + t if t.length == 1
      t
    end.join.encode(Encoding::BINARY)
  end
end
