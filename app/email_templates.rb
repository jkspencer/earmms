require 'erb'
require 'ostruct'
require 'addressable'
require 'sanitize'

class EARMMS::EmailTemplates
  attr_accessor :template_root
  attr_accessor :site_name
  attr_accessor :org_name
  attr_accessor :base_url

  def initialize(config = {})
    @template_root = config[:template_root] || File.join(EARMMS.root, "app", "views", "email_templates")
    @site_name = config[:site_name]
    @org_name = config[:org_name]
    @base_url = config[:base_url]
  end

  def password_reset(email, token)
    text_template = ERB.new(File.read(File.join(@template_root, "password_reset.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "password_reset.html.erb")))

    link = Addressable::URI.parse(@base_url)
    link += "/auth/reset/#{token}"

    data = OpenStruct.new()
    data.site_name = @site_name
    data.org_name = @org_name
    data.base_url = @base_url
    data.email = email
    data.reset_link = link.to_s

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end

  def alert(alert, role)
    text_template = ERB.new(File.read(File.join(@template_root, "alert.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "alert.html.erb")))

    alert_link = Addressable::URI.parse(@base_url)
    if alert[:section].to_s.downcase == "membership"
      alert_link += '/admin/alerts'
    else
      alert_link += '/system/alerts'
    end

    user_link = Addressable::URI.parse(@base_url)
    user_link += "/admin/user/edit/#{alert[:user] ? alert[:user].id.to_s : '0'}"

    data = OpenStruct.new()
    data.site_name = @site_name
    data.org_name = @org_name
    data.base_url = @base_url
    data.role = role
    data.alert = alert
    data.view_alert_link = alert_link
    data.view_user_link = user_link

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end

  def user_deletion(user, profile)
    text_template = ERB.new(File.read(File.join(@template_root, "user_deletion.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "user_deletion.html.erb")))

    data = OpenStruct.new()
    data.site_name = @site_name
    data.org_name = @org_name
    data.base_url = @base_url
    data.user = user
    data.profile = profile

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end

  def branch_meeting(branch, meeting)
    text_template = ERB.new(File.read(File.join(@template_root, "branch_meeting.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "branch_meeting.html.erb")))

    link = Addressable::URI.parse(@base_url)
    link += "/dashboard/meetings/#{branch.id}/#{meeting.id}"

    data = OpenStruct.new()
    data.site_name = @site_name
    data.org_name = @org_name
    data.base_url = @base_url
    data.branch = branch
    data.meeting = meeting
    data.meeting_link = link
    data.meeting_notes_html = meeting.decrypt(:notes).force_encoding("UTF-8")
    data.meeting_notes_text = Sanitize.fragment(
        data.meeting_notes_html,
        Sanitize::Config.merge(
            Sanitize::Config::DEFAULT,
            {
                :whitespace_elements => Sanitize::Config::DEFAULT[:whitespace_elements].merge({
                    "p" => { :before => "\n", :after => "\n" },
                }),
            }
        )
    )

    data.datetime = DateTime.parse(meeting.decrypt(:datetime))

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end

  def supporter_meeting_reminder(meetings)
    text_template = ERB.new(File.read(File.join(@template_root, "supporter_meeting_reminder.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "supporter_meeting_reminder.html.erb")))

    data = OpenStruct.new()
    data.site_name = @site_name
    data.org_name = @org_name
    data.base_url = @base_url
    data.meetings = meetings

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end

  def group_new_user(group, user)
    text_template = ERB.new(File.read(File.join(@template_root, "group_new_user.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "group_new_user.html.erb")))

    data = OpenStruct.new()
    data.site_name = @site_name
    data.org_name = @org_name
    data.base_url = @base_url
    data.group = group
    data.user = user

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end

  def user_changed_branch(user, branch, type)
    text_template = ERB.new(File.read(File.join(@template_root, "user_changed_branch.text.erb")))
    html_template = ERB.new(File.read(File.join(@template_root, "user_changed_branch.html.erb")))

    data = OpenStruct.new()
    data.site_name = @site_name
    data.org_name = @org_name
    data.base_url = @base_url
    data.user = user
    data.profile = EARMMS::Profile.for_user(user)
    data.branch = branch
    data.type = type

    b = data.instance_eval do
      binding
    end

    out = OpenStruct.new()
    out.content_text = text_template.result(b)
    out.content_html = html_template.result(b)

    out
  end
end
