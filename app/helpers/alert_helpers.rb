module EARMMS::AlertHelpers
  def gen_alert(section, type, message)
    alert = EARMMS::Alert.new(section: section.to_s, actioned: false, created: Time.now)
    alert.save

    alert.encrypt(:userid, current_user.id.to_s)
    alert.encrypt(:type, type.to_s)
    alert.encrypt(:data, message.to_s)
    alert.save

    alert.send_email!

    alert
  end
end
