module EARMMS::ApplicationHelpers
  require_relative './language_helpers'
  include EARMMS::LanguageHelpers

  require_relative './theme_helpers'
  include EARMMS::ThemeHelpers

  require_relative './alert_helpers'
  include EARMMS::AlertHelpers

  require_relative './user_helpers'
  include EARMMS::UserHelpers

  def flash(type, message)
    message = message.force_encoding("UTF-8")
    session[:flash] ||= []
    session[:flash] << {:type => type, :message => message}
  end

  def render_flashes
    return "" unless session[:flash]
    out = []

    session[:flash].each do |f|
      locals = {
        :type => f[:type].to_s.force_encoding("UTF-8"),
        :message => f[:message].force_encoding("UTF-8"),
      }

      out << haml(:flash, :locals => locals)
    end

    session[:flash] = []

    out.join("")
  end

  def site_name
    EARMMS.site_name
  end

  def org_name
    EARMMS.org_name
  end

  def current_prefix?(path = '/')
    request.path.start_with?(path) ? 'current' : nil
  end

  def current?(path = '/')
    request.path == path ? 'current' : nil
  end

  def is_maintenance?
    t = EARMMS::ConfigModel.where(key: 'maintenance').first
    return false unless t
    return t.value == 'yes'
  end

  def pretty_time_ago(time)
    a = (Time.now - time).to_i

    case a
    when 0..59
      "#{a} seconds ago"
    when 60..(3600 - 1)
      "#{(a / 60).to_i} minutes ago"
    when 3600..((3600 * 24) - 1)
      "#{(a / 3600).to_i} hours ago"
    when (3600 * 24)..(3600 * 24 * 30)
      "#{(a / (3600 * 24)).to_i} days ago"
    else
      time.strftime("%Y-%m-%d %H:%M")
    end
  end
end
