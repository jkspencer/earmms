module EARMMS::AuthHelpers
  def error_page(msg)
    flash(:error, msg)
    @title = "Log in or sign up"

    return haml :'auth/index'
  end

  def twofactor_error_page(msg)
    flash(:error, msg)
    @title = "Two-factor verification"

    return haml :'auth/twofactor'
  end
end
