module EARMMS::BranchAdminMeetingAttendanceHelpers
  def get_users_for_branch_meeting(meeting)
    meeting = meeting.id if meeting.respond_to? :id
    branch = EARMMS::BranchMeeting[meeting].branch
    branch = branch.id if branch.respond_to? :id

    pids = []
    users = []

    EARMMS::BranchMeetingAttendanceRecord.where(meeting: meeting).each do |at|
      profile = EARMMS::Profile[at.decrypt(:profile)]

      status = nil
      if at
        case at.decrypt(:attendance_status)
        when "present"
          status = :present
        when "apologies"
          status = :apologies
        else
          status = :unknown
        end
      end

      pids << profile.id
      users << {
        :uid => profile.user,
        :pid => profile.id,
        :at => at.respond_to?(:id) ? at.id : nil,

        :name => profile.decrypt(:full_name).force_encoding("UTF-8"),
        :status => status,
      }
    end

    EARMMS::ProfileFilter.perform_filter(:branch, "#{branch}:member").each do |p|
      profile = EARMMS::Profile[p.profile]

      next if pids.include? profile.id
      users << {
        :uid => profile.user,
        :pid => profile.id,
        :at => nil,

        :name => profile.decrypt(:full_name).force_encoding("UTF-8"),
        :status => :unknown,
      }
    end

    users.sort{|a, b| a[:name] <=> b[:name]}
  end
end
