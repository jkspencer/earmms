require 'openssl'
require 'base64'
require 'cgi'
require 'uri'

module EARMMS::DiscourseSSOHelpers
  def discourse_sso_enabled?
    return false unless EARMMS.app_config[:discourse_sso].respond_to?(:[])
    return false unless EARMMS.app_config[:discourse_sso][:enabled] == true

    # check secret
    return false unless EARMMS.app_config[:discourse_sso][:secret].is_a?(String)
    return false if EARMMS.app_config[:discourse_sso][:secret].strip.empty?

    true
  end

  def discourse_secret
    return nil unless discourse_sso_enabled?
    EARMMS.app_config[:discourse_sso][:secret]
  end

  def discourse_host_whitelist
    return [] unless discourse_sso_enabled?
    EARMMS.app_config[:discourse_sso][:host_whitelist]
  end

  def discourse_hmac_sign(data)
    return nil unless discourse_sso_enabled?
    OpenSSL::HMAC.hexdigest('SHA256', discourse_secret, data).strip.downcase
  end

  def discourse_hmac_valid?(data, sig)
    return false unless discourse_sso_enabled?

    calculated_hmac = discourse_hmac_sign(data)
    Rack::Utils.secure_compare(calculated_hmac, sig)
  end

  # Checks whether the user has the correct permissions to authenticate to
  # Discourse. The flow is:
  #
  #
  # - if user has role `discourse_sso:login`, immediately return true
  #
  # - if the `:limit_membership_status` config option is set, check whether the
  #   user has one of the permitted membership statuses, if not immediately
  #   return false
  #
  # - return true
  def discourse_user_can_log_in?(opts = {})
    user = current_user
    if opts[:user]
      user = opts[:user]
    else
      return false unless logged_in?
    end

    # immediately allow if user has the right role
    if has_role?('discourse_sso:login', :user => user)
      return true
    end

    # get user's membership status
    user_profile = EARMMS::Profile.for_user(user)
    return false unless user_profile
    user_membership_status = user_profile.decrypt(:membership_status).strip.downcase
    user_membership_status = nil unless EARMMS.membership_status_options.keys.include?(user_membership_status.to_sym)

    # check if the user's membership status is in the allowed list
    if EARMMS.app_config[:discourse_sso].key?(:limit_membership_status)
      allowed_membership_statuses = EARMMS.app_config[:discourse_sso][:limit_membership_status]
      if !(allowed_membership_statuses.include?(user_membership_status))
        return false
      end
    end

    true
  end

  def discourse_groups
    EARMMS.app_config[:discourse_sso][:groups]
  end

  # Gets the list of groups the given user belongs to.
  def discourse_groups_for_user(user)
    return [] unless user
    profile = EARMMS::Profile.for_user(user)
    return [] unless profile

    user_membership_status = profile.decrypt(:membership_status).strip.downcase

    out = []
    discourse_groups.each do |groupdesc|
      grant = false

      case groupdesc[:grant_on]
      when :membership_status
        if groupdesc[:statuses].include?(user_membership_status)
          grant = true
        end

      when :role
        groupdesc[:roles].each do |r|
          if has_role?(r, :user => user)
            grant = true
          end
        end
      end

      if grant
        out << groupdesc
      end
    end

    out
  end

  # Parses SSO data from the session, returning nil if the data is invalid.
  #
  # This function expects the data to be a hash with keys `:payload` and
  # `:signature`.
  #
  # Returns a hash with keys :nonce and :return_url.
  def discourse_sso_parse_data(data)
    return nil unless data.respond_to?(:[])
    return nil unless data[:payload]
    return nil unless data[:signature]

    payload = data[:payload]
    signature = data[:signature]

    # check hmac
    return nil unless discourse_hmac_valid?(payload, signature)

    # un-base64 the payload
    payload = Base64.decode64(payload)

    # decode query string
    payload = CGI.parse(payload)

    nonce = payload["nonce"].first
    return_url = payload["return_sso_url"].first

    {
      :nonce => nonce,
      :return_url => return_url,
    }
  end

  # Generates an SSO response containing the given user's information.
  #
  # This function returns a hash with keys `:payload` and `:signature`, ready
  # to be added to the callback URL.
  def discourse_sso_generate_response(nonce, opts = {})
    return nil unless nonce

    user = current_user
    if opts[:user]
      user = opts[:user]
    end

    return nil unless user
    profile = EARMMS::Profile.for_user(user)
    return nil unless profile

    is_admin = false
    if EARMMS.app_config[:discourse_sso][:admin_roles].respond_to?(:[])
      EARMMS.app_config[:discourse_sso][:admin_roles].each do |r|
        if has_role?(r, :user => user)
          is_admin = true
        end
      end
    end

    groups = discourse_groups()
    user_groups = discourse_groups_for_user(user)

    payload = {
      "nonce" => nonce,
      "email" => user.email,
      "external_id" => user.id.to_s,
      "name" => profile.decrypt(:full_name),
      "admin" => is_admin,
      "add_groups" => user_groups.map{|x| x[:name]}.join(','),
      "remove_groups" => groups.reject{|x| user_groups.include?(x)}.map{|x| x[:name]}.join(','),
    }

    payload_encoded = URI.encode_www_form(payload)
    payload_encoded = Base64.encode64(payload_encoded)

    # generate hmac
    signature = discourse_hmac_sign(payload_encoded)

    {
      :payload => payload_encoded,
      :signature => signature,
    }
  end
end
