module EARMMS::GroupHelpers
  def get_group_ids_user_is_member(user, opts = {})
    user = user.id if user.respond_to?(:id)
    EARMMS::GroupMemberFilter.perform_filter(:user, user.to_s).map do |cmf|
      cm = EARMMS::GroupMember[cmf.group_member]
      next nil unless cm

      if opts[:only_admin]
        unless cm.decrypt(:roles).split(",").include?("admin")
          next nil
        end
      end

      cm.group
    end.compact
  end

  def get_groups_hash_by_type(ids, opts = {})
    output = {}
    get_groups_hash(ids, opts).each do |id, group|
      output[group[:type]] ||= {}
      output[group[:type]][id] = group
    end

    output
  end

  def get_groups_hash(ids, opts = {})
    if ids == :all
      ids = EARMMS.database[EARMMS::Group.table_name].select{id}.map{|x| x[:id]}
    end

    groups = ids.map do |cid|
      group = EARMMS::Group[cid]
      next unless group

      [group.id, group]
    end.compact.to_h

    user_membership = {}
    if opts[:user]
      user = opts[:user]
      user = user.id if user.respond_to?(:id)

      EARMMS::GroupMemberFilter.perform_filter(:user, user.to_s).map do |cmf|
        cm = EARMMS::GroupMember[cmf.group_member]
        next unless cm

        group = groups[cm.group]
        next unless group

        roles = cm.decrypt(:roles).split(",").map do |r|
          r.strip.downcase
        end

        user_membership[group.id] = {
          :user_id => user,
          :roles => roles,
          :admin => roles.include?("admin"),
          :group_member_id => cm.id,
          :group_member => cm,
        }
      end
    end

    groups.map do |cid, c|
      unless opts[:admin] || c.is_joinable
        unless user_membership.keys.include?(c.id)
          next nil
        end
      end

      if opts[:user] && opts[:only_user_joined]
        unless user_membership.keys.include?(c.id)
          next nil
        end
      end

      if opts[:type]
        next nil unless c.type == opts[:type].to_s
      end

      res = {
        :instance => c,
        :id => c.id,
        :type => c.type,
        :name => c.decrypt(:name).force_encoding("UTF-8"),
        :description => c.decrypt(:description).force_encoding("UTF-8"),
        :agreement => c.decrypt(:user_agreement).force_encoding("UTF-8"),
        :is_joinable => c.is_joinable,
        :restrict_to_member => c.restrict_to_member,
        :member_count => EARMMS::GroupMember.where(group: c.id).count,
      }

      if user_membership.keys.include?(c.id)
        res[:user_membership] = user_membership[c.id]
      end

      if opts[:link_to_edit]
        if opts[:link_to_edit] == :user
          res[:link] = "/user/group/edit/#{c.id}"
        elsif opts[:link_to_edit] == :groupadmin
          res[:link] = "/group/edit/#{c.id}"
        elsif opts[:link_to_edit] == :admin
          res[:link] = "/admin/group/edit/#{c.id}"
        end
      end

      [c.id, res]
    end.compact.to_h
  end

  def get_group_members(group)
    EARMMS::GroupMember.where(group: group.id).all.map do |cm|
      uid = cm.decrypt(:user).to_i
      user = EARMMS::User[uid]
      next nil unless user
      profile = EARMMS::Profile.for_user(user)
      next nil unless profile

      roles = cm.decrypt(:roles).split(",").map do |role|
        role.strip.downcase
      end

      branch_info = nil
      branch = profile.decrypt(:branch).to_i
      branch = EARMMS::Branch[branch] if branch
      if branch
        branch_info = {
          :id => branch.id,
          :branch => branch,

          :name => branch.decrypt(:name),
        }
      end

      res = {
        :user => user,
        :profile => profile,
        :group_member => cm,
        :group_member_id => cm.id,
        :name => profile.decrypt(:full_name)&.force_encoding("UTF-8"),
        :email => user.email,
        :membership_status => profile.decrypt(:membership_status)&.strip&.downcase,
        :branch => branch_info,
        :roles => roles,
        :admin => roles.include?("admin"),
      }

      [user.id, res]
    end.compact.to_h
  end
end
