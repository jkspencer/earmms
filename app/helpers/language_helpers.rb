module EARMMS::LanguageHelpers
  def languages
    EARMMS.languages
  end

  def current_language
    return session[:lang] if session.key?(:lang)
    EARMMS.default_language
  end

  def t(translation_key, values = {})
    language = current_language

    if language == "translationkeys"
      return translation_key.to_s
    end

    text = EARMMS.languages[language][translation_key]
    text = EARMMS.languages[EARMMS.default_language][translation_key] unless text
    return "##MISSING(#{translation_key.to_s})##" if (!text and ENV['RACK_ENV'] == 'development')

    erb = ERB.new(text)

    data = OpenStruct.new
    values.keys.each do |key|
      data.send("#{key}=".to_sym, values[key])
    end

    b = data.instance_eval do
      binding
    end

    erb.result(b)
  end
end
