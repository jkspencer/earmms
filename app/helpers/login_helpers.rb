module EARMMS::LoginHelpers
  def error_page(msg)
    flash(:error, msg)
    @title = "Log in"

    return haml :'auth/login/index'
  end

  def twofactor_error_page(msg)
    flash(:error, msg)
    @title = "Two-factor verification"

    return haml :'auth/login/twofactor'
  end
end
