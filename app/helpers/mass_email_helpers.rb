module EARMMS::MassEmailHelpers
  def get_email_targets(opts = {})
    ignore_roles = opts[:ignore_roles] || false

    targets = []

    # branches
    EARMMS::Branch.all.each do |b|
      if ignore_roles || has_role?("email:branch:#{b.id}")
        name = b.decrypt(:name)

        targets << {
          :name => "Branch members: #{name}".force_encoding("UTF-8"),
          :target => "branch:#{b.id}:member",
          :type => :branch,
          :branch => name.force_encoding("UTF-8"),
          :branchid => b.id,
          :membership_status => :member,
          :count => EARMMS::ProfileFilter.perform_filter(:branch, "#{b.id.to_s}:member").count,
        }

        targets << {
          :name => "Branch supporters: #{name}".force_encoding("UTF-8"),
          :target => "branch:#{b.id}:supporter",
          :type => :branch,
          :branch => name.force_encoding("UTF-8"),
          :branchid => b.id,
          :membership_status => :supporter,
          :count => EARMMS::ProfileFilter.perform_filter(:branch, "#{b.id.to_s}:supporter").count,
        }
      end
    end

    # groups
    groups = {}
    if ignore_roles || has_role?("email:all_groups")
      EARMMS::Group.all.each do |group|
        groups[group.id] = group
      end

    else
      EARMMS::GroupMemberFilter.perform_filter(:user, current_user.id.to_s).each do |cmf|
        cm = EARMMS::GroupMember[cmf.group_member]
        next unless cm

        group = EARMMS::Group[cm.group]
        next unless group
        next if groups.key?(group.id)

        roles = cm.decrypt(:roles).split(",")
        next unless roles.include?("email") || roles.include?("admin")

        groups[group.id] = group
      end
    end

    groups.each do |id, group|
      targets << {
        :name => "Group members: #{group.decrypt(:name)}".force_encoding("UTF-8"),
        :target => "group:#{group.id}",
        :type => :group,
        :group => group,
        :groupid => id,
        :count => EARMMS::GroupMember.where(group: group.id).count,
      }
    end

    # membership / supporters / all users
    if ignore_roles || has_role?("email:members")
      targets << {
        :name => "All members",
        :target => "status:member",
        :type => :membership,
        :count => EARMMS::ProfileFilter.perform_filter(:membership_status, "member").count,
      }

      targets << {
        :name => "All supporters",
        :target => "status:supporter",
        :type => :membership,
        :count => EARMMS::ProfileFilter.perform_filter(:membership_status, "supporter").count,
      }

      targets << {
        :name => "All registered users",
        :target => "all",
        :type => :all,
        :count => EARMMS::Profile.count,
      }
    end

    targets
  end
end
