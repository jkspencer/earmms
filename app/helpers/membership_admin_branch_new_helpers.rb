module EARMMS::MembershipAdminBranchNewHelpers
  def error_page(msg)
    flash :error, msg

    @title = "New branch"
    haml :'membership_admin/branch/new'
  end
end
