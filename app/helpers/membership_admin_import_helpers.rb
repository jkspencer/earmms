require 'csv'

module EARMMS::MembershipAdminImportHelpers
  def import_csv_headers
    [
      [:full_name, "Full name"],
      [:email_address, "Email address"],
      [:address, "Address"],
      [:home_phone, "Home phone"],
      [:mobile_phone, "Mobile phone"],
      [:membership_status, "Membership status"],
      [:branch, "Branch"],
      [:member_since, "Member since date"],
    ]
  end

  def import_parse_csv(data)
    output = []

    default_branch = EARMMS::Branch.order(Sequel.asc(:id)).first
    default_branch_name = default_branch.decrypt(:name)
    default_branch_name = default_branch_name.force_encoding("UTF-8") if default_branch_name.respond_to?(:force_encoding)
    default_branch = [default_branch.id, default_branch_name]

    branches = EARMMS::Branch.map do |b|
      n = b.decrypt(:name)
      n = n.force_encoding("UTF-8") if n.respond_to?(:force_encoding)
      [b.id, n]
    end

    c = CSV.new(data, :headers => import_csv_headers.map(&:first).map(&:to_s))
    c.each do |row|
      t = {}

      import_csv_headers.map(&:first).each do |header|
        r = row[header.to_s]
        if r.nil? || r.empty?
          t[header] = nil
          next
        end

        r = r.to_s
        r = r.strip
        r = r.force_encoding(Encoding::UTF_8)
        if r.start_with?('"') && r.end_with?('"')
          r = r[1..-2].strip
        end

        t[header] = r
      end

      # Downcase email
      t[:email_address] = t[:email_address]&.strip&.downcase

      # Branch selection
      t[:branch] ||= ""
      branch = branches.select do |b|
        b.last.downcase == t[:branch].strip.downcase
      end.first
      branch = default_branch unless branch
      t[:branch] = branch

      # Membership status selection
      t[:membership_status] ||= "supporter"
      case t[:membership_status].strip.downcase
      when "member"
        t[:membership_status] = "member"
      else
        t[:membership_status] = "supporter"
      end

      # Member since date
      if t[:member_since] && t[:member_since].strip != ""
        begin
          t[:member_since] = Time.parse(t[:member_since].strip)
        rescue
          t[:member_since] = Time.parse(Time.now.strftime("%Y-%m-%d"))
        end
      else
        t[:member_since] = Time.parse(Time.now.strftime("%Y-%m-%d"))
      end

      output << t
    end

    output
  end

  def custom_profile_fields
    EARMMS.custom_user_fields.map do |f|
      {
        :name => f[:name].to_s.force_encoding("UTF-8"),
        :friendly => f[:friendly].force_encoding("UTF-8"),
        :type => (f[:type].to_s || "text").force_encoding("UTF-8"),
        :required => f[:required] || false,
        :default => f[:default] || nil,
        :select_options => f[:type] == 'select' ? f[:select_options] : nil,
        :display_only_if => f[:display_only_if] || nil,
        :display_only_if_membership_status => f[:display_only_if_membership_status] || nil,
        :is_custom => true,
      }
    end
  end
end
