module EARMMS::MembershipAdminUserEditRolesHelpers
  def get_available_roles(user)
    user_roles = user.get_roles

    roles = [
      {
        :name => "Mass emailing",
        :roles => [
          {
            :role => "email:members",
            :desc => "Send emails to entire membership",
            :has => user_roles.include?("email:members"),
          },
          {
            :role => "email:all_groups",
            :desc => "Send emails to members of all groups",
            :has => user_roles.include?("email:all_groups"),
          },
        ],
      },
      {
        :name => "Membership admin",
        :warning => t(:'admin/membership_admin_role_warning'),
        :roles => [
          {
            :role => "admin:*",
            :desc => "Full membership admin privileges",
            :has => user_roles.include?("admin:*"),
          },
          {
            :role => "admin:alert_emails",
            :desc => "Receive membership admin alert emails",
            :has => user_roles.include?("admin:alert_emails"),
          },
        ],
      },
    ]

    EARMMS::Branch.all.reverse.each do |branch|
      desc = {
        :name => "Branch: #{branch.decrypt(:name)}",
        :roles => [
          {
            :role => "branch:#{branch.id}:*",
            :desc => "Full branch admin privileges",
            :has => user_roles.include?("branch:#{branch.id}:*"),
          },
          {
            :role => "branch:#{branch.id}:alert_emails",
            :desc => "Receive branch alert emails",
            :has => user_roles.include?("branch:#{branch.id}:alert_emails"),
          },
          {
            :role => "email:branch:#{branch.id}",
            :desc => "Send emails to branch members",
            :has => user_roles.include?("email:branch:#{branch.id}"),
          },
        ],
      }

      roles.unshift(desc)
    end

    roles
  end
end
