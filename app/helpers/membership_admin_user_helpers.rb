module EARMMS::MembershipAdminUserHelpers
  def current_uri_with_query(type, query)
    t = Addressable::URI.parse(request.path)
    t.query_values = {
      "searchtype" => type,
      "query" => query,
    }

    t.to_s
  end

  def custom_profile_fields
    EARMMS.custom_user_fields.map do |f|
      field_desc = {
        :name => f[:name].to_s.force_encoding("UTF-8"),
        :friendly => f[:friendly].force_encoding("UTF-8"),
        :type => (f[:type].to_s || "text").force_encoding("UTF-8"),
        :required => f[:required] || false,
        :default => f[:default] || nil,
        :select_options => f[:type] == 'select' ? f[:select_options] : nil,
        :display_only_if => f[:display_only_if] || nil,
        :display_only_if_membership_status => f[:display_only_if_membership_status] || nil,
        :is_custom => true,
      }

      [field_desc[:name], field_desc]
    end.to_h
  end

  def do_search(type, query)
    type = type.to_s.strip.downcase
    query = query.to_s.strip.downcase

    results = []

    if type == "uid"
      EARMMS::User.where(id: query.to_i).each do |u|
        profile = EARMMS::Profile.for_user(u)
        next nil unless profile

        results << {
          :user => u,
          :profile => profile,
        }
      end

    elsif type == "email"
      EARMMS::User.where(email: query).each do |u|
        profile = EARMMS::Profile.for_user(u)
        next nil unless profile

        results << {
          :user => u,
          :profile => profile,
        }
      end

    elsif type == "creation_before"
      t = Time.parse(query)
      EARMMS::User.where{creation < t}.each do |u|
        profile = EARMMS::Profile.for_user(u)
        next nil unless profile

        results << {
          :user => u,
          :profile => profile,
        }
      end

    elsif type == "creation_after"
      t = Time.parse(query)
      EARMMS::User.where{creation >= t}.each do |u|
        profile = EARMMS::Profile.for_user(u)
        next nil unless profile

        results << {
          :user => u,
          :profile => profile,
        }
      end

    elsif ["full_name", "phone_home", "phone_mobile", "prisoner_number", "branch", "membership_status"].include? type
      EARMMS::ProfileFilter.perform_filter(type.to_sym, query).each do |f|
        profile = EARMMS::Profile[f.profile]
        next nil unless profile
        user = EARMMS::User[profile.user]
        next nil unless user

        results << {
          :user => user,
          :profile => profile,
        }
      end

    elsif type.start_with? "custom_"
      field_name = type.split("_")[1..-1].join("_")
      unless custom_profile_fields.keys.include? field_name
        return nil
      end

      EARMMS::ProfileFilter.perform_filter("custom_#{field_name}".to_sym, query).each do |f|
        profile = EARMMS::Profile[f.profile]
        next nil unless profile
        user = EARMMS::User[profile.user]
        next nil unless user

        results << {
          :user => user,
          :profile => profile,
        }
      end

    else
      return nil
    end

    results
  end

  def do_search_get_data(results)
    out = []

    results.each do |r|
      r[:user_name] = r[:profile].decrypt(:full_name).force_encoding("UTF-8")
      r[:creation] = r[:user].creation

      out << r
    end

    out
  end

  def do_search_sort(results, sort)
    sort = sort.to_s.strip.downcase

    if sort == "creation"
      results.sort! do |a, b|
        a[:user].creation <=> b[:user].creation
      end

    elsif sort == "name"
      results.sort! do |a, b|
        a[:profile].decrypt(:full_name).downcase <=> b[:profile].decrypt(:full_name).downcase
      end

    else
      results.sort! do |a, b|
        a[:user].id <=> b[:user].id
      end
    end

    results
  end
end
