module EARMMS::SignupHelpers
  def signups_allowed?
    t = EARMMS::ConfigModel.where(key: 'signups').first
    return true unless t
    return t.value == 'yes'
  end

  def get_invite(t)
    return nil if t.nil?
    return nil if t.strip.empty?

    invite = EARMMS::Token.where(token: t, use: 'invite').first
    return nil unless invite.user.nil?
    return nil unless invite.valid?

    invite
  end

  def error_page(msg)
    flash(:error, msg)
    @title = "Sign up"

    haml :'auth/signup/index'
  end

  def custom_profile_fields
    EARMMS.custom_user_fields.map do |f|
      {
        :name => f[:name].to_s.force_encoding("UTF-8"),
        :friendly => f[:friendly].force_encoding("UTF-8"),
        :type => (f[:type].to_s || "text").force_encoding("UTF-8"),
        :required => f[:required] || false,
        :default => f[:default] || nil,
        :select_options => f[:type] == 'select' ? f[:select_options] : nil,
        :display_only_if => f[:display_only_if] || nil,
        :display_only_if_membership_status => f[:display_only_if_membership_status] || nil,
        :is_custom => true,
      }
    end
  end

  def custom_field_json
    custom_fields = custom_profile_fields.map do |f|
      [f[:name], f[:default] || nil]
    end.to_h

    JSON.generate(custom_fields)
  end
end
