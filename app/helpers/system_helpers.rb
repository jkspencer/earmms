module EARMMS::SystemConfigurationHelpers
  def edit_error_page(msg)
    flash(:error, msg)
    @title = "System configuration"

    haml :'system/config/edit'
  end
end
