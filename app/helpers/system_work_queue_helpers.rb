module EARMMS::SystemWorkQueueHelpers
  def work_queue_data(m)
    created = m.decrypt(:created)
    if created == ""
      created = Time.at(0)
    else
      created = Time.parse(created)
    end

    run_at = m.decrypt(:run_at)
    if run_at == ""
      run_at = Time.at(0)
    else
      run_at = Time.parse(run_at)
    end

    started = m.decrypt(:started)
    if started == ""
      started = Time.at(0)
    else
      started = Time.parse(started)
    end

    finished = m.decrypt(:finished)
    if finished == ""
      finished = Time.at(0)
    else
      finished = Time.parse(finished)
    end

    log_uri = Addressable::URI.parse("/system/workqueue/log/#{m[:id]}")
    log_uri.query_values = {
      :back => "/system/workqueue/?page=#{@current_page}&status=#{@status}"
    }

    {
      :id => m.id,
      :task => m.decrypt(:task).force_encoding("UTF-8"),
      :data => m.decrypt(:data).force_encoding("UTF-8"),
      :status => m.status,
      :log => m.decrypt(:log).force_encoding("UTF-8"),
      :created => created,
      :run_at => run_at,
      :started => started,
      :finished => finished,
      :log_uri => log_uri,
    }
  end
end
