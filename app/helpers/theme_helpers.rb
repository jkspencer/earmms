module EARMMS::ThemeHelpers 
  def find_template(views, name, engine, &block)
    if views.instance_of? Hash
      _, folder = views.detect do |k, v|
        engine == Tilt[k]
      end

      folder ||= views[:default]
    else
      folder = views
    end

    begin
      raise "no theme dir" unless settings.theme_dir
      theme_views = File.join(settings.theme_dir, "views")

      found = false
      Tilt.default_mapping.extensions_for(engine).each do |extension|
        fn = "#{name}.#{extension}"
        if File.file?(File.join(theme_views, fn))
          found = fn
        end
      end

      raise "template not in theme dir" unless found
      super(theme_views, name, engine, &block)
    rescue => e
      super(folder, name, engine, &block)
    end
  end

  def theme_page_header(_page)
    begin
      return haml("theme/page_header/#{_page.to_s}".to_sym)
    rescue Errno::ENOENT
      return ""
    end
  end

  def theme_page_footer(_page)
    begin
      return haml("theme/page_footer/#{_page.to_s}".to_sym)
    rescue Errno::ENOENT
      return ""
    end
  end
end
