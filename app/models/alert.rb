require 'mail'

class EARMMS::Alert < Sequel::Model
  def self.section_type_name(section, type)
    types = {
      :membership => {
        :membership_status => "Membership status changes",
        :admin_force_edit_user => "User profile edited by admin",
        :user_status_change_req => "Membership status change request",
        :user_branch_change_req => "Branch change request",
        :user_deletion => "User deletion",
        :test => "Test alerts",
      },
      :system => {
        :config_change => "Configuration changes",
        :role_edit => "User role changes",
        :data_correction => "Data correction",
        :email_queue_error => "Email queue error",
        :test => "Test alerts",
      },
      :default => "Unknown alert type",
    }

    this_section = types[section.to_sym] || {}
    this_section[type.to_sym] || types[:default]
  end

  def self.parse_message(section, type, data)
    case section
    when :membership
      case type
      when :user_status_change_req
        d = JSON.parse(data)
        return "Status change request: from \"#{d["from_status"]}\" to \"#{d["to_status"]}\""
      when :user_branch_change_req
        d = JSON.parse(data)
        return "Branch change request: from \"#{d["from_branch"]}\" (id #{d["from_branch_id"]}) to \"#{d["to_branch"]}\" (id #{d["to_branch_id"]})"
      else
        return data
      end
    else
      return data
    end
  end

  def to_h
    user = nil
    profile = nil
    if self.userid
      user = EARMMS::User[self.decrypt(:userid).to_i]
      profile = EARMMS::Profile.for_user(user)
    end

    section = self.section.downcase.to_sym
    type = self.decrypt(:type).downcase.to_sym
    data = self.decrypt(:data)

    {
      :alertid => self.id,
      :created => self.created,

      :actioned => self.actioned,
      :actioned_ts => self.actioned_ts,

      :section => section,
      :type => type,
      :type_friendly => self.class.section_type_name(section, type).force_encoding("UTF-8"),

      :data => data,
      :message => self.class.parse_message(section, type, data).force_encoding("UTF-8"),

      :user => user,
      :profile => profile,
    }
  end

  def self.type_h(section, type)
    section = section.to_s.downcase.to_sym
    type = type.to_s.downcase.to_sym

    {
      :code => type,
      :name => self.section_type_name(section, type),
      :alerts => [],
    }
  end

  def self.non_actioned_grouped_for_section(section)
    alerts = {}

    self.where(section: section.to_s, actioned: false).each do |a|
      type = a.decrypt(:type).downcase.to_sym
      alerts[type] ||= self.type_h(section, type)
      alerts[type][:alerts] << a.to_h
    end

    alerts
  end

  def self.actioned_page_for_section(section, page = 1)
    alerts = []
    page_size = 25

    ds = self.where(section: section.to_s, actioned: true).order(Sequel.desc(:created)).paginate(page, page_size)
    ds.each do |a|
      alerts << a.to_h
    end

    {
      :alerts => alerts,
      :page => ds.current_page,
      :total_pages => ds.page_count,
    }
  end

  def send_email!
    alert = self.to_h

    role = "system:alert_emails"
    if self.section.to_s == "membership"
      role = "admin:alert_emails"
    end

    to_addresses = []
    EARMMS::UserHasRole.where(role: role).map(&:user).each do |uid|
      u = EARMMS::User[uid]
      if u
        to_addresses << u.email
      end
    end

    if !to_addresses.empty?
      text = EARMMS.email_templates.alert(alert, role)

      qm = EARMMS::EmailQueue.new(creation: Time.now)
      qm.save
      qm.queue_status = 'queued'
      qm.encrypt(:recipients, JSON.generate({"type" => "list", "list" => to_addresses}))
      qm.encrypt(:subject, EARMMS.email_subject("Alert (#{alert[:section].to_s}) - #{alert[:type_friendly]}"))
      qm.encrypt(:content, text.content_text)
      qm.encrypt(:content_html, text.content_html)
      qm.save
    end
  end

  def dismiss!
    self.actioned = true
    self.actioned_ts = Time.now
    self.save
  end
end
