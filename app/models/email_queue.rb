class EARMMS::EmailQueue < Sequel::Model(:email_queue)
  include EARMMS::MassEmailHelpers

  def get_recipient_list
    recipient_json = self.decrypt(:recipients)
    return [] unless recipient_json
    return [] if recipient_json.strip == ""

    recipient_json = JSON.parse(recipient_json)
    return [] unless recipient_json.key?("type")

    type = recipient_json["type"].strip.downcase
    if type == "list"
      return [] unless recipient_json.key?("list")
      return [] unless recipient_json["list"].is_a?(Array)
      return recipient_json["list"]

    elsif type == "mass_email_target"
      users = []
      target = recipient_json["target"]
      return [] unless target
      return [] if target.strip == ""

      possible_targets = get_email_targets(:ignore_roles => true).map do |t|
        [t[:target], t]
      end.to_h

      return [] unless possible_targets.key?(target)

      t = target.split(":")
      if t[0] == "branch"
        branch_id = t[1].to_i
        status = t[2]

        branch = EARMMS::Branch[branch_id]
        return [] unless branch

        EARMMS::ProfileFilter.perform_filter(:branch, "#{branch.id}:#{status}").each do |x|
          p = EARMMS::Profile[x.profile]
          next unless p
          u = EARMMS::User[p.user]
          next unless u

          users << u.email if u.email
        end

      elsif t[0] == "group"
        group_id = t[1].to_i
        group = EARMMS::Group[group_id]
        return [] unless group

        EARMMS::GroupMember.where(group: group.id).each do |cm|
          u = cm.decrypt(:user).to_i
          u = EARMMS::User[u] if u
          next unless u

          users << u.email if u.email
        end

      elsif t[0] == "status"
        status = t[1].strip.downcase

        EARMMS::ProfileFilter.perform_filter(:membership_status, status).each do |x|
          p = EARMMS::Profile[x.profile]
          next unless p
          u = EARMMS::User[p.user]
          next unless u

          users << u.email if u.email
        end

      elsif t[0] == "all"
        EARMMS::User.each do |u|
          users << u.email if u.email
        end

      else
        return []
      end

      return users
    end

    # if we get here, we have an unknown type, so just return no addresses
    []
  end
end
