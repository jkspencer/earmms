class EARMMS::Group < Sequel::Model(:groups)
end

class EARMMS::GroupMember < Sequel::Model(:group_members)
end

class EARMMS::GroupMemberFilter < Sequel::Model(:group_member_filters)
  def self.clear_filters_for(cm)
    cm = cm.id if cm.respond_to?(:id)
    self.where(:group_member => cm).delete
  end

  def self.create_filters_for(cm)
    fields = [
      :user,
      :roles
    ]

    filters = []

    # whole field
    fields.each do |f|
      s = cm.decrypt(f).strip.downcase
      s = s.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => "")
      EARMMS.filter_strip_chars.each do |x|
        s = s.gsub(x, "")
      end

      e = EARMMS::Crypto.index("CaucusMember", f.to_s.downcase, s)
      f = self.new(group_member: cm.id, filter_label: f.to_s, filter_value: e)
      f.save

      filters << f
    end

    # individual roles
    cm.decrypt(:roles).strip.downcase.split(",").each do |partial|
      partial = partial.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => "")
      EARMMS.filter_strip_chars.each do |x|
        partial = partial.gsub(x, "")
      end

      e = EARMMS::Crypto.index("CaucusMember", "roles", partial)
      f = self.new(group_member: cm.id, filter_label: "roles", filter_value: e)
      f.save

      filters << f
    end

    filters
  end

  def self.perform_filter(column, search)
    s = search.strip.downcase
    s = s.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => "")
    EARMMS.filter_strip_chars.each do |x|
      s = s.gsub(x, "")
    end

    e = EARMMS::Crypto.index("CaucusMember", column.to_s.downcase, s)
    return self.where(filter_label: column.to_s.downcase, filter_value: e)
  end
end
