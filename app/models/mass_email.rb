class EARMMS::MassEmail < Sequel::Model
  def view_token_generate!
    self.view_token = EARMMS::Crypto.generate_token
  end

  def view_token_link
    link = Addressable::URI.parse(EARMMS.base_url)
    link += "/massemail/pubview/#{self.view_token}"
    link.to_s
  end
end
