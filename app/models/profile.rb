class EARMMS::Profile < Sequel::Model
  def self.for_user(user)
    user = user.id if user.respond_to?(:id)
    return self.where(user: user).first
  end
end

class EARMMS::ProfileFilter < Sequel::Model
  def self.clear_filters_for(profile)
    profile = profile.id if profile.respond_to? :id
    self.where(profile: profile).delete
  end

  def self.create_filters_for(profile)
    fields = [
      :full_name,
      :phone_home,
      :phone_mobile,
      :prisoner_number,
      :membership_status,
    ]

    filters = []
    fields.each do |f|
      s = profile.decrypt(f).strip.downcase
      s = s.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => "")
      EARMMS.filter_strip_chars.each do |x|
        s = s.gsub(x, "")
      end

      e = EARMMS::Crypto.index("Profile", f.to_s.downcase, s)

      f = self.new(profile: profile.id, filter_label: f.to_s, filter_value: e)
      f.save

      filters << f
    end

    # partial name filter
    profile.decrypt(:full_name).strip.downcase.split(" ").map{|x| x.split("-")}.flatten.each do |partial|
      partial = partial.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => "")
      EARMMS.filter_strip_chars.each do |x|
        partial = partial.gsub(x, "")
      end

      e = EARMMS::Crypto.index("Profile", "full_name", partial)
      f = self.new(profile: profile.id, filter_label: "full_name", filter_value: e)
      f.save

      filters << f
    end

    # branch filter
    branch = "#{profile.decrypt(:branch).strip.downcase}:#{profile.decrypt(:membership_status).strip.downcase == "member" ? "member" : "supporter"}"
    e = EARMMS::Crypto.index("Profile", "branch", branch)
    f = self.new(profile: profile.id, filter_label: "branch", filter_value: e)
    f.save
    filters << f

    # custom fields
    profile_custom_json = profile.decrypt(:custom_fields)
    if profile_custom_json && profile_custom_json != ""
      profile_custom_json = JSON.parse(profile_custom_json)
    else
      profile_custom_json = {}
    end

    profile_custom_json.keys.each do |k|
      # full text filter
      if profile_custom_json[k] == nil
        ft = 'unknown'
      else
        ft = profile_custom_json[k].strip.downcase
      end

      ft = ft.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => "")
      EARMMS.filter_strip_chars.each do |x|
        ft = ft.gsub(x, "")
      end

      e = EARMMS::Crypto.index("Profile", "custom_#{k}", ft)
      f = self.new(profile: profile.id, filter_label: "custom_#{k}", filter_value: e)
      f.save
      filters << f

      # partial text filter
      if profile_custom_json[k]
        partial_values = profile_custom_json[k].strip.downcase.split(" ").map{|x| x.split("-")}.flatten
        if partial_values.length > 1
          partial_values.each do |partial|
            partial = partial.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => "")
            EARMMS.filter_strip_chars.each do |x|
              partial = partial.gsub(x, "")
            end

            e = EARMMS::Crypto.index("Profile", "custom_#{k}", partial)
            f = self.new(profile: profile.id, filter_label: "custom_#{k}", filter_value: e)
            f.save

            filters << e
          end
        end
      end
    end

    filters
  end

  def self.perform_filter(column, search)
    s = search.strip.downcase
    s = s.encode(Encoding::UTF_8, :invalid => :replace, :undef => :replace, :replace => "")
    EARMMS.filter_strip_chars.each do |x|
      s = s.gsub(x, "")
    end

    e = EARMMS::Crypto.index("Profile", column.to_s.downcase, s)
    return self.where(filter_label: column.to_s.downcase, filter_value: e)
  end
end
