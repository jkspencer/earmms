class EARMMS::Token < Sequel::Model
  def self.generate_session(user)
    user = user.id if user.respond_to?(:id)
    t = EARMMS::Crypto.generate_token
    now = Time.now

    self.new(user: user, use: 'session', token: t, created: now, last_used: now, valid: true)
  end

  def self.generate_password_reset(user)
    user = user.id if user.respond_to?(:id)
    t = EARMMS::Crypto.generate_token
    now = Time.now

    self.new(user: user, use: 'password_reset', token: t, created: now, last_used: now, valid: true)
  end

  def self.generate_invite(opts = {})
    data = {}
    unless opts[:roles].nil?
      data["roles"] = opts[:roles]
    end

    t = EARMMS::Crypto.generate_token
    now = Time.now

    self.new(user: nil, use: 'invite', token: t, created: now, last_used: now, valid: true, extra_data: JSON.generate(data))
  end
end
