class EARMMS::User < Sequel::Model
  def self.by_token(token)
    token = EARMMS::Token.where(token: token).first
    return nil unless token
    return nil unless token.valid
    return nil unless token.use == 'session'

    user = self[token.user]
    token.last_used = Time.now
    token.save

    user
  end

  def get_roles
    EARMMS::UserHasRole.where(user: self.id).map(&:role).map(&:downcase)
  end

  def verify_password(password)
    return EARMMS::Crypto.password_verify(self.password_hash, password)
  end

  def password=(password)
    self.password_hash = EARMMS::Crypto.password_hash(password)
  end

  def invalidate_tokens(current_token)
    EARMMS::Token.where(user: self.id, use: 'session').each do |token|
      next if token.token == current_token
      token.valid = false
      token.save
    end
  end

  def send_password_reset!
    token = EARMMS::Token.generate_password_reset(self)
    token.save

    text = EARMMS.email_templates.password_reset(email, token.token)

    qm = EARMMS::EmailQueue.new(creation: Time.now)
    qm.save
    qm.queue_status = 'queued'
    qm.encrypt(:recipients, JSON.generate({"type" => "list", "list" => [self.email]}))
    qm.encrypt(:subject, EARMMS.email_subject("Password reset for #{EARMMS.site_name}"))
    qm.encrypt(:content, text.content_text)
    qm.encrypt(:content_html, text.content_html)
    qm.save
  end

  def delete_account!
    profile = EARMMS::Profile.for_user(self)
    text = EARMMS.email_templates.user_deletion(self, profile)

    to_addresses = []
    EARMMS::UserHasRole.where(role: "admin:alert_emails").map(&:user).each do |uid|
      u = EARMMS::User[uid]
      if u
        to_addresses << u.email
      end
    end

    if !to_addresses.empty?
      qm = EARMMS::EmailQueue.new(creation: Time.now)
      qm.save
      qm.queue_status = 'queued'
      qm.encrypt(:recipients, JSON.generate({"type" => "list", "list" => to_addresses}))
      qm.encrypt(:subject, EARMMS.email_subject("User deletion summary"))
      qm.encrypt(:content, text.content_text)
      qm.encrypt(:content_html, text.content_html)
      qm.save
    end

    self.convert_attendance_to_guest!

    EARMMS::ProfileFilter.where(profile: profile.id).each do |x|
      x.delete
    end

    EARMMS::Token.where(user: self.id).each do |x|
      x.delete
    end

    EARMMS::UserHasRole.where(user: self.id).each do |x|
      x.delete
    end

    EARMMS::BranchMeetingAttendanceRecordFilter.perform_filter(:profile, profile.id.to_s).each do |x|
      at = EARMMS::BranchMeetingAttendanceRecord[x.attendance_record]
      x.delete
      at.delete
    end

    EARMMS::MassEmail.where(user: self.id).each do |x|
      x.delete
    end

    EARMMS::ProfileChangelog.where(profile: profile.id).each do |x|
      x.delete
    end

    EARMMS::U2FRegistration.where(user: self.id).each do |x|
      x.delete
    end

    profile.delete
    self.delete
  end

  def convert_attendance_to_guest!
    profile = EARMMS::Profile.for_user(self)

    meetings = []
    EARMMS::BranchMeetingAttendanceRecordFilter.perform_filter(:profile, profile.id.to_s).each do |x|
      at = EARMMS::BranchMeetingAttendanceRecord[x.attendance_record]
      meetings << at.meeting if at.decrypt(:attendance_status) == "present"
      x.delete
      at.delete
    end

    # add this user to the guests for each meeting they were present at
    meetings.uniq.each do |meetingid|
      m = EARMMS::BranchMeeting[meetingid]
      next nil unless m

      guests = m.decrypt(:guests)
      guests = guests.lines.map(&:strip)
      guests << profile.decrypt(:full_name)
      guests = guests.join("\n")
      m.encrypt(:guests, guests)
      m.save
    end
  end
end

class EARMMS::UserFilter < Sequel::Model
  def self.clear_filters_for(user)
    user = user.id if user.respond_to? :id
    self.where(user: user).delete
  end

  def self.perform_filter(column, search)
    e = EARMMS::Crypto.index("User", column.to_s.downcase, search)
    return self.where(filter_label: column.to_s.downcase, filter_value: e)
  end
end
