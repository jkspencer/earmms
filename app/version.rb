module EARMMS
  VERSION = "1.9.0"

  def self.version
    return @cached_version if @cached_version

    gitrev = `sh -c 'command -v git >/dev/null && git rev-parse --short HEAD'`.strip
    if gitrev != ""
      @cached_version = "v#{VERSION} rev #{gitrev}"
    else
      @cached_version = "v#{VERSION}"
    end

    @cached_version
  end
end
