module EARMMS::Workers
  class Logger
    def initialize
      @log = []
    end

    def << message
      puts message
      @log << message
    end

    def to_s
      @log.join("\n")
    end
  end

  def self.perform_queue
    ds = EARMMS::WorkQueue.where(status: 'queued').all
    puts "Workers::perform_queue starting with #{ds.count} queued jobs"

    ds.each do |entry|
      run_at = Time.parse(entry.decrypt(:run_at))
      if run_at > Time.now
        puts "Workers::perform_queue skipping job #{entry.id} as it's not scheduled to run yet"
        next
      end

      entry.status = 'running'
      entry.encrypt(:started, Time.now)
      entry.save

      task = entry.decrypt(:task).to_sym
      data = entry.decrypt(:data)
      data = JSON.parse(data) if data

      logger = Logger.new

      begin
        EARMMS::Workers.const_get(task).perform(logger, entry.id, data)
      rescue => e
        logger << "Error running task #{task.inspect} (queue id #{entry.id}): #{e}"
        puts e.backtrace
      end

      entry.status = 'finished'
      entry.encrypt(:finished, Time.now)
      entry.encrypt(:log, logger.to_s)
      entry.save
    end

    # prune old entries
    t = EARMMS::ConfigModel.where(key: 'prune-old-work-queue-entries').first
    if t.respond_to?(:value) && t.value == 'yes'
      oneweekago = Time.parse((DateTime.now() - 7).to_s)
      count = 0
      EARMMS::WorkQueue.where(status: 'finished').each do |entry|
        finished = Time.parse(entry.decrypt(:finished))
        if finished < oneweekago
          count += 1
          entry.delete
        end
      end

      puts "Workers::perform_queue pruned #{count} old entries"
    end

    puts "Workers::perform_queue ending"
  end
end
