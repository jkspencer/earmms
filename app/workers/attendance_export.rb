require 'mail'

module EARMMS::Workers::AttendanceExport
  def self.queue(data)
    self.queue_at(Time.now, data)
  end

  def self.queue_at(time, data)
    w = EARMMS::WorkQueue.new(:status => 'queued')
    w.save
    w.encrypt(:task, "AttendanceExport")
    w.encrypt(:data, JSON.dump(data))
    w.encrypt(:created, Time.now)
    w.encrypt(:run_at, time)
    w.save

    w
  end

  def self.perform(log, entryid, data)
    start = Time.now
    type = data["type"].to_sym

    log << "AttendanceExport: starting at #{start.iso8601} for type #{type.inspect}"

    requesting_user = data["requesting_user"]
    unless requesting_user == 'stdout'
      requesting_user = EARMMS::User[requesting_user.to_i] if requesting_user
      unless requesting_user
        log << "AttendanceExport: invalid requesting user, dying at #{Time.now.iso8601}"
        return
      end
    end

    export_message = ""
    attendance = {}

    case type
    when :user
      user = data["user"].to_i
      user = EARMMS::User[user] if user
      unless user
        log << "AttendanceExport: invalid user, dying at #{Time.now.iso8601}"
        return
      end

      profile = EARMMS::Profile.for_user(user)
      name = profile.decrypt(:full_name)
      name = name.force_encoding("UTF-8") if name.respond_to?(:force_encoding)
      email = user.email

      export_message = "This export was for the attendance records of user #{name} (user ID #{user.id.to_s}).".force_encoding("UTF-8")

      ds = EARMMS::BranchMeetingAttendanceRecordFilter.perform_filter(:profile, profile.id.to_s)
      ds.each do |entry|
        at = EARMMS::BranchMeetingAttendanceRecord[entry.attendance_record]
        next nil unless at

        meeting = EARMMS::BranchMeeting[at.meeting]
        next nil unless meeting

        branch = EARMMS::Branch[meeting.branch]
        next nil unless branch
        branch_name = branch.decrypt(:name)
        branch_name = branch_name.force_encoding("UTF-8") if branch_name.respond_to?(:force_encoding)

        status = at.decrypt(:attendance_status)
        case status
        when "present"
          status = "Present"
        when "apologies"
          status = "Apologies"
        else
          status = "Unknown"
        end

        attendance[user.id] ||= []
        attendance[user.id] << {
          :user => user.id,
          :name => name,
          :email => email,
          :meetingid => meeting.id,
          :branch => branch,
          :branch_name => branch_name,
          :datetime => Time.parse(meeting.decrypt(:datetime)),
          :status => status
        }
      end

    when :branch
      branch = data["branch"].to_i
      branch = EARMMS::Branch[branch] if branch
      unless branch
        log << "AttendanceExport: invalid branch, dying at #{Time.now.iso8601}"
        return
      end

      branch_name = branch.decrypt(:name)
      branch_name = branch_name.force_encoding("UTF-8") if branch_name.respond_to?(:force_encoding)
      export_message = "This export was for the attendance records of branch #{branch_name} (branch ID #{branch.id.to_s}).".force_encoding("UTF-8")

      EARMMS::BranchMeeting.where(branch: branch.id).each do |meeting|
        meeting_ts = Time.parse(meeting.decrypt(:datetime))

        EARMMS::BranchMeetingAttendanceRecord.where(meeting: meeting.id).each do |at|
          p = EARMMS::Profile[at.decrypt(:profile).to_i]
          next nil unless p
          u = EARMMS::User[p.user]
          next nil unless u

          name = p.decrypt(:full_name)
          name = name.force_encoding("UTF-8") if name.respond_to?(:force_encoding)

          status = at.decrypt(:attendance_status)
          case status
          when "present"
            status = "Present"
          when "apologies"
            status = "Apologies"
          else
            status = "Unknown"
          end

          attendance[u.id] ||= []
          attendance[u.id] << {
            :user => u.id,
            :name => name,
            :email => u.email,
            :meetingid => meeting.id,
            :branch => branch,
            :branch_name => branch_name,
            :datetime => meeting_ts,
            :status => status
          }
        end
      end

    when :meeting
      meeting = data["meeting"].to_i
      meeting = EARMMS::BranchMeeting[meeting] if meeting
      unless meeting
        log << "AttendanceExport: invalid meeting, dying at #{Time.now.iso8601}"
        return
      end

      branch = EARMMS::Branch[meeting.branch]
      unless branch
        log << "AttendanceExport: meeting has invalid branch, dying at #{Time.now.iso8601}"
        return
      end

      meeting_ts = Time.parse(meeting.decrypt(:datetime))
      branch_name = branch.decrypt(:name)
      branch_name = branch_name.force_encoding("UTF-8") if branch_name.respond_to?(:force_encoding)
      export_message = "This export was for the attendance records for the meeting at #{meeting_ts.strftime("%A, %-d %B %Y at %I:%M %P")} (meeting ID #{meeting.id.to_s}) for branch #{branch_name} (branch ID #{branch.id.to_s}).".force_encoding("UTF-8")

      EARMMS::BranchMeetingAttendanceRecord.where(meeting: meeting.id).each do |at|
        p = EARMMS::Profile[at.decrypt(:profile).to_i]
        next nil unless p
        u = EARMMS::User[p.user]
        next nil unless u

        name = p.decrypt(:full_name)
        name = name.force_encoding("UTF-8") if name.respond_to?(:force_encoding)

        status = at.decrypt(:attendance_status)
        case status
        when "present"
          status = "Present"
        when "apologies"
          status = "Apologies"
        else
          status = "Unknown"
        end

        attendance[u.id] ||= []
        attendance[u.id] << {
          :user => u.id,
          :name => name,
          :email => u.email,
          :meetingid => meeting.id,
          :branch => branch,
          :branch_name => branch_name,
          :datetime => meeting_ts,
          :status => status
        }
      end

    else
      return
    end

    meetings = []
    attendance.keys.each do |u|
      attendance[u].each do |m|
        unless meetings.map{|x|x[:meetingid]}.include? m[:meetingid]
          meetings << {
            :meetingid => m[:meetingid],
            :branch => m[:branch],
            :branch_name => m[:branch_name],
            :datetime => m[:datetime],
            :header => "#{m[:branch_name]} #{m[:datetime].iso8601}"
          }
        end
      end
    end
    meetings.sort!{|a, b| a[:meetingid] <=> b[:meetingid]}

    attendance = attendance.keys.map do |u|
      s = attendance[u]
      included = s.map{|x| x[:meetingid]}
      d = s.first
      meetings.each do |m|
        unless included.include? m[:meetingid]
          s << {
            :user => d[:user],
            :name => d[:name],
            :email => d[:email],
            :meetingid => m[:meetingid],
            :branch => m[:branch],
            :branch_name => m[:branch_name],
            :datetime => m[:datetime],
            :status => "No data",
          }
        end
      end

      [u, s]
    end.to_h

    log << "AttendanceExport: collected #{attendance.length} users"

    csv = CSV.generate do |c|
      header = ["Name", "Email address"]
      meetings.each do |m|
        header << m[:header]
      end
      c << header

      attendance.keys.each do |uid|
        row = [attendance[uid].first[:name], attendance[uid].first[:email]]
        attendance[uid].sort{|a, b| a[:meetingid] <=> b[:meetingid]}.each do |b|
          row << b[:status]
        end

        c << row
      end
    end

    content = []
    content << "Hello,"
    content << ""
    content << "The attendance record export you requested is attached."
    content << ""
    content << export_message

    if requesting_user == 'stdout'
      log << 80.times.map{"="}.join
      log << export_message
      log << 80.times.map{"="}.join
      log << csv.to_s
      log << 80.times.map{"="}.join
    else
      qm = EARMMS::EmailQueue.new(creation: Time.now)
      qm.save
      qm.queue_status = 'queued'
      qm.encrypt(:recipients, JSON.generate({"type" => "list", "list" => [requesting_user.email]}))
      qm.encrypt(:subject, EARMMS.email_subject("Attendance export"))
      qm.encrypt(:content, content.join("\n"))
      qm.encrypt(:attachments, JSON.generate([{"filename" => "attendance_export_#{start.strftime("%Y-%m-%d_%H-%M-%S")}.csv", "content" => csv.to_s}]))
      qm.save

      log << "AttendanceExport: email queued with id #{qm.id}"
    end

    finish = Time.now
    log << "AttendanceExport: done at #{finish.iso8601}, run time #{((finish - start) * 1000).round(3)}ms"
  end
end
