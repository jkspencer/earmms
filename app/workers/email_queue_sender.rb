require 'mail'

module EARMMS::Workers::EmailQueueSender
  RECIPIENTS_CHUNK_LENGTH = 25

  def self.queue(data)
    self.queue_at(Time.now, data)
  end

  def self.queue_at(time, data)
    w = EARMMS::WorkQueue.new(:status => 'queued')
    w.save
    w.encrypt(:task, "EmailQueueSender")
    w.encrypt(:data, JSON.dump(data))
    w.encrypt(:created, Time.now)
    w.encrypt(:run_at, time)
    w.save

    w
  end

  def self.perform(log, workentryid, data)
    start = Time.now
    log << "EmailQueueSender: starting at #{start.iso8601}"

    queued_ids = EARMMS.database[EARMMS::EmailQueue.table_name].where(:queue_status => 'queued').select{id}.map{|x| x[:id]}
    log << "EmailQueueSender: #{queued_ids.length} queued emails"

    unless queued_ids.empty?
      queued_ids.each do |qmid|
        log << "----- performing for queue id #{qmid} -----"
        begin
          self.perform_for_id(log, workentryid, qmid)
        rescue => e
          log << "EmailQueueSender: email id #{qmid} errored: #{e}"
          log << e.backtrace
        end
      end

      log << "----- finished performing for queued -----"
    end

    log << "EmailQueueSender: queueing next run"
    self.queue({})

    finish = Time.now
    log << "EmailQueueSender: finished at #{start.iso8601} (run time #{((finish - start) * 1000).round(3)}ms)"
  end

  def self.perform_for_id(log, workentryid, emailid)
    start = Time.now
    log << "EmailQueueSender[#{emailid}]: starting at #{start.iso8601}"

    qm = EARMMS::EmailQueue[emailid]
    unless qm
      log << "EmailQueueSender[#{emailid}]: invalemailid queue ID, dying at #{Time.now.iso8601}"
      return
    end

    qm.queue_status = "sending"
    qm.save

    recipients = qm.get_recipient_list
    if recipients.empty?
      log << "EmailQueueSender[#{emailid}]: no recipients, dying at #{Time.now.iso8601}"
      return
    end

    log << "EmailQueueSender[#{emailid}]: email has #{recipients.length} recipients"

    # retrieve email data
    email_subject = qm.decrypt(:subject)
    email_content_text = qm.decrypt(:content)
    email_content_html = qm.decrypt(:content_html)
    if email_content_html.is_a?(String)
      email_content_html = nil if email_content_html.strip == ""
    else
      email_content_html = nil
    end

    # get attachments
    email_attachments = qm.decrypt(:attachments)
    if email_attachments.is_a?(String) && email_attachments.strip != ""
      email_attachments = JSON.parse(email_attachments)
    else
      email_attachments = []
    end

    # split recipient list into chunks
    chunked_recipients = []
    current_chunk = []
    recipients.each do |r|
      current_chunk << r
      if current_chunk.length >= RECIPIENTS_CHUNK_LENGTH
        chunked_recipients << current_chunk
        current_chunk = []
      end
    end
    unless current_chunk.empty? # add last chunk
      chunked_recipients << current_chunk
      current_chunk = []
    end

    log << "EmailQueueSender[#{emailid}]: split recipients into #{chunked_recipients.length} chunks"

    has_errored = false

    # send email to each chunk
    chunked_recipients.each_index do |chunk_emailid|
      chunk = chunked_recipients[chunk_emailid]

      begin
        m = Mail.new do
          from EARMMS.email_from_address

          if EARMMS.email_send_to_self
            to EARMMS.email_from_address
          end

          bcc chunk

          subject email_subject

          text_part do
            body email_content_text
          end

          if email_content_html
            html_part do
              content_type 'text/html; charset=UTF-8'
              body email_content_html
            end
          end

          if !email_attachments.empty?
            email_attachments.each do |m|
              add_file :filename => m["filename"], :content => m["content"]
            end
          end
        end

        m.header["Precedence"] = "bulk"
        m.header["Return-Path"] = "<>"
        m.header["Auto-Submitted"] = "auto-generated"

        log << "EmailQueueSender[#{emailid}]: delivering to chunk #{chunk_emailid}"
        m.deliver!

      rescue => e
        log << "EmailQueueSender[#{emailid}]: error occured in chunk #{chunk_emailid}: #{e}"
        log << e.traceback if e.respond_to?(:traceback)
        has_errored = true
      end
    end

    if has_errored
      log << "EmailQueueSender[#{emailid}]: an error has occurred, marking queue item as error and alerting"
      qm.queue_status = 'error'
      qm.save

      alert = EARMMS::Alert.new(section: "system", actioned: false, created: Time.now)
      alert.save

      alert.encrypt(:userid, 0)
      alert.encrypt(:type, "email_queue_error")
      alert.encrypt(:data, "Queued email #{emailid} failed to send (as part of job id #{workentryid})")
      alert.save

      #alert.send_email!

    else
      log << "EmailQueueSender[#{emailid}]: sent successfully, marking as complete"
      qm.queue_status = 'complete'
      qm.save
    end

    finish = Time.now
    log << "EmailQueueSender[#{emailid}]: finished at #{start.iso8601} (run time #{((finish - start) * 1000).round(3)}ms)"
  end
end
