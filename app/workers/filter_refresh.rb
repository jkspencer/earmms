module EARMMS::Workers::FilterRefresh
  def self.queue(data)
    self.queue_at(Time.now, data)
  end

  def self.queue_at(time, data)
    w = EARMMS::WorkQueue.new(:status => 'queued')
    w.save
    w.encrypt(:task, "FilterRefresh")
    w.encrypt(:data, JSON.dump(data))
    w.encrypt(:created, Time.now)
    w.encrypt(:run_at, time)
    w.save

    w
  end

  def self.perform(log, entryid, data)
    start = Time.now
    log << "FilterRefresh: starting at #{start.iso8601}"

    # enable maintenance mode
    log << "FilterRefresh: enabling maintenance mode"
    maint_cfg = EARMMS::ConfigModel.where(:key => 'maintenance').first
    unless maint_cfg
      maint_cfg = EARMMS::ConfigModel.new(:key => 'maintenance', :type => 'boolean', :value => 'no')
    end
    maint_enabled = maint_cfg.value == 'yes'
    maint_cfg.value = 'yes'
    maint_cfg.save

    custom_fields = EARMMS.custom_user_fields.map do |f|
      {
        :name => f[:name].to_s.force_encoding("UTF-8"),
        :friendly => f[:friendly].force_encoding("UTF-8"),
        :type => (f[:type].to_s || "text").force_encoding("UTF-8"),
        :required => f[:required] || false,
        :default => f[:default] || nil,
        :select_options => f[:type] == 'select' ? f[:select_options] : nil,
        :display_only_if => f[:display_only_if] || nil,
        :display_only_if_membership_status => f[:display_only_if_membership_status] || nil,
      }
    end

    log << "FilterRefresh: starting refresh of profile filters"
    profile_count = {:started => 0, :ok => 0, :failure => 0}
    EARMMS::Profile.each do |p|
      profile_count[:started] += 1
      log << "FilterRefresh: started profile #{profile_count[:started]}" if (profile_count[:started] % 10) == 0

      begin
        # update custom fields
        p_custom_fields = {}
        p_custom_json = p.decrypt(:custom_fields)
        unless p_custom_json.nil? || p_custom_json.empty?
          p_custom_fields = JSON.parse(p_custom_json)
        end

        # set custom fields to defaults if they don't have a value
        custom_fields.each do |cf|
          v = p_custom_fields[cf[:name]]
          if v.nil? || v.empty?
            if cf[:default]
              v = cf[:default]
            end
          end

          p_custom_fields[cf[:name]] = v
        end

        # save custom fields
        p.encrypt(:custom_fields, JSON.generate(p_custom_fields))

        # save and refresh filters
        p.save
        EARMMS::ProfileFilter.clear_filters_for(p)
        EARMMS::ProfileFilter.create_filters_for(p)

        profile_count[:ok] += 1
      rescue => e
        log << "FilterRefresh: profile #{p.id} failed: #{e.inspect}"
        profile_count[:failure] += 1
      end
    end

    log << "FilterRefresh: updated #{profile_count[:ok]} profiles, #{profile_count[:failure]} failed"

    # disable maintenance mode if not enabled at worker start
    if maint_enabled
      log << "FilterRefresh: Not disabling maintenance mode as it was enabled when worker started"
    else
      log << "FilterRefresh: Disabling maintenance mode"
      maint_cfg.value = 'no'
      maint_cfg.save
    end

    finish = Time.now
    log << "FilterRefresh: finished at #{start.iso8601} (run time #{((finish - start) * 1000).round(3)}ms)"
  end
end
