module EARMMS::Workers::GroupEnforceRestrictions
  class << self
    include EARMMS::GroupHelpers
  end

  def self.queue(data)
    self.queue_at(Time.now, data)
  end

  def self.queue_at(time, data)
    w = EARMMS::WorkQueue.new(:status => 'queued')
    w.save
    w.encrypt(:task, "GroupEnforceRestrictions")
    w.encrypt(:data, JSON.dump(data))
    w.encrypt(:created, Time.now)
    w.encrypt(:run_at, time)
    w.save

    w
  end

  def self.perform(log, entryid, data)
    start = Time.now
    log << "GroupEnforceRestrictions: starting at #{start.iso8601}"

    # enable maintenance mode
    log << "GroupEnforceRestrictions: enabling maintenance mode"
    maint_cfg = EARMMS::ConfigModel.where(:key => 'maintenance').first
    unless maint_cfg
      maint_cfg = EARMMS::ConfigModel.new(:key => 'maintenance', :type => 'boolean', :value => 'no')
    end
    maint_enabled = maint_cfg.value == 'yes'
    maint_cfg.value = 'yes'
    maint_cfg.save

    # collect groups with flag enabled
    groups = EARMMS::Group.where(:restrict_to_member => true).all
    if groups.count.zero?
      finish = Time.now
      log << "GroupEnforceRestrictions: no groups with restriction flag set, dying"
      log << "GroupEnforceRestrictions: finished at #{finish.iso8601} (run time #{((finish - start) * 1000).round(3)}ms)"
      return
    end

    log << "GroupEnforceRestrictions: #{groups.count} groups with restriction flag set"

    # enforce each group
    groups.each do |g|
      members = get_group_members(g)
      to_remove = members.map{|k, v| v}.filter{|v| v[:membership_status] != "member"}

      log << "GroupEnforceRestrictions: group #{g.id}: #{members.count} members total, #{to_remove.count} non-org-members to remove"
      next if to_remove.count.zero?

      counts = {:started => 0, :ok => 0, :err => 0}
      to_remove.each do |m|
        counts[:started] += 1
        if counts[:started] % 10 == 0
          log << "GroupEnforceRestrictions: group #{g.id}: processing... #{counts[:started]} of #{to_remove.count}"
        end

        begin
          cm = m[:group_member]
          EARMMS::GroupMemberFilter.clear_filters_for(cm)
          cm.delete

          counts[:ok] += 1
        rescue => e
          log << "GroupEnforceRestrictions: group #{g.id}: failed to delete (uid:#{m[:user].id} gmid:#{m[:group_member_id]}): #{e.inspect}"
          counts[:err] += 1
        end
      end

      log << "GroupEnforceRestrictions: group #{g.id}: removed: #{counts[:ok]}, errored: #{counts[:err]}"
    end

    # disable maintenance mode if not enabled at worker start
    if maint_enabled
      log << "GroupEnforceRestrictions: Not disabling maintenance mode as it was enabled when worker started"
    else
      log << "GroupEnforceRestrictions: Disabling maintenance mode"
      maint_cfg.value = 'no'
      maint_cfg.save
    end

    finish = Time.now
    log << "GroupEnforceRestrictions: finished at #{finish.iso8601} (run time #{((finish - start) * 1000).round(3)}ms)"
  end
end
