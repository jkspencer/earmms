module EARMMS::Workers::MassAction
  class << self
    include EARMMS::MembershipAdminUserHelpers
  end

  def self.queue(data)
    self.queue_at(Time.now, data)
  end

  def self.queue_at(time, data)
    w = EARMMS::WorkQueue.new(:status => 'queued')
    w.save
    w.encrypt(:task, "MassAction")
    w.encrypt(:data, JSON.dump(data))
    w.encrypt(:created, Time.now)
    w.encrypt(:run_at, time)
    w.save

    w
  end

  def self.perform(log, entryid, data)
    start = Time.now
    log << "MassAction: starting at #{start.iso8601}"

    action = data["action"]&.split(":")
    if action.nil? || action.empty?
      log << "MassAction: invalid action, dying"
      return
    end

    unless %w[membership_status].include?(action.first)
      log << "MassAction: invalid action, dying"
      return
    end

    log << "MassAction: action #{action.inspect}"

    searchtype = data["searchtype"]
    query = data["query"]
    log << "MassAction: query type #{searchtype.inspect}, query #{query.inspect}"

    results = do_search(searchtype, query)
    if results
      log << "MassAction: search returned #{results.length} users, collating data"
    else
      log << "MassAction: search returned error, dying"
      return
    end

    # enable maintenance mode
    log << "MassAction: enabling maintenance mode"
    maint_cfg = EARMMS::ConfigModel.where(:key => 'maintenance').first
    unless maint_cfg
      maint_cfg = EARMMS::ConfigModel.new(:key => 'maintenance', :type => 'boolean', :value => 'no')
    end
    maint_enabled = maint_cfg.value == 'yes'
    maint_cfg.value = 'yes'
    maint_cfg.save

    log << "MassAction: performing action"
    profile_count = {:started => 0, :ok => 0, :failure => 0}
    results.each do |res|
      profile_count[:started] += 1
      log << "MassAction: started profile #{profile_count[:started]}" if (profile_count[:started] % 10) == 0

      p = res[:profile]
      begin
        # do action
        case action.first
        when "membership_status"
          p.encrypt(:membership_status, action.last)
        end

        # save and refresh filters
        p.save
        EARMMS::ProfileFilter.clear_filters_for(p)
        EARMMS::ProfileFilter.create_filters_for(p)

        profile_count[:ok] += 1
      rescue => e
        log << "MassAction: profile #{p.id} failed: #{e.inspect}"
        profile_count[:failure] += 1
      end
    end

    log << "MassAction: updated #{profile_count[:ok]} profiles, #{profile_count[:failure]} failed"

    # disable maintenance mode if not enabled at worker start
    if maint_enabled
      log << "MassAction: Not disabling maintenance mode as it was enabled when worker started"
    else
      log << "MassAction: Disabling maintenance mode"
      maint_cfg.value = 'no'
      maint_cfg.save
    end

    finish = Time.now
    log << "MassAction: finished at #{start.iso8601} (run time #{((finish - start) * 1000).round(3)}ms)"
  end
end
