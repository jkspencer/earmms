require 'mail'

module EARMMS::Workers::SearchExport
  class << self
    include EARMMS::MembershipAdminUserHelpers
  end

  def self.queue(data)
    self.queue_at(Time.now, data)
  end

  def self.queue_at(time, data)
    w = EARMMS::WorkQueue.new(:status => 'queued')
    w.save
    w.encrypt(:task, "SearchExport")
    w.encrypt(:data, JSON.dump(data))
    w.encrypt(:created, Time.now)
    w.encrypt(:run_at, time)
    w.save

    w
  end

  def self.perform(log, entryid, data)
    start = Time.now
    log << "SearchExport: starting at #{start.iso8601}"

    requesting_user = data["requesting_user"]
    unless requesting_user == 'stdout'
      requesting_user = EARMMS::User[requesting_user.to_i] if requesting_user
      unless requesting_user
        log << "AttendanceExport: invalid requesting user, dying at #{Time.now.iso8601}"
        return
      end
    end

    exporttype = data["exporttype"].to_sym
    searchtype = data["searchtype"]
    query = data["query"]
    log << "SearchExport: export type #{exporttype.inspect}, query type #{searchtype.inspect}, query #{query.inspect}"

    results = do_search(searchtype, query)
    if results
      log << "SearchExport: search returned #{results.length} users, collating data"
    else
      log << "SearchExport: search returned error, dying"
      return
    end

    output = results.map do |res|
      user = res[:user]
      profile = res[:profile]

      data = []
      data << ["User IDs", "uid #{user.id} pid #{profile.id}"]
      data << ["Email address", user.email]
      data << ["Full name", [profile.decrypt(:full_name)].map{|x| x.respond_to?(:force_encoding) ? x.force_encoding("UTF-8") : x}.first]

      t = profile.decrypt(:birthday)
      t = t.force_encoding("UTF-8") if t.respond_to?(:force_encoding)
      t = Time.parse(t).strftime("%Y-%m-%d") if t && t != ''
      t = "unknown" unless t
      data << ["Birthday", t]

      address = profile.decrypt(:address)
      address = address.force_encoding("UTF-8") if address.respond_to?(:force_encoding)
      address = address.split("\n").join(" / ") if address
      address = "unknown" unless address
      data << ["Address", address]

      data << ["Home phone", [profile.decrypt(:phone_home) || 'unknown'].map{|x| x.respond_to?(:force_encoding) ? x.force_encoding("UTF-8") : x}.first]
      data << ["Mobile phone", [profile.decrypt(:phone_mobile) || 'unknown'].map{|x| x.respond_to?(:force_encoding) ? x.force_encoding("UTF-8") : x}.first]
      data << ["Prisoner number", [profile.decrypt(:prisoner_number) || 'unknown'].map{|x| x.respond_to?(:force_encoding) ? x.force_encoding("UTF-8") : x}.first]
      data << ["Membership status", [profile.decrypt(:membership_status) || 'unknown'].map{|x| x.respond_to?(:force_encoding) ? x.force_encoding("UTF-8") : x}.first]

      # Branch
      branch = profile.decrypt(:branch).strip.to_i
      branch = EARMMS::Branch[branch]
      branch = "#{branch.decrypt(:name).force_encoding("UTF-8")} (id #{branch.id})".force_encoding("UTF-8") if branch
      branch = "unknown" unless branch
      data << ["Branch", branch]

      # Custom fields
      profile_custom_json = profile.decrypt(:custom_fields)
      if profile_custom_json && profile_custom_json != ""
        profile_custom_json = JSON.parse(profile_custom_json)
      else
        profile_custom_json = {}
      end
      custom_profile_fields.each do |key, field|
        data << [field[:friendly], profile_custom_json[key] || 'unknown']
      end

      data
    end

    output_text = ""
    if exporttype == :text
      output_text = output.map do |data|
        sep = 80.times.map{"-"}.join

        text = data.map do |m|
          "#{m[0]}: #{m[1]}"
        end.join("\n")

        "#{sep}\n#{text}\n#{sep}"
      end.join("\n").force_encoding("UTF-8")

    elsif exporttype == :csv
      headers = []
      output.each do |data|
        data.each do |m|
          k = m[0]
          headers << k unless headers.include?(k)
        end
      end

      csv = CSV.generate do |c|
        c << headers
        output.each do |data|
          row = []
          data.each do |m|
            row << m[1]
          end

          c << row
        end
      end

      output_text = csv.to_s
    end

    fileext = "txt"
    fileext = "csv" if exporttype == :csv
    attachments = [
      {
        "filename" => "search_export_#{start.strftime("%Y-%m-%d_%H-%M-%S")}.#{fileext}",
        "content" => output_text,
      }
    ]

    email_content = [
      "Hello,",
      "",
      "The search export you requested is attached.",
      "",
      "The search was for type #{searchtype.inspect}, query #{query.inspect}.",
      "This search returned #{results.length} users.",
    ]

    if requesting_user == 'stdout'
      log << 80.times.map{"="}.join
      log << email_content.join("\n")
      log << 80.times.map{"="}.join
      log << output_text
      log << 80.times.map{"="}.join

    else
      qm = EARMMS::EmailQueue.new(creation: Time.now)
      qm.save
      qm.queue_status = 'queued'
      qm.encrypt(:recipients, JSON.generate({"type" => "list", "list" => [requesting_user.email]}))
      qm.encrypt(:subject, EARMMS.email_subject("Search export"))
      qm.encrypt(:content, email_content.join("\n"))
      qm.encrypt(:attachments, JSON.generate(attachments))
      qm.save

      log << "SearchExport: email queued with id #{qm.id}"
    end

    finish = Time.now
    log << "SearchExport: done at #{finish.iso8601}, run time #{((finish - start) * 1000).round(3)}ms"
  end
end
