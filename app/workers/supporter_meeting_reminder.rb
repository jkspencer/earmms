require 'mail'

module EARMMS::Workers::SupporterMeetingReminder
  def self.queue(data)
    self.queue_at(Time.now, data)
  end

  def self.queue_at(time, data)
    w = EARMMS::WorkQueue.new(:status => 'queued')
    w.save
    w.encrypt(:task, "SupporterMeetingReminder")
    w.encrypt(:data, JSON.dump(data))
    w.encrypt(:created, Time.now)
    w.encrypt(:run_at, time)
    w.save

    w
  end

  def self.perform(log, entryid, data)
    start = Time.now
    log << "SupporterMeetingReminder: starting at #{start.iso8601}"

    # Get list of meetings in the next week
    yesterday = start - 1
    yesterday = DateTime.civil(yesterday.year, yesterday.month, yesterday.day, 23, 59, 59)
    oneweek = yesterday + 7
    meetings = EARMMS::Branch.all.map do |branch|
      branch_name = branch.decrypt(:name)

      upcoming = EARMMS::BranchMeeting.where(branch: branch.id).order(Sequel.desc(:id)).limit(5).map do |m|
        dt = DateTime.parse(m.decrypt(:datetime))
        next nil unless dt.between?(yesterday, oneweek)

        url = Addressable::URI.parse(EARMMS.base_url)
        url += "/dashboard/meetings/#{branch.id}/#{m.id}"

        {
          :id => m.id,
          :branch => branch.id,
          :branch_name => branch_name,
          :datetime => dt,
          :url => url
        }
      end.compact

      if upcoming.empty?
        next nil
      end

      upcoming.sort!{|a, b| a[:datetime] <=> b[:datetime]}
      upcoming.first
    end.compact

    log << "SupporterMeetingReminder: collected #{meetings.length} meetings"
    unless meetings.empty?
      # Render template
      log << "SupporterMeetingReminder: generating email"
      email_data = EARMMS.email_templates.supporter_meeting_reminder(meetings)

      qm = EARMMS::EmailQueue.new(creation: Time.now)
      qm.save
      qm.queue_status = 'queued'
      qm.encrypt(:recipients, JSON.generate({"type" => "mass_email_target", "target" => "status:supporter"}))
      qm.encrypt(:subject, EARMMS.email_subject("Upcoming branch meetings"))
      qm.encrypt(:content, email_data.content_text)
      qm.encrypt(:content_html, email_data.content_html)
      qm.save

      log << "SupporterMeetingReminder: queued email with id #{qm.id}"
    end

    # Queue the next reminder job
    cfg = EARMMS::ConfigModel.where(key: 'meeting-reminder-secs').first
    if cfg
      run_at = start + cfg.value.to_i
      log << "SupporterMeetingReminder: queueing next run for #{run_at.iso8601} (in #{run_at - Time.now} seconds)"
      self.queue_at(run_at, data)
    else
      log << "SupporterMeetingReminder: not queueing next run, 'meeting-reminder-secs' config entry not found"
    end

    finish = Time.now
    log << "SupporterMeetingReminder: finished at #{start.iso8601} (run time #{((finish - start) * 1000).round(3)}ms)"
  end
end
