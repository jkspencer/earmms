export const enableFieldDisplayOnlyIf = () => {
  let els = document.querySelectorAll("[data-display-only-if]")
  Array.from(els).forEach((el) => {
    let label = document.querySelector("label[for = '" + el.id + "']")

    Array.from(JSON.parse(el.getAttribute('data-display-only-if'))).forEach((data) => {
      let target_els = document.querySelectorAll("[name = '" + data["field"] + "']")
      let values = data["values"]

      Array.from(target_els).forEach((target_el) => {
        let doChange = () => {
          if (values.indexOf(target_el.value) != -1) {
            el.style.display = label.style.display = 'block'
          } else {
            el.style.display = label.style.display = 'none'
          }
        }

        target_el.addEventListener('change', () => doChange())
        doChange()
      })
    })
  })
}

enableFieldDisplayOnlyIf()
