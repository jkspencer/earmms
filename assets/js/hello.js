const message = (
  "EARMMS is open source! " +
  "If you want to get involved, head over to our GitHub at " +
  "https://github.com/peopleagainstprisons/earmms " +
  ":)"
)

console.log(message)
