const enableLoginSwitch = () => {
  let totpForm = document.querySelector('#totp-form')
  let u2fForm = document.querySelector('#u2f-form')

  document.querySelector('#switch-to-totp').addEventListener('click', () => {
    totpForm.style.display = 'block'
    u2fForm.style.display = 'none'
  })

  document.querySelector('#switch-to-u2f').addEventListener('click', () => {
    totpForm.style.display = 'none'
    u2fForm.style.display = 'block'
  })
}

export const enableIfPresent = () => {
  if (document.querySelector('#login-two-factor')) {
    enableLoginSwitch()
  }
}

enableIfPresent()
