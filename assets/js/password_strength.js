const getElements = (base) => {
  let statusEl = document.getElementById(base.getAttribute('data-strength'))
  let textEl = statusEl.querySelector('.zxcvbn-score-text')
  let barEl = statusEl.querySelector('.zxcvbn-score-bar')
  let warningEl = statusEl.querySelector('.zxcvbn-warnings')
  let suggestEl = statusEl.querySelector('.zxcvbn-suggestions')

  return {
    statusEl,
    textEl,
    barEl,
    warningEl,
    suggestEl,
  }
}

const getStrength = (score) => {
  if (score == 4) return 'excellent'
  if (score > 2) return 'good'
  if (score > 1) return 'mediocre'
  return 'bad'
}

const onInput = (base) => {
  let result = window.zxcvbn(base.value)
  let els = getElements(base)

  els.statusEl.hidden = false
  els.textEl.innerHTML = 'Strength: ' + getStrength(result.score)
  els.barEl.setAttribute('data-zxcvbn-score', result.score)

  if (result.feedback.warning) {
    els.warningEl.hidden = false
    els.warningEl.innerHTML = result.feedback.warning
  } else {
    els.warningEl.hidden = true
  }

  if (result.feedback.suggestions.length > 0) {
    els.suggestEl.hidden = false

    let out = "Suggestions: <ul>"
    Array.from(result.feedback.suggestions).forEach((b) => {
      out += "<li>" + b + "</li>"
    })
    out += "</ul>"

    els.suggestEl.innerHTML = out
  } else {
    els.suggestEl.hidden = true
    els.suggestEl.innerHTML = ""
  }
}

export const enableCheck = () => {
  Array.from(document.querySelectorAll('input')).forEach((el) => {
    if (el.getAttribute('data-strength') !== null) {
      let els = getElements(el)
      els.statusEl.classList.add('loaded')
      els.statusEl.hidden = (el.value == "")
      els.warningEl.hidden = true
      els.suggestEl.hidden = true

      el.addEventListener('input', (e) => onInput(e.target))
    }
  })
}

enableCheck()
