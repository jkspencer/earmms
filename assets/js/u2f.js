import { ensureSupport, register, sign } from "u2f-api"

//let messages = JSON.parse(document.querySelector('#u2f-messages').getAttribute('data-errors'))
let messages = {
  insertKey: "Please insert your security key, and press the button (if it has one)",
  errorMap: {
    0: "Successfully authenticated.",
    1: "An error occurred: unknown error",
    2: "An error occurred: bad request",
    3: "An error occurred: this key is not supported",
    4: "An error occurred: this key is ineligible",
    5: "Authentication timed out.",
  },
}

const getData = () => {
  let el = document.querySelector('#u2f-data')

  return {
    appId: JSON.parse(el.getAttribute('data-appid')),
    registrationRequests: JSON.parse(el.getAttribute('data-registration-requests')),
    signRequests: JSON.parse(el.getAttribute('data-sign-requests')),
  }
}

const getElements = () => {
  let formEl = document.querySelector('#u2f-form')
  let spinnerEl = formEl.querySelectorAll('.spinner')[0]
  let statusEl = formEl.querySelectorAll('.status')[0]

  return {
    formEl,
    spinnerEl,
    statusEl,
  }
}

export const doSign = () => {
  let data = getData()
  let { formEl, spinnerEl, statusEl } = getElements()

  ensureSupport().then(() => {
    spinnerEl.style.display = 'block'
    statusEl.innerHTML = messages.insertKey

    return sign(data.signRequests)
  }).then((resp) => {
    return JSON.stringify(resp)
  }).then((resp) => {
    let el = formEl.querySelectorAll('[name=response]')[0]
    el.value = resp
    formEl.submit()
    return true
  }).catch((e) => {
    spinnerEl.style.display = 'none'
    statusEl.innerHTML = messages.errorMap[e.metaData.code]
    return false
  })
}

export const doRegister = () => {
  let data = getData()
  let { formEl, spinnerEl, statusEl } = getElements()

  ensureSupport().then(() => {
    spinnerEl.style.display = 'block'
    statusEl.innerHTML = messages.insertKey

    return register(data.registrationRequests, data.signRequests)
  }).then((resp) => {
    return JSON.stringify(resp)
  }).then((resp) => {
    let el = formEl.querySelectorAll('[name=response]')[0]
    el.value = resp
    formEl.submit()
    return true
  }).catch((e) => {
    spinnerEl.style.display = 'none'
    statusEl.innerHTML = messages.errorMap[e.metaData.code]
    return false
  })
}

export const doU2F = () => {
  let el = document.querySelector('#u2f-form')
  if (el) {
    let type = el.getAttribute('data-u2f-type')
    if (type === 'register') {
      doRegister()
    } else if (type === 'sign') {
      doSign()
    }
  }
}

doU2F()
