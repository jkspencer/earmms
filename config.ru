require File.expand_path("../config/application.rb", __FILE__)
EARMMS.initialize

# register controllers
EARMMS.controllers.each do |c|
  map(c[:path]) do
    run c[:controller]
  end
end

