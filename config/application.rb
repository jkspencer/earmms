require File.expand_path("../boot.rb", __FILE__)

require 'yaml'
require 'sequel'
require 'mail'

module EARMMS
  @@root = File.expand_path("../..", __FILE__)
  require File.join(@@root, 'app/version')

  class << self
    attr_reader :controllers
    attr_accessor :database

    attr_accessor :app_config
    attr_accessor :custom_user_fields
    attr_accessor :base_url
    attr_accessor :site_name
    attr_accessor :org_name

    attr_accessor :email_from_address
    attr_accessor :email_send_to_self
    attr_accessor :email_subject_prefix
    attr_accessor :email_templates

    attr_accessor :default_language
    attr_reader :languages
  end

  def self.root
    @@root
  end

  def self.filter_strip_chars
    [
      " ",
      "\t",
      "\n",
      "(",
      ")",
      "[",
      "]",
      "{",
      "}",
      '"',
      "'",
      "#",
      /[\u0080-\u00ff]/, # all non-ASCII characters
    ]
  end

  def self.membership_status_options
    {
      :member => "Member",
      :supporter => "Supporter",
    }
  end

  def self.initialize(opts = {})
    # Check whether we can reach keyderiv before allowing the app to initialize
    unless opts[:no_check_keyderiv]
      begin
        EARMMS::Crypto.get_index_key "test", "test"
      rescue => e
        raise "Couldn't reach keyderiv, dying (inner: #{e.inspect})"
      end
    end

    @app_config = {}
    @custom_user_fields = []
    @base_url = ENV["APPLICATION_BASE_URL"] || "http://localhost"
    @site_name = ENV["SITE_NAME"] || "EARMMS"
    @org_name = ENV["ORG_NAME"] || "Example EARMMS organization"
    @email_from_address = ENV["SMTP_FROM"] || 'earmms@example.com'
    @email_subject_prefix = 'site-name-brackets'
    @email_send_to_self = true

    @default_language = 'en'
    @languages = Dir.glob(File.expand_path("../translations/*.yml", __FILE__)).map do |e|
      name = /(\w+)\.yml$/.match(e)[1]
      strings = YAML.load_file(e)

      [name, strings]
    end.to_h

    # In development mode, add a "language" that has no translated text, which
    # when t() is called, will display the translation key rather than any text
    if ENV['RACK_ENV'] == 'development'
      @languages["translationkeys"] = {
        :meta_description => "DEBUG: Translation keys"
      }
    end

    @controllers = YAML.load_file(File.expand_path('../controllers.yml', __FILE__)).map do |c|
      klass = self.const_get(c["controller"])

      klass.class_eval do
        configure do
          set :environment, ENV["RACK_ENV"] ||= "production"
        end
      end

      {
        :path => c["path"],
        :controller => klass,
      }
    end

    @database = Sequel.connect(ENV["DATABASE_URL"])
    @database.extension(:pagination)
    self.load_models unless opts[:no_load_models]

    @email_templates = EARMMS::EmailTemplates.new({
      :base_url => @base_url, 
      :site_name => @site_name, 
      :org_name => @org_name,
    })

    self.load_configs unless opts[:no_load_configs]
  end

  def self.load_configs
    require File.expand_path("../default_config.rb", __FILE__)
    require File.expand_path("../environments/#{ENV["RACK_ENV"]}.rb", __FILE__)

    if ENV["CONFIG_FILE"]
      require ENV["CONFIG_FILE"]
    end

    @custom_user_fields = @app_config[:custom_user_fields]
    @base_url = @email_templates.base_url = @app_config[:base_url]
    @site_name = @email_templates.site_name = @app_config[:site_name]
    @org_name = @email_templates.org_name = @app_config[:org_name]
    @email_from_address = @app_config[:email_from]

    self.refresh_db_configs
  end

  def self.refresh_db_configs
    @email_send_to_self = true
    EARMMS::ConfigModel.where(key: 'email-to-self').each do |m|
      @email_send_to_self = (m.value == 'yes')
    end

    @email_subject_prefix = 'site-name-brackets'
    EARMMS::ConfigModel.where(key: 'email-subject-prefix').each do |m|
      @email_subject_prefix = m.value
    end

    if !(["org-name", "org-name-brackets", "site-name", "site-name-brackets", "none"].include?(@email_subject_prefix))
      @email_subject_prefix = 'site-name-brackets'
    end
  end

  def self.configure(&block)
    config = {}
    block.(config)

    @controllers.each do |c|
      c[:controller].class_eval do
        configure do
          config.keys.each do |k|
            set k, config[k]
          end
        end
      end
    end

    config.each do |k, v|
      @app_config[k] = v
    end
  end

  def self.load_models
    require File.join(@@root, 'app/model')
    Dir.glob(File.join(@@root, 'app/models/*.rb')).each {|f| require f}
  end

  def self.email_subject(text)
    out = ""
    case EARMMS.email_subject_prefix
    when "none"
      out = text
    when "org-name"
      out = "#{EARMMS.org_name}: #{text}"
    when "org-name-brackets"
      out = "[#{EARMMS.org_name}] #{text}"
    when "site-name"
      out = "#{EARMMS.site_name}: #{text}"
    else # site-name-brackets
      out = "[#{EARMMS.site_name}] #{text}"
    end

    return out.force_encoding("UTF-8")
  end
end

require File.join(EARMMS.root, 'app/crypto')
require File.join(EARMMS.root, 'app/email_templates')

require File.join(EARMMS.root, 'app/helpers/application_helpers')
require File.join(EARMMS.root, 'app/controllers/application_controller')
require File.join(EARMMS.root, 'app/controllers/membership_admin_controller')
require File.join(EARMMS.root, 'app/controllers/system_controller')

Dir.glob(File.join(EARMMS.root, 'app/helpers/*.rb')).each {|f| require f}
Dir.glob(File.join(EARMMS.root, 'app/controllers/*.rb')).each {|f| require f}

# workers
require File.join(EARMMS.root, 'app/workers')
Dir.glob(File.join(EARMMS.root, 'app/workers/*.rb')).each {|f| require f}
