EARMMS.configure do |config|
  config[:session_secret] = ENV.fetch 'SESSION_SECRET' do
    require 'securerandom'
    SecureRandom.hex(32)
  end

  config[:site_name] = "EARMMS"
  config[:org_name] = "Example EARMMS organization"
  config[:base_url] = "http://localhost"
  config[:email_from] = "earmms@example.com"
  config[:theme_dir] = nil

  config[:extended_signup] = false
  config[:custom_user_fields] = []
end
