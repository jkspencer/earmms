EARMMS.configure do |config|
  config[:environment] = :development
end

Mail.defaults do
  # Use mailcatcher by default in development
  delivery_method :smtp, :address => 'localhost', :port => 9333
end
