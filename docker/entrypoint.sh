#!/bin/sh

echo "==> Running database migrations..."
bundle exec rake db:migrate || exit 1

echo "==> Configuring..."
bundle exec rake site:default_config || exit 1

cat <<-ENDHELP

================================================================================
Welcome to EARMMS! Signups are disabled by default, to create a user with admin
privileges, please run the following (substituting CONTAINER_NAME with the name
of the Docker container EARMMS is running in):

$ docker exec CONTAINER_NAME bundle exec rake user:invite_admin

This will print a link to the signup page with an invite code, assuming you've
configured the base URL correctly in your configuration file.
================================================================================

ENDHELP

echo "==> Starting supervisord"
exec supervisord -c /etc/supervisor/supervisord.conf
