Sequel.migration do
  up do
    add_column :alerts, :created, DateTime
    add_column :alerts, :actioned_ts, DateTime

    from(:alerts).update(:created => Time.now)
    from(:alerts).where(:actioned => true).update(:actioned_ts => Time.now)
  end

  down do
    drop_column :alerts, :created
    drop_column :alerts, :actioned_ts
  end
end
