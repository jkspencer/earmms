Sequel.migration do
  change do
    create_table :branch_meetings do
      primary_key :id
      foreign_key :branch, :branches

      String :datetime
      String :location
      String :notes
    end

    create_table :branch_meeting_attendance_records do
      primary_key :id
      foreign_key :meeting, :branch_meetings

      String :profile
      String :attendance_status
    end

    create_table :branch_meeting_attendance_record_filters do
      primary_key :id
      foreign_key :attendance_record, :branch_meeting_attendance_records

      String :filter_label
      String :filter_value
    end
  end
end
