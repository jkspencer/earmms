Sequel.migration do
  change do
    create_table :profile_changelog do
      primary_key :id
      foreign_key :profile, :profiles

      Time :created
      String :data
    end
  end
end
