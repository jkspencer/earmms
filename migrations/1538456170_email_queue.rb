Sequel.migration do
  change do
    create_table :email_queue do
      primary_key :id
      DateTime :creation

      String :recipients
      String :subject
      String :content
      String :content_html
      String :queue_status
    end

    add_column :mass_emails, :email_queue_id, Integer
  end
end
