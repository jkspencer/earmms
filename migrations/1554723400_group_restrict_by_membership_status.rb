Sequel.migration do
  change do
    add_column :groups, :restrict_to_member, TrueClass
  end
end
